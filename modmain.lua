local _G = GLOBAL
local require = _G.require

--========================================
--			   Util/Tuning
--========================================
_G.PP_DEBUG = GetModConfigData("pp_debug")
_G.MOBCHANGE = GetModConfigData("Mobchange")
_G.MOBZKEY = GetModConfigData("MobZkey", true)
_G.MOBXKEY = GetModConfigData("MobXkey", true)
_G.MOBCKEY = GetModConfigData("MobCkey", true)
_G.MOBVKEY = GetModConfigData("MobVkey", true)
_G.MOBBKEY = GetModConfigData("MobBkey", true)
_G.MOBPKEY = GetModConfigData("MobPkey", true)

_G.PP_ABILITY_SLOTS = {
	SLEEP = 1,
	SKILL1 = 2,
	SKILL2 = 3,
	SKILL3 = 4,
	SKILL4 = 5, 
}

_G.PP_SETTINGS = require("pp_settings")
_G.PP_MOBTYPES = require("pp_mobtype")
_G.PlayablePets = require("pp_util")
_G.PP_SKIN_DATA = require("pp_skindata")
_G.PIGNOTE_DATA = require("pignote_data")
_G.TUNING.PP = require("ppe_tuning") --does not include mob tunings, for that its TUNING.PLAYERMOBP
_G.PP_FORGE = {}
_G.TUNING.PM = require("pm_tuning") --TODO Remove
_G.PPSHOP = require("pp_shopdata")
_G.PP_ABILITIES = require("pp_abilities")

local PlayablePets = _G.PlayablePets

PlayablePets.Init(env.modname) 
PlayablePets.SetUpRPCs(env.modname)
AddModRPCHandler(env.modname, "PurchasedItem", PlayablePets.PurchasedItem)
PlayablePets.DoPurchasedItemRPC = function(player, ...)
	SendModRPCToServer(MOD_RPC[env.modname]["PurchasedItem"], ...)
end
_G.MOBPRESET = GetModConfigData("MobPreset")
_G.BOSS_STATS = GetModConfigData("MobBossMode")
_G.MONSTERHUNGER = GetModConfigData("MobHunger")
_G.MONSTERSANITY = GetModConfigData("MobSanity")
_G.MOBCRAFT = GetModConfigData("MobCraft")
_G.MOBHOUSE = GetModConfigData("MobHouseMethod")
_G.MOBPVP = GetModConfigData("MobPvP")
_G.MOBGHOST = GetModConfigData("MobGhosts")
_G.MOBSKELETONS = GetModConfigData("MobSkeletons")
_G.MOBPVPMODE = GetModConfigData("MobPvPMode")
_G.MOBNIGHTVISION = GetModConfigData("MobNightvision")
_G.MOBWEATHER = GetModConfigData("MobWeatherResistance")
_G.MOBFIRE = GetModConfigData("MobFire")
_G.SLEEP_REGEN = GetModConfigData("MobSleepRegen")
_G.MOBGIANTS = GetModConfigData("MobGiants")
_G.MOBGROW = GetModConfigData("MobGrowth")
_G.MOBPOISON = GetModConfigData("MobPoison")

local STRINGS = require("ppe_strings")
--========================================
--			Mod Compatibility
--========================================
GLOBAL.SW_PP_MODE = false
for k,v in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(v).name == "Island Adventures" or GLOBAL.KnownModIndex:GetModInfo(v).IslandAdventures then
		GLOBAL.SW_PP_MODE = true
    end
end

GLOBAL.HAMLET_PP_MODE = false
for k,v in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(v).name == "云霄国度-(Above the Clouds)" then
		GLOBAL.HAMLET_PP_MODE = true
    end
end

GLOBAL.PP_FORGE_ENABLED = false

for k,v in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.TheNet:GetServerGameMode() == "lavaarena" then
		GLOBAL.PP_FORGE_ENABLED = true
    end
end

GLOBAL.PP_SPIDER_HATS = false
for k,v in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(v).name == "Spider Hats" then
		GLOBAL.PP_SPIDER_HATS = true
    end
end

for k,v in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(v).name == "PP Server Stuff" then
		GLOBAL.PP_SERVER_STUFF = true
    end
end

--========================================
--			Standard Mod Stuff
--========================================

PrefabFiles = 
{
	"cultist_dagger_l",
	"wadebag",
	"poisonbubble2p",
	"antidotep",
	"pp_skeletons",
	"pp_fx",
	"pm_stage",
	"skinboxp",
	"gay_glommerp",
	"pignotesp",
	--"pm_bristle",
	----------------
	--Decor--
	----------------
	"ppd_candles",
	"ppd_candlesticks",
	"pp_structures",
	----------------
	--Misc
	----------------
	"death_carrot",
}	

Assets = 
{
	Asset("SOUNDPACKAGE", "sound/pp_sounds.fev"), 
	Asset( "SOUND", "sound/pp_sounds_bank00.fsb"), 
	------------------------
	Asset("ANIM", "anim/poison.zip"),
	Asset("ANIM", "anim/ghost_monster_build.zip"),
	Asset("ANIM", "anim/elder_cultist_l.zip"),
	------------------------
	Asset("IMAGE", "levels/textures/ground_noise_test.tex"),
	--Asset("IMAGE", "levels/tiles/forest_dark.tex"),
	--Asset("IMAGE", "levels/tiles/forest_dark.xml"),
	------------------------
	Asset( "IMAGE", "images/pp_essentials_icons.tex" ),
	Asset( "ATLAS", "images/pp_essentials_icons.xml" ),
	Asset( "IMAGE", "images/oinc1p.tex" ),
	Asset( "ATLAS", "images/oinc1p.xml" ),
	Asset( "IMAGE", "images/cultist_icons_l.tex" ),
	Asset( "ATLAS", "images/cultist_icons_l.xml" ),
	Asset( "IMAGE", "images/pigshopwindow.tex" ),
	Asset( "ATLAS", "images/pigshopwindow.xml" ),
	Asset( "IMAGE", "images/pigshopwindowbg.tex" ),
	Asset( "ATLAS", "images/pigshopwindowbg.xml" ),
	Asset( "IMAGE", "images/pigshop_ui.tex" ),
	Asset( "ATLAS", "images/pigshop_ui.xml" ),
	Asset( "IMAGE", "images/pm_ui.tex" ),
	Asset( "ATLAS", "images/pm_ui.xml" ),

	-------------------------------------------
	--Experimental
	Asset( "IMAGE", "images/pignote_old.tex" ),
	Asset( "ATLAS", "images/pignote_old.xml" ),

	Asset( "IMAGE", "images/pignote_icons.tex" ),
	Asset( "ATLAS", "images/pignote_icons.xml" ),

	Asset( "IMAGE", "images/pig_note_test.tex" ),
	Asset( "ATLAS", "images/pig_note_test.xml" ),
	--Asset("FONT", "fonts/Hey_Gorgeous_Standard.zip")
}
--GLOBAL.PAPER_MARIO_TALK = "paper_mario_talk"
--GLOBAL.TheSim:LoadFont("fonts/Hey_Gorgeous_Standard.zip", GLOBAL.PAPER_MARIO_TALK)

--========================================
--				 Strings
--========================================
local STRINGS = GLOBAL.STRINGS
STRINGS.CHARACTERS.MOBPLAYER = require("speech_mobplayer")
--========================================
--			    Postinits
--========================================
local Widget = require "widgets/widget"
local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local Text = require "widgets/text"

local currencyhud = require "widgets/ppcurrency"
AddClassPostConstruct("widgets/inventorybar", function(self)
	self.ppcurrency = self.root:AddChild(currencyhud(self.owner))
	self.ppcurrency:SetPosition(430, 80)
	self.ppcurrency:Hide() --gets unhidden by hamlet pets.
end)
AddReplicableComponent("ppcurrency")
AddReplicableComponent("pigshopping")
AddReplicableComponent("ppskin_manager")
AddReplicableComponent("pm_battlemanager")

local function OnCurrencyDirty(inst)
	if inst and inst.HUD and inst.HUD.controls.inv.ppcurrency then
		inst.HUD.controls.inv.ppcurrency:SetAmount(inst.currentcurrency:value())
	end
end


local function UnlockSkin(inst, data)
	--inst.replica.ppskin_manager:Initialize()
	if data.errorm then
		GLOBAL.ChatHistory:AddToHistory(GLOBAL.ChatTypes.CommandResponse, nil, nil, nil, data.errorm, {255/255, 80/255, 80/255}, nil, nil, true)
		return
	end
	if data.skin_id then
		local success = true
		GLOBAL.TheSim:GetPersistentString("ppskins", function(load_success, vdata)
			if load_success and vdata then
				local status, old_data = GLOBAL.pcall( function() return GLOBAL.json.decode(vdata) end )
				local id = data.skin_id
				local mob = data.targetprefab or nil
				local newdata = old_data
				if type(old_data) == "table" then
					--if 
					if GLOBAL.PP_SKIN_DATA[string.upper(id)] and not mob then
						local skin_list = GLOBAL.PP_SKIN_DATA[string.upper(id)]
						local obtainable_list = {}
						for i, v in ipairs(skin_list) do
							if not newdata[v] or not newdata[v][id] then
								table.insert(obtainable_list, v)
							end
						end
						if #obtainable_list > 0 then
							mob = obtainable_list[math.random(1, #obtainable_list)]
							data.targetprefab = mob
						end
					end

					if not mob or (newdata[mob] and newdata[mob][id] == true) then
						success = false
					end
					--print(newdata)
					if success and mob then
						if not newdata[mob] then
							newdata[mob] = {}
						end
					
						GLOBAL.ChatHistory:SendCommandResponse("You've unlocked the "..data.skin_id.." skin for "..(STRINGS.CHARACTER_TITLES[data.targetprefab] or data.targetprefab).."! Use /setskin to access them!")
						newdata[mob][id] = true
						GLOBAL.TheSim:SetPersistentString("ppskins", GLOBAL.json.encode(newdata), false)
					end
				else
					print("PPERROR: Your ppskins file seems to be corrupted, so we're generating you a new one. Sorry but you won't get your skin.")
					GLOBAL.TheSim:SetPersistentString("ppskins", GLOBAL.json.encode({}), false)
				end
			else
				PlayablePets.DebugPrint("ppskins data not found, creating new data!")
            	GLOBAL.TheSim:SetPersistentString("ppskins", GLOBAL.json.encode({}), false)
			end
		end)
		if success then
			local message = "You've unlocked the "..tostring(data.skin_id).." skin for "..(STRINGS.CHARACTER_TITLES[data.targetprefab] or tostring(data.targetprefab)).."! Use /setskin to access them!"
			--GLOBAL.ChatHistory:AddToHistory(GLOBAL.ChatTypes.CommandResponse, nil, nil, nil, message, {120/255, 89/255, 186/255}, nil, nil, true)
		else
			local message = "Sorry, you've already unlocked this skin."
			print(inst:GetDisplayName().." got a duplicate skin: skin_id-"..data.skin_id.." prefab-"..tostring(data.targetprefab))
			--GLOBAL.ChatHistory:AddToHistory(GLOBAL.ChatTypes.CommandResponse, nil, nil, nil, message, {120/255, 89/255, 186/255}, nil, nil, true)
		end
	end
end

AddPlayerPostInit(function(inst)
	inst.currentcurrency = GLOBAL.net_int(inst.GUID, "ppcurrency.current", "ppcurrencydirty")
	inst.currentcurrency:set(0)


	inst:DoTaskInTime(1,function (inst)
		if inst and inst.HUD then
			
		end
	end)

	if not GLOBAL.TheNet:IsDedicated() then
		inst:ListenForEvent("ppcurrencydirty", function(inst) OnCurrencyDirty(inst) end)
	end
	if not GLOBAL.TheWorld.ismastersim then
		inst:ListenForEvent("ppunlockskin", UnlockSkin)
        return inst
    end
	inst:AddComponent("ppcurrency")
	inst:DoTaskInTime(0, function(inst)
		if GLOBAL.TheWorld.components.ppbank_manager then
			GLOBAL.TheWorld.components.ppbank_manager:Withdraw(inst) --If there is a listing, withdraw and unregister.
		else
			print("PP Error: TheWorld did not have ppbank_manager component!")
		end
	end)
	if inst.mobplayer and not inst.components.playablepet then
		inst:AddComponent("playablepet")
	end
	inst:AddComponent("factionp")
end)

--[[
	self.cur_icon = self:AddChild(Image("images/oinc1p.xml", "oinc1p.tex"))  
	self.cur_icon:SetPosition(200, 300)
	self.cur_icon:MoveToFront()
    self.cur_num = self:AddChild(Text(GLOBAL.NUMBERFONT, 33, "0"))
	self.cur_num:SetPosition(200, 300)
	self.cur_num:MoveToFront()]]

local SkinsPuppet = require "widgets/skinspuppet"

if not SkinsPuppet.pp_puppets then
	SkinsPuppet.pp_puppets = {}
end

if not SkinsPuppet.pp_skins then
	SkinsPuppet.pp_skins = {}
end

require("pp_wardrobe_postinit")

require("pp_postinits")

--Projectile
AddComponentPostInit("projectile", function (projectile)
	local function StopTrackingDelayOwner(self)
		if self.delayowner ~= nil then
			self.inst:RemoveEventCallback("onremove", self._ondelaycancel, self.delayowner)
			self.inst:RemoveEventCallback("newstate", self._ondelaycancel, self.delayowner)
			self.delayowner = nil
		end
	end
	
	local function GetMobOwner(inst, owner)
		if owner and owner._ismonster then
			return nil
		else
			return inst
		end
	end
	
	local old_projectilehit = projectile.Hit
	--if GLOBAL.TheNet:GetServerGameMode() ~= "lavaarena" then
		
	
		function projectile:Hit(target)
			if self.owner and self.owner._ismonster then
				local attacker = self.owner
				local weapon = GetMobOwner(self.inst, attacker)
				--print("DEBUG: weapon is "..weapon)
				StopTrackingDelayOwner(self)
				self:Stop()
				self.inst.Physics:Stop()
				if attacker.components.combat == nil and attacker.components.weapon ~= nil and attacker.components.inventoryitem ~= nil then
					weapon = attacker
					attacker = weapon.components.inventoryitem.owner
				end
				if attacker ~= nil and attacker.components.combat ~= nil then
					attacker.components.combat:DoAttack(target, weapon, self.inst, self.stimuli)
				end
				if self.onhit ~= nil then
					self.onhit(self.inst, attacker, target)
				end
			else
				return old_projectilehit(self, target)
			end
		end	
	--end
end)

--Ins
AddComponentPostInit("stormwatcher", function (stormwatcher)
	
	local old_UpdateStormLevel = stormwatcher.UpdateStormLevel
	
	function stormwatcher:UpdateStormLevel()
		if self.inst and self.inst._stormimmune then
			self.currentstorm = GLOBAL.STORM_TYPES.NONE
			return
		else
			old_UpdateStormLevel(self)
		end
	end
end)

local function IsHeavyLifter(inst)
	return inst.components.inventoryitem and inst.components.inventoryitem.owner and (inst.components.inventoryitem.owner:HasTag("epic") or inst.components.inventoryitem.owner:HasTag("largecreature"))
end

AddComponentPostInit("equippable", function (equippable)
	local old_GetWalkSpeedMult = equippable.GetWalkSpeedMult
	
	function equippable:GetWalkSpeedMult()
		return IsHeavyLifter(self.inst) and 1.0 or old_GetWalkSpeedMult(self)
	end		
end)

AddComponentPostInit("combat", function (combat)
	local old_StartAttack = combat.StartAttack
	
	function combat:StartAttack()
		self.inst:PushEvent("cursed_attack")
		old_StartAttack(self)
	end		
end)

AddComponentPostInit("locomotor", function (combat)
	local old_StartAttack = combat.StartAttack
	
	function combat:StartAttack()
		self.inst:PushEvent("cursed_attack")
		old_StartAttack(self)
	end		
end)

GLOBAL.CHARACTER_INGREDIENT.OINCS = "oincsp"
--[[
AddComponentPostInit("builder", function (builder)
	local old_HasCharacterIngredient = builder.HasCharacterIngredient
	function builder:HasCharacterIngredient(ingredient)
		if ingredient.type == GLOBAL.CHARACTER_INGREDIENT.OINCS then
			if self.inst.components.ppcurrency ~= nil then
				local current = math.ceil(self.inst.components.ppcurrency.current)
				return current > ingredient.amount, current
			end
		end
		old_HasCharacterIngredient(ingredient)
	end

	local old_RemoveIngredients = builder.RemoveIngredients

	function builder:RemoveIngredients(ingredients, recname, discounted)
		if self.freebuildmode then
			return
		end

		local recipe = GLOBAL.AllRecipes[recname]
		if recipe then
			for k,v in pairs(recipe.character_ingredients) do
				if v.type == GLOBAL.CHARACTER_INGREDIENT.OINCS then
					self.inst:PushEvent("consumeppcurrencycost")
					self.inst.components.ppcurrency:DoDelta(-v.amount, false)
				end
			end
		end
		old_RemoveIngredients(ingredients, recname, discounted)
	end
end)

AddComponentPostInit("builder_replica", function (builder)
	local old_HasCharacterIngredient = builder.HasCharacterIngredient
	function builder:HasCharacterIngredient(ingredient)
		if self.classified ~= nil then
			if ingredient.type == GLOBAL.CHARACTER_INGREDIENT.OINCS then
				if self.inst.replica.ppcurrency ~= nil then
					local current = math.ceil(self.inst.replica.ppcurrency:GetCurrent())
					return current > ingredient.amount, current
				end
			end
		end
		old_HasCharacterIngredient(ingredient)
	end
end)]]

GLOBAL.FOODTYPE.BONE= "BONE"

local bone_edibles = {
	"boneshard",
	"houndstooth",
	"fossil_piece"
}

local function MakeBoneEdible(inst)
	if not inst.components.edible then --halloween candy and other food trinkets will not count
		inst:AddComponent("edible")
		inst.components.edible.foodtype = GLOBAL.FOODTYPE.BONE
		inst.components.edible.healthvalue = 0
		inst.components.edible.hungervalue = 5
		inst.components.edible.sanityvalue = 0
	end
end

for i, v in ipairs(bone_edibles) do
	AddPrefabPostInit(v, MakeBoneEdible)
end

GLOBAL.FOODTYPE.BOOK = "BOOK"
--GLOBAL.FOODTYPE.INSECT = "INSECT"

local function MakeBookEdible(inst)
	inst:AddComponent("edible")
	inst.components.edible.foodtype = GLOBAL.FOODTYPE.BOOK
	inst.components.edible.healthvalue = 10
    inst.components.edible.hungervalue = 10
    inst.components.edible.sanityvalue = 10
end

local book_items = {
	"book_birds",
	"book_brimstone",
	"book_gardening",
	"book_horticulture",
	"book_silviculture",
	"book_sleep",
	"book_tentacles",
	"cookbook",
	"papyrus",
}

for i, v in ipairs(book_items) do
	AddPrefabPostInit(v, MakeBookEdible)
end

AddPrefabPostInit("forest", function(inst)
	if not GLOBAL.TheWorld.ismastersim then
        return inst
    end
	--TODO use the WorldPostinit
	inst:AddComponent("ppbank_manager") --saves player's currency when they force despawn.
end)

AddPrefabPostInit("cave", function(inst)
	if not GLOBAL.TheWorld.ismastersim then
        return inst
    end
	inst:AddComponent("ppbank_manager") --saves player's currency when they force despawn.
end)
-----------------------------------------------------
-- HUD/Popup/Screens
-----------------------------------------------------
local PHUD = require("screens/playerhud")
local ShopPopupScreen = require "screens/pigshopppopupscreen"
function PHUD:OpenShoppScreen(data, customer)
    self:CloseShoppScreen()
    self.shoppscreen = ShopPopupScreen(self.owner, data, customer)
    self:OpenScreenUnderPause(self.shoppscreen)
    return true
end

function PHUD:CloseShoppScreen()
    if self.shoppscreen ~= nil then
        if self.shoppscreen.inst:IsValid() then
            TheFrontEnd:PopScreen(self.shoppscreen)
		end
        self.shoppscreen = nil
    end
end
AddPopup("pigshopp")
GLOBAL.POPUPS.pigshopp.fn = function(inst, show, shop)
    if inst.HUD then
        if not show then
            inst.HUD:CloseShoppScreen()
        elseif not inst.HUD:OpenShoppScreen(shop, inst) then
            POPUPS.pigshopp:Close(inst)
        end
    end
end

--Experimental stuff
AddReplicableComponent("pignote")

local PigNotePopupScreen = require "screens/pignotepopupscreen"
function PHUD:OpenPigNoteScreen(data)
    self:ClosePigNoteScreen()
    self.pignotescreen = PigNotePopupScreen(self.owner, data.replica.pignote.text_data)
    self:OpenScreenUnderPause(self.pignotescreen)
    return true
end

function PHUD:ClosePigNoteScreen()
    if self.pignotescreen ~= nil then
        if self.pignotescreen.inst:IsValid() then
            TheFrontEnd:PopScreen(self.pignotescreen)
		end
        self.pignotescreen = nil
    end
end

AddPopup("pig_note")
GLOBAL.POPUPS.pig_note.fn = function(inst, show, note)
	--local note = card.replica.disgaeacard.carddata
    if inst.HUD then
        if not show then
            inst.HUD:ClosePigNoteScreen()
        elseif not inst.HUD:OpenPigNoteScreen(note) then
            POPUPS.pig_note:Close(inst)
        end
    end
end
--========================================
--				  Tiles
--========================================
--[[
AddTile(
    "FOREST_DARK",
    "LAND",
    {ground_name = "Forest Dark", old_static_id = GLOBAL.GROUND.FOREST},
    {
        name="forest_dark",
        noise_texture="ground_noise_test",
        runsound="dontstarve/movement/run_woods",
        walksound="dontstarve/movement/walk_woods",
        snowsound="dontstarve/movement/run_snow",
        mudsound="dontstarve/movement/run_mud",
    },
    {
        name="map_edge",
        noise_texture="mini_forest_noise"
    },
    {
        name = "forest",
    }
)]]
--========================================
--			     Recipes
--========================================
modimport("scripts/ppe_recipes")
--========================================
--			     Commands
--========================================
require("pp_commands")
--========================================
--			     Actions
--========================================
--Transform table is set up this:
--key(target's prefabname) --> value(what the imposter transforms into)
GLOBAL.PP_MIMICLIST = {}
AddAction("PP_TRANSFORM", "Transform", function(act)
	if act.target:HasTag("player") and not act.target:HasTag("mimicer")  then
		return true
	else
		return act.target.prefab and GLOBAL.PP_MIMICLIST[act.target.prefab]
	end
end)
GLOBAL.ACTIONS.PP_TRANSFORM.distance = 40 

AddComponentAction("SCENE", "health", function(inst, doer, actions, right)
	if (inst:HasTag("player") or GLOBAL.PP_MIMICLIST[inst.prefab]) and right and inst ~= doer and doer:HasTag("mimicer") then
		table.insert(actions, GLOBAL.ACTIONS.PP_TRANSFORM)
	end
end)

GLOBAL.ACTIONS.PP_TRANSFORM.rmb = true 

AddAction("PP_SHOPPING", "Shop at", function(act)
	if act.doer ~= nil and act.target ~= nil then
		--print("DEBUG: SLEEPIN OVERRIDE WORKED!")
        local shop = act.target.components.pigshopping
        if shop ~= nil and shop.isopen then
            shop:SellToCustomer(act.doer)
            return true
        end
    end
end)

AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.PP_SHOPPING, "doshortaction"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.PP_SHOPPING, "doshortaction"))

AddComponentAction("SCENE", "pigshopping", function(inst, doer, actions, right)
	if inst:HasTag("structure") and inst:HasTag("pig_shop") then
		table.insert(actions, GLOBAL.ACTIONS.PP_SHOPPING)
	end
end)

AddAction("PP_DESTROY", "Destroy", function(act)
	return act.target:HasTag("structure")
end)

GLOBAL.ACTIONS.PP_DESTROY.rmb = true 

AddComponentAction("SCENE", "workable", function(inst, doer, actions, right)
	if inst:HasTag("structure") and doer:HasTag("deerclops") and right then
		table.insert(actions, GLOBAL.ACTIONS.PP_DESTROY)
	end
end)

AddAction("PP_CENSOREDDEVOUR", "Devour", function(act)
	if act.doer:HasTag("CENSORED") and (act.target:HasTag("playerskeleton") or act.target:HasTag("carcass")) then
		act.doer.sg:GoToState("devour", act.target)
		return true
	else
		return false
	end
end)

GLOBAL.ACTIONS.PP_CENSOREDDEVOUR.rmb = true 

GLOBAL.ACTIONS.SLEEPIN.fn = function(act)
    if act.doer ~= nil then
		--print("DEBUG: SLEEPIN OVERRIDE WORKED!")
        local bag =
            (act.invobject ~= nil and act.invobject.components.sleepingbag ~= nil and act.invobject) or
            (act.target ~= nil and act.target.components.sleepingbag ~= nil and act.target) or (act.invobject ~= nil and act.invobject.components.multisleepingbag ~= nil and act.invobject) or
            (act.target ~= nil and act.target.components.multisleepingbag ~= nil and act.target) or
            nil
        if bag ~= nil then
            if bag.components.sleepingbag then
				bag.components.sleepingbag:DoSleep(act.doer)
			else
				bag.components.multisleepingbag:DoSleep(act.doer)
			end
            return true
        end
    end
end

AddAction("HULK_TELEPORT", "Teleport", function(act)
	local act_pos = act:GetActionPoint() or nil
	if act.doer ~= nil and act.doer:HasTag("player") and act.doer.can_tele and act.doer.can_tele == true and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer.sg:GoToState("portal_jumpin", act_pos and act_pos or act.pos)
		return true
	else
		--print("ACTION FAILED")
		return false
	end
end)

GLOBAL.ACTIONS.HULK_TELEPORT.distance = 40 
GLOBAL.ACTIONS.HULK_TELEPORT.rmb = true 

AddAction("PP_RANGEDSPECIAL", "Use Ranged Special", function(act)
	local act_pos = act:GetActionPoint() or nil
	if act.doer ~= nil and act.doer:HasTag("player") and act.doer.components.combat and not act.doer.components.combat:InCooldown() and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer.sg:GoToState("pp_rangedspecial", act_pos or act.pos)
		return true
	else
		--print("ACTION FAILED")
		return false
	end
end)

GLOBAL.ACTIONS.PP_RANGEDSPECIAL.distance = 20 
GLOBAL.ACTIONS.PP_RANGEDSPECIAL.rmb = true 

AddAction("PP_DODGE", "Dodge Roll", function(act)
	local act_pos = act:GetActionPoint() or nil
	if act.doer ~= nil and act.doer:HasTag("player") and act.doer.can_dodge and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer.sg:GoToState("dodge", act_pos and act_pos or act.pos)
		return true
	else
		--print("ACTION FAILED")
		return false
	end
end)

GLOBAL.ACTIONS.PP_DODGE.distance = 100 
GLOBAL.ACTIONS.PP_DODGE.rmb = true 

AddComponentAction("SCENE", "multisleepingbag", function(inst, doer, actions, right)
	if inst:HasTag("mobhome") then
		table.insert(actions, GLOBAL.ACTIONS.SLEEPIN)
	end
end)

local function CheckCarcass(carcass)
	return not (carcass.components.burnable ~= nil and carcass.components.burnable:IsBurning())
		and carcass:IsValid()
		and carcass:HasTag("meat_carcass")
end

local function CanEatCarcass(inst)
	if inst.components.eater then
		for i, v in ipairs(inst.components.eater.caneat) do
			if v == GLOBAL.FOODTYPE.MEAT then
				--print("Test result came up positive, you can eat dead man")
				return true
			end
		end
		return false
	else
		return false
	end
end

AddAction("PP_CHOMP", "Eat", function(act)
	local act_pos = act:GetActionPoint() or nil
	local target = act.target
	if act.doer ~= nil and act.doer:HasTag("player") and (target and CheckCarcass(target)) and CanEatCarcass(act.doer) and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer:PushEvent("chomp", {target = target})
		return true
	else
		--print("ACTION FAILED")
		return false
	end
end)

AddComponentAction("SCENE", "timer", function(inst, doer, actions, right)
	if inst:HasTag("meat_carcass") then
		table.insert(actions, GLOBAL.ACTIONS.PP_CHOMP)
	end
end)

GLOBAL.ACTIONS.PP_CHOMP.distance = 2 

--SCENES don't stack, at least in the same mod, might need to see if you can wrap around this to avoid conflicts with mods.
AddComponentAction("SCENE", "workable", function(inst, doer, actions, right)
    if inst:HasTag("tree") and doer.components.worker and doer.components.worker.actions[GLOBAL.ACTIONS.CHOP] then
        table.insert(actions, GLOBAL.ACTIONS.CHOP)
	elseif inst:HasTag("boulder") and doer.components.worker and doer.components.worker.actions[GLOBAL.ACTIONS.MINE] then
        table.insert(actions, GLOBAL.ACTIONS.MINE)
	elseif inst:HasTag("structure") and doer:HasTag("deerclops") and right then
		table.insert(actions, GLOBAL.ACTIONS.PP_DESTROY)
	elseif (inst:HasTag("bone") or inst.prefab == "skeleton" or inst.prefab == "skeleton_player") and doer:HasTag("scavantula") and right then
		table.insert(actions, GLOBAL.ACTIONS.PP_DESTROY)
    end
	if (inst:HasTag("playerskeleton") or inst:HasTag("carcass")) and doer:HasTag("CENSORED") and right then
		table.insert(actions, GLOBAL.ACTIONS.PP_CENSOREDDEVOUR)
	end
end)

AddAction("MALBATROSS_TELEPORT", "Dive", function(act)
	local act_pos = act:GetActionPoint() or nil
	local actor_pos = act.doer and act.doer:GetPosition() or nil
	if act.doer ~= nil and act.doer:HasTag("player") and not act.doer._isflying and act.doer.willdive and act.doer.willdive == true and (act.doer.components.health and not act.doer.components.health:IsDead())  then
		act.doer.sg:GoToState("dive", act_pos and act_pos or act.pos)
		return true
	else
		print("MALBATROSS ACTION FAILED")
		return false
	end
end)

GLOBAL.ACTIONS.MALBATROSS_TELEPORT.distance = 40 
GLOBAL.ACTIONS.MALBATROSS_TELEPORT.rmb = true 
GLOBAL.ACTIONS.MALBATROSS_TELEPORT.do_not_locomote = true 

AddAction("PP_ROLL", "Roll", function(act)
	local act_pos = act:GetActionPoint() or nil
	if act.doer ~= nil and act.doer:HasTag("player") and act.doer.canroll and (act.doer.components.health and not act.doer.components.health:IsDead()) then
		act.doer.sg:GoToState("roll_start", act_pos and act_pos or act.pos)
		return true
	else
		return false
	end
end)

GLOBAL.ACTIONS.PP_ROLL.distance = 40 
GLOBAL.ACTIONS.PP_ROLL.rmb = true 
GLOBAL.ACTIONS.PP_ROLL.do_not_locomote = true 

AddAction("DUSTMOTH_DUST", "Dust", function(act)
	return act.target:HasTag("structure") and not act.target:HasTag("dusted") and not (act.target:HasTag("wall") or act.target:HasTag("tree"))
end)

GLOBAL.ACTIONS.DUSTMOTH_DUST.rmb = true 

AddComponentAction("SCENE", "inspectable", function(inst, doer, actions, right)
	if inst:HasTag("structure") and not inst:HasTag("dusted") and not inst:HasTag("wall") and not inst:HasTag("tree") and doer and doer.prefab == "dustmothp" and right then
		table.insert(actions, GLOBAL.ACTIONS.DUSTMOTH_DUST)
	end
end)



