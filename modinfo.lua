-- This information tells other players more about the mod
name = "Playable Pets Essentials"
author = "Leonardo Coxington"
version = "1.31.10"
description = "Core mod that Playable Pets require to function. \n \nVersion:"..version
-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "/topic/73911-playable-pets/"
folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = name.." Dev."
end
-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true
forge_compatible = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = 5804674 --must load before all Playable Pets

server_filter_tags = {
	"mob", "playable pets"
}

local SETTING = {
	OFF = "Off",
	DISABLE = "Disable",
	ENABLE = "Enable",
	LOCKED = "Locked",
	
	-- Mob Presets
	ALL_MOBS = "AllMobs",
	NO_GIANTS = "NoGiants",
	NO_BOSSES = "NoBosses",
	BOSSES_ONLY = "BossesOnly",
	GIANTS_ONLY = "GiantsOnly",
	CRAFTY_MOBS = "CraftyMobsOnly",
	
	-- Mob Houses
	HOUSE_CRAFT_ONLY = "Enable2",
	HOUSE_ON_SPAWN = "Enable1",
	HOUSE_BOTH = "Enable3",
	
	-- PvP Damage
	PVP_50_PERCENT_DMG = 0.5,
	PVP_100_PERCENT_DMG = 1.0,
	PVP_150_PERCENT_DMG = 1.5,
	PVP_200_PERCENT_DMG = 2.0,
	
	-- Misc
	HUMANOID_SANITY_ONLY = "Disable1",
	MONSTER_CHARCHANGE_ONLY = "Enable1",
}

local function GetKeys()
	local options = 
	{
		{description= "A", data = 97},
		{description= "B", data = 98},
		{description= "C", data = 99},
		{description= "D", data = 100},
		{description= "E", data = 101},
		{description= "F", data = 102},
		{description= "G", data = 103},
		{description= "H", data = 104},
		{description= "I", data = 105},
		{description= "J", data = 106},
		{description= "K", data = 107},
		{description= "L", data = 108},
		{description= "M", data = 109},
		{description= "N", data = 110},
		{description= "O", data = 111},
		{description= "P", data = 112},
		{description= "Q", data = 113},
		{description= "R", data = 114},
		{description= "S", data = 115},
		{description= "T", data = 116},
		{description= "U", data = 117},
		{description= "V", data = 118},
		{description= "W", data = 119},
		{description= "X", data = 120},
		{description= "Y", data = 121},
		{description= "Z", data = 122},
		{description= "F1", data = 282},
		{description= "F2", data = 283},
		{description= "F3", data = 284},
		{description= "F4", data = 285},
		{description= "F5", data = 286},
		{description= "F6", data = 287},
		{description= "F7", data = 288},
		{description= "F8", data = 289},
		{description= "F9", data = 290},
		{description= "F10", data = 291},
		{description= "F11", data = 292},
		{description= "F12", data = 293},
		{description= "0", data = 48},
		{description= "1", data = 49},
		{description= "2", data = 50},
		{description= "3", data = 51},
		{description= "4", data = 52},
		{description= "5", data = 53},
		{description= "6", data = 54},
		{description= "7", data = 55},
		{description= "8", data = 56},
		{description= "9", data = 57},
		{description= "Alt", data = 400},
		{description= "Control", data = 401},
		{description= "Shift", data = 402},
		{description= "Back Space", data = 8},
		{description= "Period", data = 46},
		{description= "/", data = 47},
		{description= ";", data = 400}
		--TODO add inputs for controller buttons/mouse buttons?
	}
	return options
end

configuration_options =
{
	{
		name = "MobGhosts",
		label = "Mob Ghosts",
		options = 
		{
			{description = "Yes", data = SETTING.ENABLE,  hover = "Mobs will become ghosts on death."},
			{description = "No",  data = SETTING.DISABLE, hover = "Mobs will return to character select on death."},
			--{description = "Locked", data = SETTING.LOCKED, hover = "Coming Soon?"},
		},
		default = SETTING.ENABLE
	},

	{
		name = "MobSkeletons",
		label = "Mob Skeletons",
		options = 
		{
			{description = "Yes", data = true,  hover = "Mobs will drop skeletons on death."},
			{description = "No",  data = false, hover = "Mobs will not drop skeletons on death."},
			--{description = "Locked", data = SETTING.LOCKED, hover = "Coming Soon?"},
		},
		default = true
	},
	
	{
		name = "Mobchange",
		label = "Character Change (P)",
		options = 
		{
			{description = "Enable",  data = SETTING.ENABLE},
			--{description = "For Monsters Only", data = SETTING.MONSTER_CHARCHANGE_ONLY},
			{description = "Disable", data = SETTING.DISABLE},
		},
		default = SETTING.ENABLE
	},
	
	{
		name = "MobSleepRegen",
		label = "Sleep Regen", -- Testing to see if variables can be used in place of strings.
		options = 
		{
			{description = "Disable", data = 0, hover = "Mobs will not restore HP through sleeping"},
			{description = "-75%",  data = 0.25,  hover = "Health regen will be reduced by 75%"},
			{description = "-50%",  data = 0.5,  hover = "Health regen will be halved"},
			{description = "-25%",  data = 0.75,  hover = "Health regen will be reduced by 25%"},
			{description = "Default",  data = 1,  hover = "Health regen will be not changed"},
			{description = "+25%",  data = 1.25,  hover = "Health regen will be increased by 25%"},
			{description = "+50%",  data = 1.5,  hover = "Health regen will be increased by 50%"},
			{description = "+75%",  data = 1.75,  hover = "Health regen will be increased by 75%"},
			{description = "+100%",  data = 2,  hover = "Health regen will be doubled"},
			--{description = "Locked", data = SETTING.LOCKED, hover = "Coming Soon?"},
		},
		default = 1
	},
	
	{
		name = "MobHunger",
		label = "Should Mobs get hungry?",
		options = 
		{
			{description = "Yes", data = SETTING.ENABLE},
			{description = "No",  data = SETTING.DISABLE},
		},
		default = SETTING.ENABLE
	},

	{
		name = "MobSanity",
		label = "Should mobs have sanity?",
		options = 
		{
			{description = "Yes",       data = SETTING.ENABLE,               hover = "Mobs will lose sanity and get tired."},
			{description = "Humanoids Only", data = SETTING.HUMANOID_SANITY_ONLY, hover = "Only mobs similar to pigmen will lose sanity."},
			{description = "No",        data = SETTING.DISABLE,              hover = "No mob will lose sanity."},
		},
		default = SETTING.DISABLE
	},	
	
	{
		name = "MobWeatherResistance",
		label = "Weather Res.",
		options = 
		{
			{description = "None",  data = 0,  hover = "Mobs will not be immune to weather."},
			{description = "Default", data = 1, hover = "Mobs will have resistances they are built with."},
			{description = "All", data = 2, hover = "All Mobs will be resistant to weather."},
		},
		default = 1
	},
	
	{
		name = "MobFire",
		label = "Mob Destruction",
		options = 
		{
			{description = "Disable",  data = SETTING.ENABLE,  hover = "Destructive traits will be disabled."},
			{description = "Enable", data = SETTING.DISABLE, hover = "Destructive traits will be enabled."},
		},
		default = SETTING.DISABLE
	},
	
	{
		name = "MobCraft",
		label = "Mob Crafting",
		options = 
		{
			{description = "Yes", data = SETTING.ENABLE,  hover = "All mobs can craft, build, etc."},
			{description = "No",  data = SETTING.DISABLE, hover = "Most mob's actions are restricted."},
		},
		default = SETTING.ENABLE
	},
	
	{
		name = "MobHouseMethod",
		label = "House Attainability",
		options = 
		{
			{description = "On Spawn", data = SETTING.HOUSE_ON_SPAWN,   hover = "Mobs will spawn with their homes."},
			{description = "Crafting", data = SETTING.HOUSE_CRAFT_ONLY, hover = "Homes will need to be crafted."},
			{description = "Both",     data = SETTING.HOUSE_BOTH,       hover = "Homes can be crafted and obtained on Spawn."},
			{description = "No Homes", data = SETTING.DISABLE,          hover = "Homes cannot be obtained through normal means."},
		},
		default = SETTING.HOUSE_ON_SPAWN
	},
	
	{
		name = "MobPvP",
		label = "PvP Damage Scale", 
		options = 
		{
			{description = "50%",  data = SETTING.PVP_50_PERCENT_DMG,  hover = "Mobs will do half damage to players (recommended for co-op)."},
			{description = "100%", data = SETTING.PVP_100_PERCENT_DMG, hover = "Mobs will do default dmg to players."},
			{description = "150%", data = SETTING.PVP_150_PERCENT_DMG, hover = "Mobs will do 50% more damage to players (recommended for PvP probably)."},
			{description = "200%", data = SETTING.PVP_200_PERCENT_DMG, hover = "Mobs will do double damage to players (NOT RECOMMENDED FOR PUB PLAY)."},
		},
		default = SETTING.PVP_100_PERCENT_DMG
	},
	
	{
		name = "MobNightvision",
		label = "Nightvision", 
		options = 
		{
			{description = "Enable",  data = SETTING.ENABLE,  hover = "Mobs (that apply) will have nightvision."},
			{description = "Disable", data = SETTING.DISABLE, hover = "Mobs will not have nightvision, but will still be immune to charlie."},
			{description = "Disable w/o immunity", data = "Disable2", hover = "Mobs will not have nightvision and will not be immune to charlie."},
			
		},
		default = SETTING.ENABLE
	},
	
	{
		name = "MobBossMode",
		label = "Boss Stats", -- Testing to see if variables can be used in place of strings.
		options = 
		{
			{description = "Original",  data = true,  hover = "Bosses, such as Dragonfly, will retain their basegame stats (Very OP)"},
			{description = "Balanced", data = false, hover = "Bosses, such as Dragonfly, will have heavily altered stats to be more balanced"},
			--{description = "Locked", data = SETTING.LOCKED, hover = "Coming Soon?"},
		},
		default = false
	},
	
	--[[
	{
		name = "MobShadow",
        label = "Shadow's Mode", --testing to see if variables can be used in place of strings.
        options = 
        {
            {description = "Mob Mode", data = "Enable", hover = "You can be seen by everyone and interact with them"},
			{description = "Natural Mode", data = "Enable2", hover = "Only insane entities can see and interact with them."},
        },
        default = "Enable"
	
	},
	]]
	
	{
		name = "MobPoison",
        label = "Poison", --testing to see if variables can be used in place of strings.
        options = 
        {
            {description = "Enable", data = "Enable", hover = "Mobs, that can do it, can inflict poison"},
			{description = "Disable", data = "Disable", hover = "Poison cannot be inflicted"},
        },
        default = "Enable"
	
	},
	
	{
		name = "MobGrowth",
        label = "Growth", --testing to see if variables can be used in place of strings.
        options = 
        {
            {description = "Enable", data = "Enable", hover = "Mobs, that can do it, can grow up"},
			{description = "Disable", data = "Disable", hover = "Mobs cannot grow up"},
        },
        default = "Enable"
	
	},
	
	{
		name = "MobGiants",
        label = "Giants", --testing to see if variables can be used in place of strings.
        options = 
        {
            {description = "Enabled", data = 1, hover = "Giants are playable"},
			{description = "Disabled", data = 0, hover = "Giants are not playable"},
			{description = "Only Giants", data = 2, hover = "Only Giants can be played"},
        },
        default = 1
	
	},
	
	{
		name = "MobZkey",
		label = "Z key Override",
		options = GetKeys(),
		default = 122
	},
	
	{
		name = "MobXkey",
		label = "H key Override",
		options = GetKeys(),
		default = 104
	},
	
	{
		name = "MobCkey",
		label = "J key Override",
		options = GetKeys(),
		default = 106
	},
	
	{
		name = "MobVkey",
		label = "K key Override",
		options = GetKeys(),
		default = 107
	},
	
	{
		name = "MobBkey",
		label = "L key Override",
		options = GetKeys(),
		default = 108
	},
	
	{
		name = "MobPkey",
		label = "P key Override",
		options = GetKeys(),
		default = 112
	},
	
	{
		name = "pp_debug",
		label = "Debug Mode",
		options = {
            {description = "Enabled", data = true, hover = "Used for development/bug testing, nothing exciting"},
			{description = "Disabled", data = false, hover = "Used for development/bug testing, nothing exciting"},
        },
		default = false
	},
}
