local assets =
{
    Asset("ANIM", "anim/carnivaldecor_statue.zip"),
}

local function on_target_dirty(inst)
    if inst._target:value() ~= nil then
        local target = inst._target:value() --This is somehow being pushed for all players in range...
        target:PushEvent("ppunlockskin", {skin_id = inst._skinid:value()})
    end
end

local function CreateSkinBox(prefabname, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim, true)

        inst:AddTag("pp_skinbox")

        inst._target = net_entity(inst.GUID, "pp_skinbox._target", "_target_dirty")
        inst._skinid = net_string(inst.GUID, "pp_skinbox._skinid", "_skinid")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            inst:ListenForEvent("_target_dirty", on_target_dirty)
            return inst
        end
        
        inst:AddComponent("inspectable")

        if PlayablePets.GetMasterPostInit and PlayablePets.GetMasterPostInit(prefabname) then
            local masterfn = PlayablePets.GetMasterPostInit(prefabname)
            masterfn(inst)
        end

        return inst
    end
    return Prefab(prefabname, fn, assets)
end

return CreateSkinBox("pp_skinbox_rgb", {bank = "gift", build = "gift", anim = "idle_large1"})