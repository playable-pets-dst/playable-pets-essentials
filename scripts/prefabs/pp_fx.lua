local prefabs = {}

local assets = {

}
local function SetFace(inst, type)
	if type == 6 then
		inst.Transform:SetSixFaced()
	elseif type == 4 then
		inst.Transform:SetFourFaced()
	elseif type == 2 then
		inst.Transform:SetTwoFaced()
	end
end
local function MakeFX(name, data)--bank, build, anim, animloop, isflat, glow, multc, bloom)
    local function fn()
        local inst = CreateEntity()
        ------------------------------------------
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()
        ------------------------------------------
        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim..(data.rvar and tostring(math.random(1,4)) or ""))
		if data.multc then
			local multc = data.multc
			inst.AnimState:SetMultColour(multc[1]/255, multc[2]/255, multc[3]/255, multc[4])
		end
		if data.addcolour then
			local addcolour = data.addcolour
			inst.AnimState:SetAddColour(addcolour[1]/255, addcolour[2]/255, addcolour[3]/255, addcolour[4])
		end
		if data.animloop then
			inst.AnimState:PushAnimation(data.animloop, true)
		end
		if data.foffset then
			inst.AnimState:SetFinalOffset(data.foffset)
		end
		if data.scale then
			inst.AnimState:SetScale(data.scale[1], data.scale[2])
		end
		if data.hidesymbols then
			for i, v in ipairs(data.hidesymbols) do
				inst.AnimState:HideSymbol(v)
			end
		end
		if data.facetype then
			SetFace(inst, data.facetype)
		end
		------------------------------------------
        inst:AddTag("FX")
		inst:AddTag("NOCLICK")
		------------------------------------------
		if data.isflat then
			--ideally we want them to just be under characters and above everything else
			inst.AnimState:SetRayTestOnBB(true)
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder(3)
		end
		------------------------------------------
		if data.order then
			inst.AnimState:SetSortOrder(data.order)
		end
		------------------------------------------
		if data.bloom then
			inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
		end
		------------------------------------------
		if data.glow then
			inst.AnimState:SetLightOverride(data.glow)
		end
		------------------------------------------
		if data.sound then
			inst.SoundEmitter:PlaySound(data.sound)
		end
		------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end
        ------------------------------------------
		inst.persists = false
		------------------------------------------
		if data.noloop then
			inst:ListenForEvent("animqueueover", inst.Remove)
		end
		------------------------------------------
		if data.fadein then
			inst:AddComponent("colourtweener")
			inst.AnimState:SetMultColour(0, 0, 0, 0)
			inst:DoTaskInTime(0, function(inst)
				inst.components.colourtweener:StartTween(data.multc and {data.multc[1]/255, data.multc[2]/255, data.multc[3]/255, data.multc[4]} or {1,1,1,1}, data.fadein)
			end)
		end
		------------------------------------------
		if data.fadeout then
			inst:AddComponent("colourtweener")
			inst:DoTaskInTime(0, function(inst)
				inst.components.colourtweener:StartTween({0,0,0,0}, data.fadeout, inst.Remove)
			end)
		end
		------------------------------------------
        return inst
    end

    return Prefab(name, fn, assets)
end

return MakeFX("icespikep", {bank = "deerclops_icespike", build = "deerclops_icespike", anim = "spike", rvar = 4, sound = "dontstarve/creatures/deerclops/ice_small", scale = {1.2, 2}, noloop = true}),
MakeFX("brrrd_cough_fxp", {bank = "fire_puff", build = "brrrd_fire_puff", anim = "cough", facetype = 6, noloop = true, glow = 1}),
MakeFX("brrrd_transform_fxp", {bank = "fire_puff", build = "brrrd_fire_puff", anim = "transform", noloop = true, glow = 1, sound = "dontstarve/common/fireBurstLarge"}),
MakeFX("brrrd_taunt_fxp", {bank = "fire_puff", build = "brrrd_fire_puff", anim = "taunt", facetype = 6, noloop = true, glow = 1, sound = "dontstarve/common/fireBurstSmall"}),
MakeFX("pp_ground_fx", {bank = "bearger_ring_fx", build = "bearger_ring_fx", anim = "idle", noloop = true, isflat = true}),
MakeFX("pp_transform_fx", {bank = "collapse", build = "structure_collapse_fx", anim = "collapse_small", noloop = true, multc = {125, 78, 191, 1}, sound = "pm/creatures/duplighost/transform", scale = {2, 2}})

