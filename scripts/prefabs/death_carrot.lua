local assets =
{
    Asset("ANIM", "anim/flies.zip"),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
	end
end

local function spin(inst, time)
    inst.entity:AddSoundEmitter()
    inst.AnimState:PlayAnimation("spin_pre")
    inst.AnimState:PushAnimation("spin_loop",true)
    inst.components.timer:StartTimer("spin", 3 *(math.random()*2) + 2)
    inst.SoundEmitter:PlaySound("yotr_2023/common/carrot_spin", "spin_lp")
end

local function FindPlayer(inst, x, z)
    --local fx = SpawnPrefab("carrot")
    --fx.Transform:SetPosition(x, 0, z)
    local ents = TheSim:FindEntities(x, 0, z, 2, {"player"}, {"INLIMBO", "playerghost"})
    if #ents > 0 then 
        for i, v in ipairs(ents) do
            print(v:GetDisplayName())
            if v and v:IsValid() and v.components.health and not v.components.health:IsDead() then
                return v
            end
        end
    end
end

local function timerdone(inst)
    inst.Transform:SetEightFaced()
    inst.Transform:SetRotation(math.random()*360)
    inst.AnimState:PlayAnimation("spin_pst")
    inst.components.activatable.inactive = true
    inst.SoundEmitter:PlaySound("yotr_2023/common/carrot_spin_pst")
    inst.SoundEmitter:KillSound("spin_lp")

    local fx = SpawnPrefab("carrot_spinner")
    inst:AddChild(fx)
    inst:DoTaskInTime(20*FRAMES, function(inst)
        --generate a line of small find ents, if a player is found, kill them.
        for i = 1, 6 do
            local posx, posy, posz = inst.Transform:GetWorldPosition()
            local angle = -inst.Transform:GetRotation() * DEGREES
            local offset = 2*i
            local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
            local player = FindPlayer(inst, targetpos.x, targetpos.z)
            if player then
                player.components.health:DoDelta(-9999, nil, "death_carrot")
                local fx = SpawnPrefab("fossilspike")
                RemovePhysicsColliders(fx)
                ShakeIfClose(inst)
                fx.Transform:SetPosition(player:GetPosition():Get())
                fx.AnimState:SetMultColour(0, 0, 0, 1)
                fx.AnimState:SetScale(2, 2)
                break
            end
        end
    end)
end

local function GetActivateVerb()
    return "SPIN"
end    

local function OnActivateSpin(inst)
    inst:Spin()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	MakeInventoryFloatable(inst, "med", 0.05, 0.68)

    inst.AnimState:SetBank("carrot")
	inst.AnimState:SetBuild("carrot")
	inst.AnimState:PlayAnimation("idle")

    inst.AnimState:SetSaturation(0)
    local mc = 0.5
    inst.AnimState:SetMultColour(mc, mc, mc, 1)

    inst.GetActivateVerb = GetActivateVerb

    inst:AddTag("death_carrot")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst.Spin = spin
    inst:AddComponent("timer")
    inst:ListenForEvent("timerdone", timerdone)
    inst:AddComponent("activatable")
    inst.components.activatable.OnActivate = OnActivateSpin
    inst.components.activatable.quickaction = true  

    local flies = SpawnPrefab("fliesp")
    flies.AnimState:SetScale(0.5, 0.5)
    inst:AddChild(flies)

    return inst
end

local function flies_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("flies")
    inst.AnimState:SetBuild("flies")

    inst.AnimState:PlayAnimation("swarm_pre")

    inst:AddTag("NOCLICK")
    inst:AddTag("DECOR")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst.AnimState:PushAnimation("swarm_loop", true)

    return inst
end

return Prefab("death_carrot", fn, assets),
    Prefab("fliesp", flies_fn)