local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "prefabnamehere"

local assets = 
{
	
}

local prefabs = 
{	
	"chest_mimic_revealed",
    "chest_mimic_ruinsspawn_tracker",
    "shadowheart_infused",
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE,
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
    scale        = 1,
	
	bank         = "chest_mimic",
	build        = "chest_mimic",
    	
	stategraph   = "SG"..prefabname,
	minimap      = prefabname..".tex",	
}


SetSharedLootTable(prefabname,
{
    --{"meat",  1.00},
})

local sounds = {
    --t  = "path",
}

local FORGE_STATS = MOB_TUNING.FORGE
--==============================================
--					Local Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		--inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	--data.isshiny = inst.isshiny or 0
end

--==============================================
--					Reforged
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPROT_FORGE.SPIDER_MOON)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:AddComponent("buffable")
	--inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
        if ThePlayer then
            inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
        end
    end)
	----------------------------------
	--Tags--

	----------------------------------
    inst:ListenForEvent("phasechanged", PlayablePets.SetNightVision)	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
    inst.sounds = sounds --sounds need to exist before stategraph
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0, 0) --fire, acid, poison
	----------------------------------
	--Variables	
    inst.shouldwalk = true
    ----------------------------------
    --Playable Pets
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SLEEP, PP_ABILITIES.PP_SLEEP)
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL1, PP_ABILITIES.SKILL1)
	----------------------------------
	--Debuff Symbol
	local body_symbol = "symbolhere"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
    ----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .1)
    inst.DynamicShadow:SetSize(2.5, 1.5)
	inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function()
        PlayablePets.RevRestore(inst, mob) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)

    PlayablePets.SetCommonSaveAndLoad(inst, OnSave, OnLoad)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)