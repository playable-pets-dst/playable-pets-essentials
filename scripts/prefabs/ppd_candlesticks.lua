local prefabs = {}

local function MakeCandle(name, data)
    local assets = {
        Asset("ANIM", "anim/ppd_candlestick.zip"),
        Asset("ANIM", "anim/"..data.build..".zip"),
    }
    local function fn()
        local inst = CreateEntity()
        ------------------------------------------
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddLight()
        inst.entity:AddNetwork()
        ------------------------------------------
        inst.AnimState:SetBank(data.bank or "ppd_candlestick")
        inst.AnimState:SetBuild(data.build or "ppd_candlestick")
        inst.AnimState:PlayAnimation(data.anim, true)

        inst.Light:Enable(true)
        inst.Light:SetRadius(4)
        inst.Light:SetFalloff(1.6)
        inst.Light:SetIntensity(0.25)
        inst.Light:SetColour(1, 1, 1)
        inst.AnimState:SetSymbolLightOverride("light", 1)
		if data.color then
			local multc = data.color
			inst.AnimState:SetSymbolMultColour("stick", multc[1]/255, multc[2]/255, multc[3]/255, multc[4])
		end
        if data.lcolor then
            local multlc = data.lcolor
			inst.AnimState:SetSymbolMultColour("light", multlc[1]/255, multlc[2]/255, multlc[3]/255, multlc[4])
            inst.Light:SetColour(multlc[1]/255, multlc[2]/255, multlc[3]/255)
        end
		if data.addcolour then
			local addcolour = data.addcolour
			inst.AnimState:SetAddColour(addcolour[1]/255, addcolour[2]/255, addcolour[3]/255, addcolour[4])
		end
		if data.animloop then
			inst.AnimState:PushAnimation(data.animloop, true)
		end
		if data.foffset then
			inst.AnimState:SetFinalOffset(data.foffset)
		end
		if data.scale then
			inst.AnimState:SetScale(data.scale[1], data.scale[2])
		end
		------------------------------------------
        inst:AddTag("FX")
		inst:AddTag("NOCLICK")
		------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        return inst
    end

    return Prefab(name, fn, assets)
end

return MakeCandle("ppd_candlestick_shadow", {lcolor = {255, 36, 65, 1}, anim = "active", build = "ppd_candlestick_shadow", scale = {1.2, 1.2}})

