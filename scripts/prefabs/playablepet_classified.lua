--------------------------------------------------------------------------
--Dependencies
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--Constants
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--Server interface
--------------------------------------------------------------------------

--------------------------------------------------------------------------

--------------------------------------------------------------------------

local function fn()
    local inst = CreateEntity()

    if TheWorld.ismastersim then
        inst.entity:AddTransform() --So we can follow parent's sleep state
    end
    inst.entity:AddNetwork()
    inst.entity:Hide()
    inst:AddTag("CLASSIFIED")

    --Variables for tracking local preview state;
    --Whenever a server sync is received, all local dirty states are reverted
    inst._refreshtask = nil
    inst.ignoreoverflow = false

    --Network variables
    inst.ability_slots = net_int(inst.GUID, "playablepet.abilityslots", "abilityslotsdirty")


    inst._active = net_entity(inst.GUID, "playablepet._active", "activedirty")
    inst._abilities = {}
    inst._passives = {}
    inst._slottasks = nil

    for i = 1, 5 do
        inst._abilities[i]          = {}
        inst._abilities[i].name     = net_string(inst.GUID, "playablepet._abilities["..tostring(i).."].name", "playablepet_ability_dirty")
        inst._abilities[i].cooldown = net_float(inst.GUID, "playablepet._abilities["..tostring(i).."].cooldown", "playablepet_ability_dirty")
        inst._abilities[i].enabled  = net_bool(inst.GUID, "playablepet._abilities["..tostring(i).."].enabled", "playablepet_ability_dirty")
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        --Client interface
        --inst.IsHolding = IsHolding

        --Delay net listeners until after initial values are deserialized
        --inst:DoStaticTaskInTime(0, RegisterNetListeners)
        return inst
    end

    inst.persists = false

    return inst
end

return Prefab("playablepet_classified", fn)