local assets =
{
	Asset("ANIM", "anim/portal_adventure.zip"),
	--Asset("MINIMAP_IMAGE", "portal"),
}

--------------------------------------------------------------------------
-- Camera Functions
--------------------------------------------------------------------------
local function TeamUpdateDirty(inst)
    --print("Team Update Dirty Ran")
    --These are made accessible for clients for huds.
    inst.teams = {
        team1 = {
            --slot1 = inst._team1_slot1:value(),
            --slot2 = inst._team1_slot2:value(),
        },
        team2 = {
            --slot1 = inst._team2_slot1:value(),
            --slot2 = inst._team2_slot2:value(),
        }
    }

    for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
        inst.teams.team1["slot"..i] = inst["_team1_slot"..i]:value()
    end
    for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
        inst.teams.team2["slot"..i] = inst["_team1_slot"..i]:value()
    end
end

local function OnCameraFocusDirty(inst)
    if inst._camerafocus:value() then
        if inst:HasTag("active") then
            --default battle view
            TheFocalPoint.components.focalpoint:StartFocusSource(inst, nil, nil, 60, 60, 3)
            TheCamera:SetDistance(30)
            TheCamera:SetControllable(false)
            TheCamera:SetHeadingTarget(0)
        end
    else
        TheFocalPoint.components.focalpoint:StopFocusSource(inst)
        TheCamera:SetControllable(true)
    end
end

local function EnableCameraFocus(inst, enable)
    if enable ~= inst._camerafocus:value() then
        inst._camerafocus:set(enable)
        if not TheNet:IsDedicated() then
            OnCameraFocusDirty(inst)
        end
    end
end

local function DoUpdateTeams(inst)
    if inst.components.pm_battlemanager then
        local teams = inst.components.pm_battlemanager:GetAllTeams()
        for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
            inst["_team1_slot"..i]:set(teams.team1[i])
        end
        for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
            inst["_team2_slot"..i]:set(teams.team2[i])
        end
        if not TheNet:IsDedicated() then
            TeamUpdateDirty(inst, teams)
        end
    end
end

local function OnUnitEndTurn(inst, data)
    inst.components.pm_battlemanager:EndAction()
    inst.components.pm_battlemanager:StartNextUnitTurn(data.prevactor)
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()
    --

    MakeObstaclePhysics(inst, 1)

    inst.MiniMapEntity:SetIcon("portal.png")

    inst.AnimState:SetBank("portal_adventure")
    inst.AnimState:SetBuild("portal_adventure")
    inst.AnimState:PlayAnimation("idle_off", true)
    inst.AnimState:SetMultColour(1, 1, 1, 0.1)

    inst._camerafocus = net_bool(inst.GUID, "pm_stage._camerafocus", "camerafocusdirty")
    --[[
    inst._team1_slot1 = net_entity(inst.GUID, "pm_stage.team1.slot1", "pm_stage_teamupdate_dirty")
    inst._team1_slot2 = net_entity(inst.GUID, "pm_stage.team1.slot2", "pm_stage_teamupdate_dirty")
    inst._team2_slot1 = net_entity(inst.GUID, "pm_stage.team2.slot1", "pm_stage_teamupdate_dirty")
    inst._team2_slot2 = net_entity(inst.GUID, "pm_stage.team2.slot2", "pm_stage_teamupdate_dirty")]]

    for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
        inst["_team1_slot"..i] = net_entity(inst.GUID, "pm_stage.team1.slot"..i, "pm_stage_teamupdate_dirty")
    end
    for i = 1, TUNING.PM.MAX_PARTY_SLOTS do
        inst["_team2_slot"..i] = net_entity(inst.GUID, "pm_stage.team2.slot"..i, "pm_stage_teamupdate_dirty")
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst:ListenForEvent("pm_stage_teamupdate_dirty", TeamUpdateDirty)
        inst:ListenForEvent("camerafocusdirty", OnCameraFocusDirty)
        return inst
    end

    inst.persists = false

    inst.EnableCameraFocus = EnableCameraFocus
    inst.DoUpdateTeams = DoUpdateTeams

    inst:AddComponent("pm_battlemanager")
    inst:ListenForEvent("pm_unit_endturn", OnUnitEndTurn)

    return inst
end

return Prefab("pm_stage", fn, assets)
