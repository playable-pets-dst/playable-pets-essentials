local assets =
{
	Asset("ANIM", "anim/hound_base.zip"),
	Asset("ANIM", "anim/pp_skeleton_giant.zip"),
    Asset("ANIM", "anim/lc_lynch.zip"),
}

local function getdesc(inst, viewer)
    if inst.char ~= nil and not viewer:HasTag("playerghost") then
        local mod = GetGenderStrings(inst.char)
        local desc = GetDescription(viewer, inst, mod)
        local name = inst.playername or STRINGS.NAMES[string.upper(inst.char)]

        --no translations for player killer's name
        if inst.pkname ~= nil then
            return string.format(desc, name, inst.pkname)
        end

        --permanent translations for death cause
        if inst.cause == "unknown" then
            inst.cause = "shenanigans"
        elseif inst.cause == "moose" then
            inst.cause = math.random() < .5 and "moose1" or "moose2"
        end

        --viewer based temp translations for death cause
        local cause =
            inst.cause == "nil"
            and (viewer == "waxwell" and
                "charlie" or
                "darkness")
            or inst.cause

        return string.format(desc, name, STRINGS.NAMES[string.upper(cause)] or STRINGS.NAMES.SHENANIGANS)
    end
end

local function decay(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst:Remove()
    SpawnPrefab("ash").Transform:SetPosition(x, y, z)
    SpawnPrefab("collapse_small").Transform:SetPosition(x, y, z)
end

local function SetSkeletonDescription(inst, char, playername, cause, pkname, userid)
    inst.char = char
    inst.playername = playername
	inst.userid = userid
    inst.pkname = pkname
    inst.cause = (pkname == nil and cause) and cause:lower() or nil
    inst.components.inspectable.getspecialdescription = getdesc
end

local function SetSkeletonAvatarData(inst, client_obj)
    inst.components.playeravatardata:SetData(client_obj)
end

local function onhammered(inst)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("rock")
    inst:Remove()
end

local function onsave(inst, data)
    data.anime = inst.anime
end

local function onload(inst, data)
    if data ~= nil and data.anime ~= nil then
        inst.anime = data.anime
        inst.AnimState:PlayAnimation(inst.anime)
    end
end

local function onsaveplayer(inst, data)
    onsave(inst, data)

    data.char = inst.char
    data.playername = inst.playername
	data.userid = inst.userid
    data.pkname = inst.pkname
    data.cause = inst.cause
    if inst.skeletonspawntime ~= nil then
        local time = GetTime()
        if time > inst.skeletonspawntime then
            data.age = time - inst.skeletonspawntime
        end
    end
end

local function onloadplayer(inst, data)
    onload(inst, data)

    if data ~= nil and data.char ~= nil and (data.cause ~= nil or data.pkname ~= nil) then
        inst.char = data.char
        inst.playername = data.playername --backward compatibility for nil playername
		inst.userid = data.userid
        inst.pkname = data.pkname --backward compatibility for nil pkname
        inst.cause = data.cause
        if inst.components.inspectable ~= nil then
            inst.components.inspectable.getspecialdescription = getdesc
        end
        if data.age ~= nil and data.age > 0 then
            inst.skeletonspawntime = -data.age
        end

        if data.avatar ~= nil then
            --Load legacy data
            inst.components.playeravatardata:OnLoad(data.avatar)
        end
    end
end

local function MakeSkeleton(name, data)
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()
		inst.entity:AddSoundEmitter()

		MakeSmallObstaclePhysics(inst, data.physoverride or 0.25)
		MakeInventoryFloatable(inst, data.floatsize or "med", 0.05, 0.68)

		inst.AnimState:SetBank(data.bank)
   		inst.AnimState:SetBuild(data.build)

		inst:AddTag("playerskeleton")
        inst:AddTag("bone")

   		inst:AddComponent("playeravatardata")
   		inst.components.playeravatardata:AddPlayerData(true)

		inst:SetPrefabNameOverride(data.prefaboverride or "skeleton")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

        local anim = data.anim_prefix or "piece"
        if data.max_animnum and data.min_animnum then
		    inst.anime = anim..tostring(math.random(data.min_animnum, data.max_animnum))
        else
            inst.anime = data.animoverride or anim..tostring(math.random(data.animnum))
        end
		inst.AnimState:PlayAnimation(inst.anime, not data.animloop)

        if data.animloop then
            inst.AnimState:PushAnimation(data.animloop, true)
        end

		inst:AddComponent("inspectable")
    	inst.components.inspectable:RecordViews()

   		inst:AddComponent("lootdropper")
    	inst.components.lootdropper:SetChanceLootTable('skeleton')

		inst:AddComponent("workable")
   		inst.components.workable:SetWorkAction(data.actionoverride or ACTIONS.HAMMER)
   		inst.components.workable:SetWorkLeft(data.workleft or 3)
    	inst.components.workable:SetOnFinishCallback(onhammered)

		inst.OnLoad = onloadplayer
		inst.OnSave = onsaveplayer
		inst.SetSkeletonDescription = SetSkeletonDescription
		inst.SetSkeletonAvatarData = SetSkeletonAvatarData
		inst.Decay = decay
		inst.skeletonspawntime = GetTime()
		TheWorld:PushEvent("ms_skeletonspawn", inst)

		inst:DoTaskInTime(0.5, function(inst)
			if not PlayablePets.IsAboveLand(inst:GetPosition()) then
				inst:Remove()
			end
		end)

		return inst
	end
	return Prefab("pp_skeleton_"..name, fn, assets)
end

return MakeSkeleton("generic", {bank = "houndbase", build = "hound_base", animnum = 3}),
MakeSkeleton("giant", {bank = "pp_skeleton_giant", build = "pp_skeleton_giant", animnum = 3, workleft = 6, physoverride = 1, floatsize = "large"}),
MakeSkeleton("lynch", {bank = "lc_lynch", build = "lc_lynch", animoverride = "Lynch_Start", animloop = "Lynch_Idle"}),
MakeSkeleton("pig", {bank = "skeleton", build = "skeletons", anim_prefix = "idle", min_animnum = 7, max_animnum = 8})
