local assets =
{
   Asset("ANIM", "anim/wadebag.zip"),
}

local function SetValue(inst, value)
	local newvalue = value or inst.currencyvalue
	inst.currencyvalue = newvalue
	inst.components.edible.hungervalue = newvalue
	inst.components.named:SetName("Bag of "..tostring(inst.currencyvalue).." Oincs")
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.currencyvalue = data.currencyvalue or 1
	end
	inst:DoTaskInTime(0, SetValue)
end

local function OnSave(inst, data)
	data.currencyvalue = inst.currencyvalue or 1
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
		
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		MakeInventoryFloatable(inst)
	end	

    inst.AnimState:SetBank("wadebag")
	inst.AnimState:SetBuild("wadebag")
	inst.AnimState:PlayAnimation("idle_loop")

    inst:AddTag("hamlet_coin")
	inst:AddTag("currency")
	inst:AddTag("ppcurrency")

    MakeInventoryPhysics(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
		
	inst.currencyvalue = 1

	inst:AddComponent("edible")
	inst.components.edible.foodtype = "ELEMENTAL"
	inst.components.edible.hungervalue = 1

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/pp_essentials_icons.xml"

	inst:AddComponent("tradable")

	inst:AddComponent("named")
	inst.components.named:SetName("Bag of "..tostring(inst.currencyvalue).." Oincs")

	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	inst.SetValue = SetValue

    return inst
end

return Prefab("wadebag", fn, assets)
