local assets =
{
    Asset("ANIM", "anim/glommer.zip"),
    Asset("SOUND", "sound/glommer.fsb"),
}

local prefabs =
{
    "glommerfuel",
    "glommerwings",
    "monstermeat",
}

local brain = require("brains/gay_glommerbrain")

SetSharedLootTable('gay_glommerp',
{
    {'pp_skinbox_rgb',             1.00},
})

local WAKE_TO_FOLLOW_DISTANCE = 14
local SLEEP_NEAR_LEADER_DISTANCE = 7

local function retargetfn(inst)
    return FindEntity(
                inst,
                5,
                function(guy)
                    return guy and guy:IsValid() and guy.components.combat and guy.prefab ~= inst.prefab
                end,
                { "_combat"}, --See entityreplica.lua (re: "_combat" tag)
                {"notarget"}
            )
        or nil
end

local function KeepTargetFn(inst, target)
    return inst.components.combat:CanTarget(target)
end

local function OnEntitySleep(inst)
    inst:Remove()
end

local function OnSave(inst, data)
    
end

local function OnLoad(inst, data)

end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.DynamicShadow:SetSize(1, .5)
    inst.Transform:SetFourFaced()

    MakeGhostPhysics(inst, 1, .5)

    inst.AnimState:SetBank("glommer")
    inst.AnimState:SetBuild("glommer")
    inst.AnimState:PlayAnimation("idle_loop")

    inst.AnimState:SetDeltaTimeMultiplier(1.5)

    inst:AddTag("glommer")
    inst:AddTag("hostile") --makes it attackable.
    inst:AddTag("flying")
    inst:AddTag("ignorewalkableplatformdrowning")
    inst:AddTag("cattoyairborne")

    MakeInventoryFloatable(inst, "med")

    inst.AnimState:SetScale(0.5, 0.5)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(1000)

    inst:AddComponent("combat")
    inst.components.combat:SetRetargetFunction(0.3, retargetfn)
	inst.components.combat:SetKeepTargetFunction(KeepTargetFn)
    inst:AddComponent("knownlocations")
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('gay_glommerp')

    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = 10
    inst.components.locomotor.pathcaps = {allowocean = true}

    inst:SetBrain(brain)
    inst:SetStateGraph("SGglommer")

    PlayablePets.MakeRainbow(inst)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnEntitySleep = OnEntitySleep

    MakeHauntablePanic(inst)

    return inst
end

return Prefab("gay_glommerp", fn, assets, prefabs)
