local prefabs = {}
local structures = {}

---------------------------------------------------------
--Save/Load Functions
---------------------------------------------------------
local function OnSave(inst, data)
    data.anime = inst.anime
end

local function OnLoad(inst, data)
    if data ~= nil and data.anime ~= nil then
        inst.anime = data.anime
        inst.AnimState:PlayAnimation(inst.anime)
    end
end

--------------------------------------------------------
--Workable Stuff
--------------------------------------------------------
local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        --inst.AnimState:PushAnimation("idle")
    end
end

--------------------------------------------------------
--Automated FNs
--------------------------------------------------------

local function SetFace(inst, ftype)
	if ftype == "six" then
		inst.Transform:SetSixFaced()
	elseif ftype == "four" then
		inst.Transform:SetFourFaced()
	else
		inst.Transform:SetTwoFaced()
	end
end

local function BuildStructure(name, data)--bank, build, anim, animloop, isflat, glow, multc, bloom)
    local assets = {
        Asset("ANIM", "anim/"..(data.asset or data.build)..".zip"),
    }
    local function fn()
        local inst = CreateEntity()
        ------------------------------------------
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()
        ------------------------------------------
        MakeObstaclePhysics(inst, data.phys_rad or .3)

        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        local var = data.rvar and math.random(1, data.rvar) or nil
        inst.AnimState:PlayAnimation(data.anim..(var and tostring(var) or "")) --gets overwritten below.

		if data.multc then
			local multc = data.multc
			inst.AnimState:SetMultColour(multc[1]/255, multc[2]/255, multc[3]/255, multc[4])
		end
		if data.addcolour then
			local addcolour = data.addcolour
			inst.AnimState:SetAddColour(addcolour[1]/255, addcolour[2]/255, addcolour[3]/255, addcolour[4])
		end
		if data.animloop then
			inst.AnimState:PushAnimation(data.animloop, true)
		end
		if data.foffset then
			inst.AnimState:SetFinalOffset(data.foffset)
		end
		if data.scale then
			inst.AnimState:SetScale(data.scale[1], data.scale[2])
		end
		if data.hidesymbols then
			for i, v in ipairs(data.hidesymbols) do
				inst.AnimState:HideSymbol(v)
			end
		end
		if data.facetype then
			SetFace(inst, data.facetype)
		end
		------------------------------------------
        inst:AddTag("structure")
        if data.tags then
            for i, v in ipairs(data.tags) do
                inst:AddTag(v)
            end
        end
		------------------------------------------
		if data.isflat then
			--ideally we want them to just be under characters and above everything else
			inst.AnimState:SetRayTestOnBB(true)
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder(3)
		end
		------------------------------------------
		if data.order then
			inst.AnimState:SetSortOrder(data.order)
		end
		------------------------------------------
		if data.bloom then
			inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
		end
		------------------------------------------
		if data.glow then
			inst.AnimState:SetLightOverride(data.glow)
		end
		------------------------------------------
		if data.sound then
			inst.SoundEmitter:PlaySound(data.sound)
		end
		------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        inst.anime = data.anim..(var and var or "")
        inst.AnimState:PlayAnimation(inst.anime)
        ------------------------------------------
        --Components
        inst:AddComponent("inspectable")
		------------------------------------------
		if data.noloop then
			inst:ListenForEvent("animqueueover", inst.Remove)
		end
		------------------------------------------
		if data.fadein then
			inst:AddComponent("colourtweener")
			inst.AnimState:SetMultColour(0, 0, 0, 0)
			inst:DoTaskInTime(0, function(inst)
				inst.components.colourtweener:StartTween(data.multc and {data.multc[1]/255, data.multc[2]/255, data.multc[3]/255, data.multc[4]} or {1,1,1,1}, data.fadein)
			end)
		end
		------------------------------------------
		if data.fadeout then
			inst:AddComponent("colourtweener")
			inst:DoTaskInTime(0, function(inst)
				inst.components.colourtweener:StartTween({0,0,0,0}, data.fadeout, inst.Remove)
			end)
		end
		------------------------------------------
        if data.workable then
            inst:AddComponent("lootdropper")
            inst:AddComponent("workable")
            inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
            inst.components.workable:SetWorkLeft(data.workleft or 3)
            inst.components.workable:SetOnFinishCallback(onhammered)
            inst.components.workable:SetOnWorkCallback(onhit)
        end

        inst.OnLoad = OnLoad
        inst.OnSave = OnSave
        return inst
    end

    return Prefab(name.."p", fn, assets)
end

local function MakeStructure(name, data)
    table.insert(structures, BuildStructure(name, data))
    table.insert(structures, MakePlacer(name.."p_placer", data.bank, data.build, data.anim..(data.rvar and "1" or ""), nil, nil, nil, nil, nil, data.facetype))
end

MakeStructure("cotl_midas_statue", {bank = "cotl_midas_statue", build = "cotl_midas_statue", anim = "", rvar = 11, bloom = true, workable = true})
MakeStructure("cotl_goldpile_small", {bank = "cotl_goldpile", build = "cotl_goldpile", anim = "small", rvar = 3, bloom = true, workable = true})
MakeStructure("cotl_goldpile_large", {bank = "cotl_goldpile", build = "cotl_goldpile", anim = "large", rvar = 2, bloom = true, workable = true})
MakeStructure("cotl_cairn", {bank = "cairn", build = "cairn", anim = "f", rvar = 3, workable = true, workleft = 1})
MakeStructure("cotl_godstatue_leshy", {bank = "cotl_godstatue", build = "cotl_godstatue", anim = "idle_loop", workable = true, workleft = 6}) --this is leshy
MakeStructure("cotl_godstatue_heket", {bank = "cotl_godstatue", build = "cotl_godstatue_heket", anim = "idle_loop", workable = true, workleft = 6})
MakeStructure("cotl_godstatue_kallamar", {bank = "cotl_godstatue", build = "cotl_godstatue_kallamar", anim = "idle_loop", workable = true, workleft = 6})
MakeStructure("cotl_godstatue_shamura", {bank = "cotl_godstatue", build = "cotl_godstatue_shamura", anim = "idle_loop", workable = true, workleft = 6})

return unpack(structures)

