local assets =
{
    Asset("ANIM", "anim/poison_antidote.zip"),
	 Asset("ANIM", "anim/venom_gland.zip"),
}

local function OnHeal(inst, target)
if target and target.ispoisoned ~= nil then
	target.ispoisoned = nil
	if not target.poisonimmune then
		target.poisonimmune = true 
		if target.poisonimmune_task then
			target.poisonimmune_task:Cancel()
			target.poisonimmune_task = nil
		end
		target.poisonimmune_task = target:DoTaskInTime(480, function(inst) inst.poisonimmune = nil end) --makes person immune for a whole day.
	end
	if target.components.sanity then
		target.components.sanity:DoDelta(20, false)
		end	
	end
end

local function OnHealGland(inst, target)
	if target and target.ispoisoned ~= nil then
		target.ispoisoned = nil
	end
	if target.components.health then
		target.components.health:DoDelta(-20, false)
	end
	if target.components.sanity then
		target.components.sanity:DoDelta(-20, false)
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
	
	MakeInventoryFloatable(inst, "med", 0.05, 0.68)

    inst.AnimState:SetBank("poison_antidote")
	inst.AnimState:SetBuild("poison_antidote")
	inst.AnimState:PlayAnimation("idle")

    inst:AddTag("medicine")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/pp_essentials_icons.xml"
	
    inst:AddComponent("stackable")
    inst:AddComponent("tradable")
	
    inst:AddComponent("healer")
	inst.components.healer.onhealfn = OnHeal
    

    return inst
end

-------
--Venom Gland

local function gland_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("venom_gland")
	inst.AnimState:SetBuild("venom_gland")
	inst.AnimState:PlayAnimation("idle")
	
	MakeInventoryFloatable(inst, "med", 0.05, 0.68)

    inst:AddTag("cattoy")
    inst:AddTag("venomgland")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/pp_essentials_icons.xml"
	
	--[[
	inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.MEAT
    inst.components.edible.healthvalue = -20
    inst.components.edible.hungervalue = TUNING.CALORIES_TINY
	inst.components.edible.sanityvalue = -20]]
	
	MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
    MakeSmallPropagator(inst)
	
    inst:AddComponent("stackable")
    inst:AddComponent("tradable")

	inst:AddComponent("healer")
	inst.components.healer.onhealfn = OnHealGland    

    return inst
end

local function skin_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("venom_gland")
	inst.AnimState:SetBuild("venom_gland")
	inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetMultColour(1, 0, 0, 1)
	
	MakeInventoryFloatable(inst, "med", 0.05, 0.68)

    inst:AddTag("cattoy")
    inst:AddTag("venomgland")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/pp_essentials_icons.xml"
	
	--[[
	inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.MEAT
    inst.components.edible.healthvalue = -20
    inst.components.edible.hungervalue = TUNING.CALORIES_TINY
	inst.components.edible.sanityvalue = -20]]
	
	MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
    MakeSmallPropagator(inst)
	
    inst:AddComponent("stackable")
    inst:AddComponent("tradable")

	inst:AddComponent("healer")
	inst.components.healer.onhealfn = OnHealGland    

    return inst
end

return Prefab("antidotep", fn, assets),
Prefab("venomglandp", gland_fn, assets)