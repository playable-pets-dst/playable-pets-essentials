local assets =
{
    Asset("ANIM", "anim/pignote_old.zip"),
}

local function OnReadBook(inst, doer)
	doer:ShowPopUp(POPUPS.pig_note, true, inst)
end

local function MakeNote(prefabname, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim, false)

        inst:AddTag("pignote")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
        
        inst:AddComponent("inspectable")

        inst:AddComponent("named")
        inst.components.named:SetName(data.name or "Note")

        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem.atlasname = "images/pignote_icons.xml"
        inst.components.inventoryitem.imagename = data.text_data and data.build.."_written" or data.build

        inst:AddComponent("simplebook")
	    inst.components.simplebook.onreadfn = OnReadBook

        inst:AddComponent("pignote")
        if data.text_data then
            inst.components.pignote:SetData(data.text_data)
        end

        return inst
    end
    return Prefab("pignote_"..prefabname, fn, assets)
end

return MakeNote("pikoletter", {bank = "pignote", build = "pignote_old", anim = "idle_loop", name = "Old Note", text_data = PIGNOTE_DATA.PIKO_LETTER}),
    MakeNote("project01", {bank = "pignote", build = "pignote_old", anim = "idle_loop", name = "Old Note", text_data = PIGNOTE_DATA.LOG1}),
    MakeNote("pioneerletter", {bank = "pignote", build = "pignote_old", anim = "idle_loop", name = "Formal Note", text_data = PIGNOTE_DATA.PIONEER_LETTTER})