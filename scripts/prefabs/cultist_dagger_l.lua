local assets =
{
    Asset("ANIM", "anim/cultist_dagger_l.zip"),
	Asset("ANIM", "anim/swap_cultist_dagger_l.zip"),
    Asset("ANIM", "anim/blood_jar_l.zip"),
}

local function OnKill(inst, data)
    if data.victim and data.victim:HasTag("player") then
        local pos = data.victim:GetPosition()
        local item = SpawnPrefab("blood_jar_l")
        inst.components.inventory:GiveItem(item)
    end
end

local function onequip(inst, owner)
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("equipskinneditem", inst:GetSkinName())
        owner.AnimState:OverrideItemSkinSymbol("swap_object", skin_build, "swap_spear", inst.GUID, "swap_spear")
    else
        owner.AnimState:OverrideSymbol("swap_object", "swap_cultist_dagger_l", "swap_cultist_dagger_l")
    end
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
    owner:ListenForEvent("killed", OnKill)
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("unequipskinneditem", inst:GetSkinName())
    end
    owner:RemoveEventCallback("killed", OnKill)
end

local function OnSpawn(inst)
    inst.AnimState:PlayAnimation("drop", false)
    inst.AnimState:PushAnimation("idle_pre", false)
    inst:DoTaskInTime(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/fossil_spike") end)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddSoundEmitter()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("cultist_dagger_l")
	inst.AnimState:SetBuild("cultist_dagger_l")
	inst.AnimState:PlayAnimation("idle_loop", false)

    inst:AddTag("cultist_dagger")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/cultist_icons_l.xml"

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.SPEAR_DAMAGE)
    -------

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.SPEAR_USES)
    inst.components.finiteuses:SetUses(TUNING.SPEAR_USES)

    inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst.OnSpawn = OnSpawn

    MakeHauntableLaunch(inst)   

    return inst
end

local function jarfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("blood_jar_l")
	inst.AnimState:SetBuild("blood_jar_l")
	inst.AnimState:PlayAnimation("idle_loop", false)

    inst:AddTag("cultist_blood")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/cultist_icons_l.xml"
    inst:AddComponent("stackable")

    MakeHauntableLaunch(inst)   

    return inst
end

return Prefab("cultist_dagger_l", fn, assets),
Prefab("blood_jar_l", jarfn, assets)