local SKIN = {}
-- These integer assignments are VERY important! Decided to make it more explicit
SKIN[1] = 1 --"Something Different"
SKIN[2] = 2 --"Contributor"
SKIN[3] = 3 --"Shadow"
SKIN[4] = 4 --
SKIN[5] = 5 --
SKIN[6] = 6 --"Community" 
SKIN[7] = 7
SKIN[8] = 8
SKIN[9] = 9

-- Might be a bit "too clever" - what is being done here is the skin names are given a numerical identifier, so the number and name are 'linked'
-- Unsure if skin values are saved but I may need to undo this if the numerical values are saved and someone swaps the name ordering
-- SKIN[1] is "formal" and SKIN["FORMAL"] is 1
--for idx, skinName in ipairs(SKIN) do
	--SKIN[skinName:upper()] = idx
--end

return SKIN