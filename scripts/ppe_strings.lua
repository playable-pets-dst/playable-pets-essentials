
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------
STRINGS.RECIPE_DESC.ANTIDOTEP = "Cleanse your body and soul of poison."
STRINGS.RECIPE_DESC.COTL_MIDAS_STATUEP = "Totally not alive statue of gold."
STRINGS.RECIPE_DESC.COTL_GOLDPILE_SMALLP = "Swim in your riches!"
STRINGS.RECIPE_DESC.COTL_GOLDPILE_LARGEP = "Swim in your riches!"
------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.NAMES.PP_HEART_ATTACK = "Heart Attack"
STRINGS.NAMES.PP_SACRIFICE = "Sacrifice"
STRINGS.NAMES.PP_FALLING = "Falling"
STRINGS.NAMES.PP_EXPLOSION = "Exploding"
STRINGS.NAMES.PP_SUICIDE = "Killing Themself"

STRINGS.NAMES.CULTIST_DAGGER_L = "Dagger of the Eye"
STRINGS.NAMES.BLOOD_JAR_L = "Jar of Pure Blood"
STRINGS.NAMES.MONSTER_WPN = "Mysterious Power"
STRINGS.NAMES.VENOMGLANDP = "Venom Gland"
STRINGS.NAMES.ANTIDOTEP = "Anti Venom"

--Decor
STRINGS.NAMES.COTL_MIDAS_STATUEP = "Strange Gold Statue"
STRINGS.NAMES.COTL_GOLDPILE_SMALLP = "Pile of Gold"
STRINGS.NAMES.COTL_GOLDPILE_LARGEP = STRINGS.NAMES.COTL_GOLDPILE_SMALLP

--experimental
STRINGS.NAMES.PM_BRISTLE = "Bristle"
STRINGS.NAMES.DEATH_CARROT = "Death Blighted Carrot"

--factions
STRINGS.NAMES.FACTION_GOLD = "Royal Order"
STRINGS.NAMES.FACTION_GREEN = "Merm Clan"
STRINGS.NAMES.FACTION_RED = "Shadow Cult"
STRINGS.NAMES.FACTION_BLUE = "Bright Cult"
STRINGS.NAMES.FACTION_PURPLE = "Undead"
STRINGS.NAMES.FACTION_NONE = "Nuetral"
STRINGS.NAMES.FACTION_ORANGE = "Rat Clan"
--Event strings
STRINGS.NAMES.PP_SKINBOX_RGB = "RGB Skin Box"
STRINGS.NAMES.GAY_GLOMMERP = "Gay Glommer"
------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------
STRINGS.CHARACTERS.GENERIC.DESCRIBE.DEATH_CARROT = 
{
	GENERIC = "...This is not a good idea.",
}
STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_MIDAS_STATUEP = 
{
	GENERIC = "WE'RE RICH!",
}
STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_MIDAS_STATUEP = 
{
	GENERIC = "Spooky.",
}
STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_GOLDPILE_SMALLP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_MIDAS_STATUEP
STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_GOLDPILE_LARGEP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_MIDAS_STATUEP

local function Capitilize(str)
    return (str:gsub("^%l", string.upper))
end

local godstatues = {"leshy", "heket", "kallamar", "shamura"}
for i, v in ipairs(godstatues) do
    local prefab = "cotl_godstatue_"..v.."p"
    STRINGS.NAMES[string.upper(prefab)] = Capitilize(v).." Idol"
    STRINGS.RECIPE_DESC[string.upper(prefab)] = "Erect an idol to "..Capitilize(v).."."
    STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(prefab)] = STRINGS.CHARACTERS.GENERIC.DESCRIBE.COTL_GODSTATUEP
end

return STRINGS