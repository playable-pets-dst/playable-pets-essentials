return {
    MAX_PARTY_SLOTS = 5,
    MAX_LEVEL = 999,
    MAXHP_CAP = 999,
    MAXFP_CAP = 999,
    MAXBP_CAP = 99,
    SKILLS = {
        DESTRUCTION_I = {
            fname = "Destruction",
            name = "destruction",
            fp_cost = 0,
            base_dmg = 1,
            atk_type = "melee",
            dmg_type = "spike",
            piercing = false, --should it ignore defense?
            on_hitfn = function(inst) end,
            ignore_list = { --might be used to ignore enemy "shields" like spears or spikes.
                "spike_melee",
            },
            icon = {
                atlas = "",
                tex = "",
            }
        },
        BONE_THROW = {
            fname = "Bone Throw",
            name = "bone_throw",
            fp_cost = 0,
            base_dmg = 1,
            atk_type = "range",
            dmg_type = nil,
            piercing = false,
            on_hitfn = function(inst) end,
            ignore_list = { --might be used to ignore enemy "shields" like spears or spikes.
                "spike_melee",
            },
            icon = {
                
            }
        },
        DESTRUCTION_II = {
            fname = "Destruction II",
            name = "destruction",
            base_dmg = 2,
            atk_type = "melee",
            dmg_type = "spike",
            piercing = false,
            on_hitfn = function(inst) end,
            ignore_list = { --might be used to ignore enemy "shields" like spears or spikes.
                "spike_melee",
            },
        },
    },
    CLASSES = {
        --have either growth values and/or basic stats here.
        BRISTLE = {
            BASE_HP = 2,
            HP_GROWTH = 2,
            BASE_FP = 5,
            FP_GROWTH = 5,
            BASE_BP = 3,
            BP_GROWTH = 3,
            EXP_CAP = 100,

            DEFENSE = 1,
            SKILLS = {
                [0] = {
                    fname = "Destruction",
                    name = "destruction",
                    fp_cost = 0,
                    base_dmg = 1,
                    atk_type = "melee",
                    dmg_type = "spike",
                    piercing = false,
                    on_hitfn = function(inst) end,
                    ignore_list = { --might be used to ignore enemy "shields" like spears or spikes.
                        "spike_melee",
                    },
                },
                [5] = {
                    fname = "Destruction II",
                    name = "destruction",
                    fp_cost = 3,
                    base_dmg = 3,
                    atk_type = "melee",
                    dmg_type = "spike",
                    piercing = false,
                    on_hitfn = function(inst) end,
                    ignore_list = { --might be used to ignore enemy "shields" like spears or spikes.
                        "spike_melee",
                    },
                },
            },
        },
    },
    ENEMIES = {
        BRISTLE = {
            LEVEL = 16, --enemy level is used for Exp calcs.
            MAXHP = 2,
            DEFENSE = 2,

            WALKSPEED = 3,
            RUNSPEED = 6,
            SKILLS = {
                {
                    fname = "Destruction",
                    name = "destruction",
                    base_dmg = 1,
                    atk_type = "melee",
                    piercing = false,
                    on_hitfn = function(inst) end,
                },
            }
        },
    },
}