local SkinsPuppet = require "widgets/skinspuppet" --by the time this is called, it should already be populated with necessary data

local function DoSay(string)
	ChatHistory:SendCommandResponse(string)
end

--These functions exist because the ppskin_manager has no replica component, but the pp_skins table is still on the client
local function GetOwnedSkin(inst, skintable)
	if skintable and not skintable.locked and skintable.owners then
		--print("skintable has owners, iterating...")
		local isowned = false
		for i, v in ipairs(skintable.owners) do
			if inst.userid == v then
				--print("user owns the skin!")
				isowned = true
			end
		end
		return isowned
	else
		--print("skintable does not have owners, returning true")
		return not skintable.locked
	end
end

local function GetSkinsList(inst, mobtable)
	local skintable = {}
	for i, v in pairs(mobtable) do --fun fact, ipairs only works for integer keys aka not named keys, neat
		if GetOwnedSkin(inst, v) then
			table.insert(skintable, v.fname.." skin = /setskin "..v.name)
		end
	end
	if #skintable > 0 then
		DoSay("Here are skins you can use! You can also use '/setskin 0' to return to normal")
		for i, v in ipairs(skintable) do
			DoSay(v)
		end
	else
		DoSay("Sorry, but you don't have any skins for this character...")
	end
end

local UserCommands = require("usercommands")
AddUserCommand("setskin_unlock", {
    prettyname = "Set Skin Unlock", 
    desc = "", 
    permission = COMMAND_PERMISSION.USER,
    slash = false,
    usermenu = false,
    servermenu = false,	
    params = {"skin_id", "targetprefab"},
    vote = false,
    localfn = function(params, caller)
		print("SetSkin Unlock Ran - "..caller.prefab)
		if caller ~= nil and params.skin and params.prefab and caller.components.ppskin_manager then
			--print("Intial check for setskin passed")
			caller:PushEvent("ppunlockskin", {skin_id = params.skin, targetprefab = params.prefab})
		end	
    end,
})

AddUserCommand("setskin_server", {
    prettyname = "Set Skin Server", 
    desc = "", 
    permission = COMMAND_PERMISSION.USER,
    slash = false,
    usermenu = false,
    servermenu = false,	
    params = {"skin", "unlocked"},
    vote = false,
    serverfn = function(params, caller)
		print("SetSkin Server ran")
		if caller ~= nil and not (caller:HasTag("playerghost") or caller:HasTag("INLIMBO") or 
		(caller.sg and caller.sg:HasStateTag("busy"))) and params.skin and caller.components.ppskin_manager and 
		SkinsPuppet.pp_skins[caller.prefab] and params.skin ~= "help" and params.unlocked == "true" then
			--print("Intial check for setskin passed")
			caller.components.ppskin_manager:SetSkin(params.skin, caller.prefab)
		end	
    end,
})

local function IsRevertSkin(skin)
	return skin == "none" or skin == "0" or skin == "default"
end

AddUserCommand("setskin", {
    prettyname = "Set Skin", 
    desc = "Change your skin! Use '/setskin help' for help", 
    permission = COMMAND_PERMISSION.USER,
    slash = true,
    usermenu = false,
    servermenu = false,	
    params = {"skin"},
    vote = false,
	localfn = function(params, caller)
		if caller ~= nil and caller.HUD ~= nil then
			if not caller.replica.ppskin_manager then
				DoSay("Sorry, but your character doesn't support this feature")
				return
			end
			if params.skin and params.skin ~= "help" and not IsRevertSkin(params.skin) and caller.replica.ppskin_manager then
				caller.replica.ppskin_manager:Initialize()
				if not SkinsPuppet.pp_skins[caller.prefab] then
					DoSay("Sorry, but your character doesn't support this feature")
				elseif not SkinsPuppet.pp_skins[caller.prefab][params.skin] then
					DoSay("Sorry, but your character doesn't have this skin! Use '/setskin help' for other skins")
				elseif SkinsPuppet.pp_skins[caller.prefab][params.skin] and (SkinsPuppet.pp_skins[caller.prefab][params.skin].locked and not caller.replica.ppskin_manager:OwnsSkin(params.skin)) then
					DoSay("Sorry, but you don't have access to this skin! Use '/setskin help' for other skins")
				elseif ((SkinsPuppet.pp_skins[caller.prefab] and SkinsPuppet.pp_skins[caller.prefab][params.skin]) or (caller.replica.ppskin_manager and caller.replica.ppskin_manager:IsDefault(params.skin))) then
					local owned = true
					if SkinsPuppet.pp_skins[caller.prefab][params.skin] and SkinsPuppet.pp_skins[caller.prefab][params.skin].locked then
						print("Skin is locked, running own check")
						print(caller.replica.ppskin_manager:OwnsSkin(params.skin))
						owned = caller.replica.ppskin_manager:OwnsSkin(params.skin)
					end
					print("owned is "..tostring(owned))
					UserCommands.RunUserCommand("setskin_server", {skin = params.skin, unlocked = tostring(owned)}, caller)
				end
			end
			if (not params.skin or params.skin == "help") and SkinsPuppet.pp_skins[caller.prefab] then
				GetSkinsList(caller, SkinsPuppet.pp_skins[caller.prefab])
			end
			if caller.replica.ppskin_manager and params.skin and IsRevertSkin(params.skin) then
				UserCommands.RunUserCommand("setskin_server", {skin = params.skin, unlocked = tostring(true)}, caller)
			end
		end	
    end,
})

AddUserCommand("dropoinc", {
    prettyname = "Drop Oinc", 
    desc = "Drop a bag of oincs!", 
    permission = COMMAND_PERMISSION.USER,
    slash = true,
    usermenu = false,
    servermenu = false,	
    params = {"amount"},
    vote = false,
	localfn = function(params, caller)
		if caller and caller.HUD and caller.replica.ppcurrency then
			if params.amount and not tonumber(params.amount) then
				DoSay("DropOinc: You must insert a valid number!")
			elseif tonumber(params.amount) <= 0 then
				DoSay("DropOinc: You must insert a number above 0")
			elseif tonumber(params.amount) > caller.currentcurrency:value() then
				DoSay("DropOinc: You don't have that much to drop! Dropping your current amount.")
			elseif caller.currentcurrency:value() <= 0 then
				DoSay("DropOinc: You have nothing to drop!")
			end
		end
    end,
	serverfn = function(params, caller)
		if caller and caller.components.ppcurrency and tonumber(params.amount) and caller.components.ppcurrency:GetCurrent() > 0 and tonumber(params.amount) > 0 then
			local todrop = 0
			local amount = math.floor(tonumber(params.amount))
			local curamount = caller.components.ppcurrency:GetCurrent()
			if curamount >= amount then
				todrop = amount
			elseif curamount < amount then
				todrop = curamount
			end
			caller.components.ppcurrency:DoDelta(-todrop)
			local bag = SpawnPrefab("wadebag")
			bag:SetValue(todrop)
			caller.components.inventory:GiveItem(bag)
		end
    end,
})

--local UserCommands = require("usercommands") UserCommands.RunUserCommand("setskin_unlock", {skin_id = "hf", targetprefab = "deerplayer"}, ThePlayer)