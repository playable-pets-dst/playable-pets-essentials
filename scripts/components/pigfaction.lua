--[[
local function onunlockskindata(self, unlockskindata)
	if unlockskindata.skin_id and unlockskindata.targetprefab then
		self.inst.replica.ppskin_manager:UnlockSkin(unlockskindata)
	end
end]]

local PigFaction = Class(function(self, inst)
    self.inst = inst
	self.faction = "none"
	self.faction_rank = 1
end, nil,
{
	--unlockskindata = onunlockskindata
})

function PigFaction:GetFaction()
	return self.faction
end

function PigFaction:SetFaction(team)
	if team then
		self.faction = team
	end
end

function PigFaction:GetRank()
	return self.faction_rank
end

function PigFaction:SetRank(rank)
	self.faction_rank = rank or 1
end

function PigFaction:OnSave()
	return {faction = self.faction or "none", faction_rank = self.faction_rank or 1}
end

function PigFaction:OnLoad(data)
	if data then
		self.faction = data.faction
		self.faction_rank = data.faction_rank or 1
	end
end

return PigFaction