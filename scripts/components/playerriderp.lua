--Acts as both a mount and rider component.

local PlayerRider = Class(function(self, inst)
    self.inst = inst
    --for mounts--
	self.riders = {}
    self.max_riders = 1
    self.is_rideable = false
    self.protect_rider = true
    --for riders--
	self.isriding = false
    self.ride_target = nil
    self.locked = false -- If you're "locked" then the mount has to release you. e.g. you're captured.
    self.can_ride = true
end, nil,
{
	--unlockskindata = onunlockskindata
})

function PlayerRider:IsFull()
	return #self.riders >= self.max_riders
end

function PlayerRider:GetRiders()
	return self.riders
end

function PlayerRider:GetRider()
	if #self.riders > 0 then
        return self.riders[1]
    else
        return nil
    end
end

function PlayerRider:RemoveRider(target)
	for i, v in ipairs(self.riders) do
        if v == target then
            table.remove(self.riders, v)
        end
    end
end

function PlayerRider:ReleaseRider(target)
    if #self.riders > 0 then
        if not target then
            self.riders[1].components.playerriderp:Unride(self.inst)
        else
            target.components.playerriderp:Unride(self.inst)
        end
    end
end

function PlayerRider:CanRide(target)
	return target and target:IsValid() and target.components.playerriderp and not
            target.components.playerriderp:IsFull()
end

local function Player_Unride(inst)
    inst.components.playerriderp:Unride()
end

function PlayerRider:Unride()
	if self.ride_target then
        self.ride_target.components.playerriderp:RemoveRider(self.inst)
    end
    self.inst:Show()
    self.inst.components.playercontroller.disable = false
end

function PlayerRider:Ride(target, locked)
	if target and target.components.playerriderp then
        target.components.playerriderp:AddRider(self.inst)
        
        self.inst:Hide()

        if not locked then
            self.inst:ListenForEvent("locomote", Player_Unride)
        else
            self.inst.components.playercontroller.disable = true
        end

        if target.components.playerriderp.protect_rider then
            self.inst.components.health:SetInvincible(true)
        end
    end
end

function PlayerRider:OnSave()
	return {}
end

function PlayerRider:OnLoad(data)
	if data then
		--You cannot save entities, so don't save riders.
	end
end

function PlayerRider:OnUpdate()
	if self.ride_target and self.ride_target:IsValid() and self.isriding then
        local pos = self.ride_target.Transform:GetPosition()
        self.inst.Transform:SetPosition(pos:Get())
    elseif ((self.ride_target and not self.ride_target:IsValid()) or not self.ride_target) and self.isriding then
        
    end
end

return PlayerRider