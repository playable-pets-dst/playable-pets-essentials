local PMUnit = Class(function(self, inst)
    self.inst = inst
    self.current = 0
    if TheWorld.ismastersim then
        self.classified = inst.player_classified
    elseif self.classified == nil and inst.player_classified ~= nil then
        self:AttachClassified(inst.player_classified)
    end
end)

--------------------------------------------------------------------------

function PMUnit:OnRemoveFromEntity()
    if self.classified ~= nil then
        if TheWorld.ismastersim then
            self.classified = nil
        else
            self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
            self:DetachClassified()
        end
    end
end

PMUnit.OnRemoveEntity = PMUnit.OnRemoveFromEntity

function PMUnit:AttachClassified(classified)
    self.classified = classified
    self.ondetachclassified = function() self:DetachClassified() end
    self.inst:ListenForEvent("onremove", self.ondetachclassified, classified)
end

function PMUnit:DetachClassified()
    self.classified = nil
    self.ondetachclassified = nil
end

--------------------------------------------------------------------------

function PMUnit:SetCurrentHP(current)
    if self.classified ~= nil then
        self.inst.currentPMUnit:set(current)
        self.current = current
    end
end

function PMUnit:SetMax(max)
    if self.classified ~= nil then
        --self.classified:SetValue("maxPMUnit", max)
    end
end

function PMUnit:Max()
    if self.inst.components.ppPMUnit ~= nil then
        return self.inst.components.ppPMUnit.max
    elseif self.classified ~= nil then
        return self.classified.maxPMUnit:value()
    else
        return 9999
    end
end

function PMUnit:GetCurrent()
    if self.inst.components.ppPMUnit ~= nil then
        return self.inst.components.ppPMUnit.current
    elseif self.classified ~= nil then
        return self.inst.currentPMUnit:value()
    else
        return self.current
    end
end

return PMUnit