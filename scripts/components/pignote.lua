
local SourceModifierList = require("util/sourcemodifierlist")

local function ontextdata(self, data)
    self.inst.replica.pignote:SetData(data)
end

local PigNote = Class(function(self, inst)
    self.inst = inst
    self.text_data = ""
end,
nil,
{
    text_data = ontextdata,
})

function PigNote:OnSave()
    return { text_data = self.text_data } or nil
end

function PigNote:OnLoad(data)
    if data.text_data ~= nil then
        self.text_data = data.text_data
    end
end

function PigNote:HasData()
    return self.text_data ~= nil
end

function PigNote:SetData(data)
    local old = self.text_data
    self.text_data = data
    self.inst:PushEvent("pignote_update", { olddata = old, newdata = self.text_data})
end

return PigNote
