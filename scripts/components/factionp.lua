--Leo: Mostly for the server and for another mod.

local Faction = Class(function(self, inst)
    self.inst = inst
    self.faction = "none"
    self.title = ""
    self.rank = 0
    self.points = 0
end,
nil,
{
    --faction = onfaction,
    --rank = onrank,
    --points = onpoints,
    --title = ontitle,
})

--------------------------------------------------
-- Getters/Setters
--------------------------------------------------
function Faction:GetFaction()
    return self.faction
end

function Faction:GetRank()
    return self.faction
end

function Faction:SetFaction(new_faction)
    --Leo: We're going to have a manager that will save and load
    --records of the player's faction progression. But for now,
    --just change faction and reset progression.

    self.faction = new_faction
    self.title = ""
    self.rank = 0
    self.points = 0
end
--------------------------------------------------
-- Save/Load
--------------------------------------------------

function Faction:OnSave()
    --reminder, you can't save entities or functions.
	return {
        faction = self.faction or "none",
        title = self.title or "",
        rank = self.rank or 0,
        points = self.points or 0,
    }
end

function Faction:OnLoad(data)
	if data then
		self.faction = data.faction or "none"
        self.title = data.title or ""
        self.rank = data.rank or 0
        self.points = data.points or 0
    end
end

function Faction:OnRemoved()

end

Faction.OnRemoveFromEntity = Faction.OnRemoved

return Faction