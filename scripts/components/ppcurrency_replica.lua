local Currency = Class(function(self, inst)
    self.inst = inst
    self.current = 0
    if TheWorld.ismastersim then
        self.classified = inst.player_classified
    elseif self.classified == nil and inst.player_classified ~= nil then
        self:AttachClassified(inst.player_classified)
    end
end)

--------------------------------------------------------------------------

function Currency:OnRemoveFromEntity()
    if self.classified ~= nil then
        if TheWorld.ismastersim then
            self.classified = nil
        else
            self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
            self:DetachClassified()
        end
    end
end

Currency.OnRemoveEntity = Currency.OnRemoveFromEntity

function Currency:AttachClassified(classified)
    self.classified = classified
    self.ondetachclassified = function() self:DetachClassified() end
    self.inst:ListenForEvent("onremove", self.ondetachclassified, classified)
end

function Currency:DetachClassified()
    self.classified = nil
    self.ondetachclassified = nil
end

--------------------------------------------------------------------------

function Currency:SetCurrent(current)
    if self.classified ~= nil then
        self.inst.currentcurrency:set(current)
        self.current = current
    end
end

function Currency:SetMax(max)
    if self.classified ~= nil then
        --self.classified:SetValue("maxcurrency", max)
    end
end

function Currency:Max()
    if self.inst.components.ppcurrency ~= nil then
        return self.inst.components.ppcurrency.max
    elseif self.classified ~= nil then
        return self.classified.maxcurrency:value()
    else
        return 9999
    end
end

function Currency:GetCurrent()
    if self.inst.components.ppcurrency ~= nil then
        return self.inst.components.ppcurrency.current
    elseif self.classified ~= nil then
        return self.inst.currentcurrency:value()
    else
        return self.current
    end
end

return Currency