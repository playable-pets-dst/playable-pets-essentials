
local SourceModifierList = require("util/sourcemodifierlist")

local BankManager = Class(function(self, inst)
    self.inst = inst
    self.banklist = {}
end,
nil,
{

})

function BankManager:OnSave()
    return { banklist = self.banklist }
end

function BankManager:OnLoad(data)
    if data.banklist then
        self.banklist = data.banklist
    end
end

function BankManager:RegisterPlayer(player)
    self.banklist[player.userid] = 0
end

function BankManager:UnregisterPlayer(player)
    self.banklist[player.userid] = nil
end

function BankManager:Deposit(player)
    if not self.banklist[player.userid] then
        self:RegisterPlayer(player)
    end
    self.banklist[player.userid] = player.components.ppcurrency.current
end

function BankManager:Withdraw(player)
    if self.banklist[player.userid] then
        player.components.ppcurrency:SetAmount(self.banklist[player.userid] or 0)
        self:UnregisterPlayer(player)
    end
end

return BankManager
