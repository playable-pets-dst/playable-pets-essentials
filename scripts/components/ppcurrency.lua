
local SourceModifierList = require("util/sourcemodifierlist")

local function onmax(self, max)
    self.inst.replica.ppcurrency:SetMax(max)
end

local function oncurrent(self, current)
    self.inst.replica.ppcurrency:SetCurrent(current)
end

local Currency = Class(function(self, inst)
    self.inst = inst
    self.max = 99999999
    self.current = 0
end,
nil,
{
    max = onmax,
    current = oncurrent,
})

function Currency:OnSave()
    return self.current ~= 0 and { ppcurrency = self.current } or nil
end

function Currency:OnLoad(data)
    if data.ppcurrency ~= nil and self.current ~= data.ppcurrency then
        self.current = data.ppcurrency
        self:DoDelta(0)
    end
end

function Currency:SetOverrideStarveFn(fn)
    self.overridestarvefn = fn
end

function Currency:SetMax(amount)
    self.max = amount
end

function Currency:SetAmount(amount)
    self.current = amount >= self.max and self.max or amount
    self.inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/coins/3_plus")
end

function Currency:GetCurrent()
    return self.current
end

function Currency:DoDelta(delta, overtime, ignore_invincible)
    if self.redirect ~= nil then
        self.redirect(self.inst, delta, overtime)
        return
    end

    local old = self.current
    self.current = math.clamp(self.current + delta, 0, self.max)
    self.inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/coins/3_plus")

    self.inst:PushEvent("ppcurrencydelta", { oldamount = old, newtotal = self.current, delta = self.current-old })
end

return Currency
