local PlayablePet = Class(function(self, inst)
    self.inst = inst
    
    --Not ready yet.
    --[[
    if TheWorld.ismastersim then
        if inst:HasTag("player") then
            self.classified = SpawnPrefab("playablepet_classified")
            self.classified.entity:SetParent(inst.entity)

            --self.opentask = inst:DoStaticTaskInTime(0, OpenInventory, self)

            --Server intercepts messages and forwards to clients via classified net vars
            inst:ListenForEvent("pp_addability", function(inst, data) self.classified:AddAbility(data.slot, data.ability) end)
        end
    elseif self.classified == nil and inst.playablepet_classified ~= nil then
        self.classified = inst.playablepet_classified
        inst.playablepet_classified.OnRemoveEntity = nil
        inst.playablepet_classified = nil
        self:AttachClassified(self.classified)
    end]]
end,
nil,
{

})

function PlayablePet:UpdateAbilities(data)
    
end

function PlayablePet:OnRemoveEntity()
    if self.classified ~= nil then
        if TheWorld.ismastersim then
            self.classified:Remove()
            self.classified = nil
        else
            self.classified._parent = nil
            self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
            self:DetachClassified()
        end
    end
end

---------------------------------------------
-- Getters/Setters
---------------------------------------------
--Abilities


PlayablePet.OnRemoveFromEntity = PlayablePet.OnRemoveEntity

return PlayablePet