local SkinsPuppet = require "widgets/skinspuppet"

local function DebugPrint(self, str)
	if self.debug then
		print("PPSkin Debug: "..tostring(str))
	end
end

local function onunlockskindata(self, unlockskindata)
	if unlockskindata.skin_id and unlockskindata.targetprefab then
		self.inst.replica.ppskin_manager:UnlockSkin(unlockskindata)
	end
end

local PPSkinManager = Class(function(self, inst)
    self.inst = inst
	self.skin = nil
	self.skinprefab = nil --cannot be used by players, due to local check in the setskin command
	self.debug = false
	self.unlockskindata = {}
end, nil,
{
	unlockskindata = onunlockskindata
})

function PPSkinManager:UnlockSkin(data)
	self.unlockskindata = data
end

function PPSkinManager:GetSkin()
	DebugPrint(self, "GetSkin is returning "..tostring(self.skin))
	return self.skin
end

function PPSkinManager:GrabSkin(skin, prefab)
	DebugPrint(self, "Grabbing Skin "..tostring(skin))
	local skindata = SkinsPuppet.pp_skins
	if skindata then
		if not skindata[prefab or self.inst.prefab] then
			print("ERROR: Prefab '"..tostring(prefab or self.inst.prefab).."' is not registered in the skindata table!")
		end
		return skindata[prefab or (self.skinprefab or self.inst.prefab)][skin]
	else
		print("ERROR: Skindata does not exist!")
		return nil
	end
end

function PPSkinManager:GetSkinList(prefab)
	DebugPrint(self, "Getting Skin List for  "..tostring(prefab or self.inst.prefab))
	return skindata[prefab or self.inst.prefab]
end

local function ResetSymbolData(inst, skindata)
	if skindata then
		inst.AnimState:SetHue(1)
		inst.AnimState:SetSaturation(1)
		if skindata.symbol_data then
			for i, v in ipairs(skindata.symbol_data) do
				if v.symbol then
					if v.hue then
						inst.AnimState:SetSymbolHue(v.symbol, 1)
					end
				end
				if v.sat then
					inst.AnimState:SetSymbolSaturation(v.symbol, 1)
				end
				if v.colour then
					inst.AnimState:SetSymbolMultColour(v.symbol, 1, 1, 1, 1)
				end
				if v.lightoverride then
					inst.AnimState:SetSymbolLightOverride(v.symbol, 1)
				end
			end
		end
	end
end

local function DoSymbolData(inst, skindata)
	inst.AnimState:SetHue(skindata.hue or 1)
	inst.AnimState:SetSaturation(skindata.sat or 1)
	if skindata.symbol_data then
		for i, v in ipairs(skindata.symbol_data) do
			if v.symbol then
				if v.hue then
					inst.AnimState:SetSymbolHue(v.symbol, v.hue)
				end
			end
			if v.sat then
				inst.AnimState:SetSymbolSaturation(v.symbol, v.sat)
			end
			if v.colour then
				inst.AnimState:SetSymbolMultColour(v.symbol, unpack(v.colour))
			end
			if v.lightoverride then
				inst.AnimState:SetSymbolLightOverride(v.symbol, v.lightoverride)
			end
		end
	end
end

local function OnChangeSkin(inst, skindata, newskindata)
	if skindata then
		if skindata and skindata.on_remove then
			skindata.on_remove(inst)
		end
	end
	if newskindata then
		--Note that this gets called when mobs change forms/growup etc from LoadSkin.
		--So write these expecting them to be called more than once
		--while using the same skin.
		if newskindata.on_equip then 
			newskindata.on_equip(inst)
		end
	end
end

function PPSkinManager:LoadSkin(mob, wardrobe)
	DebugPrint(self, "LoadSkin is running")
	if not self.inst:HasTag("playerghost") and (mob or self.inst.mob_table) then
		if self.skin then
			DebugPrint(self, "LoadSkin self.skin passed")
			local skindata = self:GrabSkin(self.skin, (self.skinprefab or self.inst.prefab))
			if skindata then
				if self.setskin_defaultfn then
					self.setskin_defaultfn(self.inst, skindata)
				else
					self.inst.AnimState:SetBuild(skindata.build or self.inst.mob_table.build)
				end
				OnChangeSkin(self.inst, nil, skindata)
				DoSymbolData(self.inst, skindata)
			else
				DebugPrint(self, "Tried to load a skin that no doesn't exists, setting to nil")
				self.skin = nil
			end
		elseif not self.skin and wardrobe then --only run this when using wardrobe
			ResetSymbolData(self.inst)
			if self.setskin_defaultfn then
				self.setskin_defaultfn(self.inst)
			elseif mob or self.inst.mob_table then
				self.inst.AnimState:SetBuild(mob and mob.build or self.inst.mob_table.build)
			end
		end
	end	
end

function PPSkinManager:IsDefault(skin)
	return skin == "0" or skin == "none" or skin == "default"
end

function PPSkinManager:SetSkinDefaultFn(fn)
	DebugPrint(self, tostring(fn))
	self.setskin_defaultfn = fn
end

function PPSkinManager:SetSkin(skin, prefab)
	DebugPrint(self, "Setting skin with these params -  "..tostring(skin).." "..tostring(prefab))
	if not self.inst:HasTag("playerghost") then
		local skin_id = skin
		local skindata = self:GrabSkin(skin_id, prefab)
		if skin_id and skindata and not self:IsDefault(skin_id) then
			DebugPrint(self, "Skin verified, applying...")
			if self.skin ~= skindata.name then
				ResetSymbolData(self.inst, self:GrabSkin(self.skin, prefab))
				local fx = SpawnPrefab("explode_reskin")
				fx.Transform:SetPosition(self.inst:GetPosition():Get())
				if self.inst:HasTag("epic") then
					fx.Transform:SetScale(4, 5, 4)
				else
					fx.Transform:SetScale(1.75, 3, 1.75)
				end
			end
			OnChangeSkin(self.inst, self:GrabSkin(self.skin, prefab), skindata)
			self.skin = skindata.name
			if self.inst.prefab ~= prefab then
				self.skinprefab = prefab
			end
		
			if self.setskin_defaultfn then
				self.setskin_defaultfn(self.inst, skindata)
			else
				self.inst.AnimState:SetBuild(skindata.build)
			end
			DoSymbolData(self.inst, skindata)
		elseif self:IsDefault(skin_id) then
			DebugPrint(self, "Removing skin...")
			if self.skin then
				OnChangeSkin(self.inst, self:GrabSkin(self.skin, prefab))
				ResetSymbolData(self.inst, self:GrabSkin(self.skin, prefab))
				local fx = SpawnPrefab("explode_reskin")
				fx.Transform:SetPosition(self.inst:GetPosition():Get())
				if self.inst:HasTag("epic") then
					fx.Transform:SetScale(3, 5, 3)
				else
					fx.Transform:SetScale(1.75, 3, 1.75)
				end
			end
			self.skin = nil
			if self.setskin_defaultfn then
				self.setskin_defaultfn(self.inst)
			elseif self.inst.mob_table then
				self.inst.AnimState:SetBuild(self.inst.mob_table.build)
			end
		end
	end
end

function PPSkinManager:OnSave()
	return {skin = self.skin, skinprefab = self.skinprefab or nil}
end

function PPSkinManager:OnLoad(data)
	if data then
		self.skin = data.skin or nil
	end
	
	if self.skin then
		self:LoadSkin()
	end
end

return PPSkinManager