--Leo: Mostly for the server and for another mod.

local Poise = Class(function(self, inst)
    self.inst = inst
    self.current = 100
    self.max     = 100
    self.regen   = 0.5
    self.absorb  = 1
    self.isbroken = false

    self.modifiers = {
        poise_reduction = {}
        poise_regen = {}
    }
end,
nil,
{

})

--------------------------------------------------
-- Getters/Setters
--------------------------------------------------
function Poise:AddModifier(mod, source, value, add_type)
    self.modifiers[mod][source][add_type or "mult"] = value
end

function Poise:RemoveModifier(mod, source, add_type)
    self.modifiers[mod][source][add_type or "mult"] = nil
end

function Poise:GetModifier(mod, add_type)
    local add_type = add_type or "mult"
    local modifier = add_type ~= "mult" and 0 or 1
    if not self.modifiers[mod] or not self.modifiers[mod][add_type or "mult"] then
        return modifier
    end
    for k, v in pairs(self.modifiers[mod][add_type]) do
        if add_type ~= "mult" then
            modifier = modifier + v
        else
            modifier = modifier * v
        end
    end
    return modifier
end

function Poise:GetPoise()
    return self.current
end

function Poise:GetMaxPoise()
    return self.max
end

function Poise:GetPercent()
    return self.max/self.current
end

function Poise:SetPoise(value)
    self.current = value
    self.max = value    
end

function Poise:SetMaxPoise(value)
    self.max = value    
end

function Poise:IsBroken()
    return self.poise <= 0
end

function Poise:Reset()
    self.current = self.max
    self.isbroken = false
end

function Poise:PoiseBreak(source)
    self.isbroken = true
    self.inst:PushEvent("poisebreak", {source = source})
end

local function IsPoiseImmune(inst)
    return inst.sg:HasStateTag("poise_immune")
        or inst.sg:HasStateTag("hyperarmor")
        or inst.sg:HasStateTag("nointerrupt") 
end

function Poise:DoDelta(amount, cause, ignore_hyper, afflicter, ignore_absorb)
    local old_percent = self:GetPercent()
    local absorb_mult = ignore_absorb and 1 or self.absorb
    if self.inst.sg and IsPoiseImmune(self.inst) and not ignore_hyper then
        amount = 0
    end
    amount = amount * self.absorb

    self.poise = math.max(0, poise - amount)
    if self.poise <= 0 and not self:IsBroken() then
        self:PoiseBreak(afflicter)
    end

    self.inst:PushEvent("poisedelta", { oldpercent = old_percent, newpercent = self:GetPercent(), cause = cause, afflicter = afflicter, amount = amount })
end
--------------------------------------------------
-- Save/Load
--------------------------------------------------
function Poise:OnUpdate(dt)
    if not self:IsBroken() and self:GetPercent() < 1 then
        self.poise = math.min(self.maxpoise, self.poise + (dt * self.poise_regen))
    end
end

function Poise:OnSave()
    --reminder, you can't save entities or functions.
	return {

    }
end

function Poise:OnLoad(data)
	if data then

    end
end

function Poise:OnRemoved()

end

Poise.OnRemoveFromEntity = Poise.OnRemoved

return Poise