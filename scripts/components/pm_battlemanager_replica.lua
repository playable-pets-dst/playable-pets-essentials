local PMBattlerManager = Class(function(self, inst)
    self.inst = inst

    if TheWorld.ismastersim then
        self.classified = inst.player_classified
    elseif self.classified == nil and inst.player_classified ~= nil then
        self:AttachClassified(inst.player_classified)
    end
end)

--------------------------------------------------------------------------

function PMBattlerManager:OnRemoveFromEntity()
    if self.classified ~= nil then
        if TheWorld.ismastersim then
            self.classified = nil
        else
            self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
            self:DetachClassified()
        end
    end
end

PMBattlerManager.OnRemoveEntity = PMBattlerManager.OnRemoveFromEntity

function PMBattlerManager:AttachClassified(classified)
    self.classified = classified
    self.ondetachclassified = function() self:DetachClassified() end
    self.inst:ListenForEvent("onremove", self.ondetachclassified, classified)
end

function PMBattlerManager:DetachClassified()
    self.classified = nil
    self.ondetachclassified = nil
end

--------------------------------------------------------------------------
function PMBattlerManager:UpdateTeams(teams)
    print("UpdateTeams running")
    for i, v in ipairs(teams.team1) do
        if v and v:IsValid() then
            self.inst["_team1_slot"..i]:set(v)
        end
    end
    for i, v in ipairs(teams.team2) do
        if v and v:IsValid() then
            self.inst["_team2_slot"..i]:set(v)
        end
    end
end

return PMBattlerManager