local function ontype(self, type)
    self.inst.replica.pigshopping:SetType(type)
end

local function onopen(self, open)
    self.inst.replica.pigshopping:SetOpen(open)
end

local function oninventory(self, inventory)
    self.inst.replica.pigshopping:SetInventory(inventory)
end

local PigShopping = Class(function(self, inst)
    self.inst = inst
	self.inventory = {}
	self.type = "GENERAL"
	self.isopen = true
end,
nil,
{
    inventory = oninventory,
	type = ontype,
	isopen = onopen,
})

function PigShopping:GetDebugString()
    return self.isclose and "NEAR" or "FAR"
end

function PigShopping:OnSave()
    return { inventory = self.inventory,
	type = self.type,
	isopen = self.isopen} or nil
end

function PigShopping:OnLoad(data)
    if data ~= nil then
        self.type = data.type or "GENERAL"
		self.inventory = data.inventory or {}
		self.isopen = data.isopen
    end
end

function PigShopping:SetType(type)
	self.type = type
end

function PigShopping:SetOpen(open)
	self.isopen = open
end

function PigShopping:SetInventory(inventory)
	self.inventory = inventory
end

local function WeightedRandomSelect(table)
    local total_weight = 0

    -- Sum the weight values
    for _, v in ipairs(table) do
        total_weight = total_weight + table[v].weight
    end

    -- Generate random number from 1 to the total weight
    local weightSelect = math.random(1, total_weight)
    local thisThing = 0

    -- Check every row of the inner table till we find one smaller than our random number, then return it
    for sumdumvar, v in ipairs(table) do
        thisThing = thisThing+(table[sumdumvar].weight)
        if (weightSelect < thisThing) then
			print(tostring(table[sumdumvar]))
            return table[sumdumvar]
        end
    end
end

local function ShuffleTable(table)
	for i = 1, #table - 1 do
	  local j = math.random(i, #table)
	  table[i], table[j] = table[j], table[i]
	end
end

local function GetItem(table)
	return table[math.random(1, #table)]
end

function PigShopping:GetNewInventory(type)
	local shoptype = type or self.type
	local newinventory = {}
	local temptable = {}
	--fill the temptable with the keys.
	--shuffle the table, then pick the
	--first 6 results.
	for k, v in pairs(PPSHOP[shoptype].items) do
		table.insert(temptable, k)
	end
	ShuffleTable(temptable)
	for i = 1, 6 do
		local item = temptable[i]
		table.insert(newinventory, PPSHOP[shoptype].items[item])
	end
	self.inventory = newinventory
end

function PigShopping:Initialize()
	--print(tostring(self.inventory[1]))
	if not self.inventory[1] then
		self:GetNewInventory(self.type)
	end
end

function PigShopping:SellToCustomer(customer)
	customer:ShowPopUp(POPUPS.pigshopp, true, self.inst)
end

return PigShopping
