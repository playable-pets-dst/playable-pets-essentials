--This is experimental, do not add this.
--The goal of this is to try and tie together special abilities into
--an easy-to-access component that should make mobs more uniform and
--make it more flexible for abilities.
local MAX_SLOTS = 4 --Should not be public.

--Maybe have exclusion tags here as well?
local function onabilitiesupdate()

end

local PlayablePet = Class(function(self, inst)
    self.inst = inst
    --in this order: z, x, c, v / h, j, k, l
    --p will not be tracked in this component.
    --If need be, maybe do shift + ability key to double it?

    --Abilities must be empty first as to not break mobs using legacy code.
	self.abilities = {}
    --This is to keep track and commonize certain passives.
    --i.e. Spiders calling for help if they get attacked.
    --Sort them by "source".
    self.passives = {}

    --Simple variables.
    self.is_human = false
    self.can_despawn = true
    self.ally_tags = {}

    self.foodpoints = 0
    self.kills = 0
    self.pvp_kills = 0
    self.human_kills = 0
    self.deaths = 0
    self.pvp_mult = 1
    --Tags
    self.inst:AddTag("playablepet")

    self.inst:StartUpdatingComponent(self)

    --Events
end,
nil,
{
    --abilities = onabilitiesupdate,
})

---------------------------------------------
-- Abilities
---------------------------------------------
function PlayablePet:AddAbility(slot, data)
	if slot > 0 then
        self.abilities[slot] = {} --empty it first.
        for k, v in pairs(data) do
            self.abilities[slot][k] = v
        end
    else
        PlayablePets.DebugPrint("PlayablePet: Attempted to add an ability to an invalid slot. Please do 1-4.")
    end
end

function PlayablePet:RemoveAbility(slot)
	if self.abilities[slot] then
        self.abilities[slot] = {}
    end
end

function PlayablePet:HasAbility(slot)
    if self.abilities[slot] then
        return self.abilities[slot]
    else
        PlayablePets.DebugPrint("PlayablePet: There is no ability in this slot.")
    end
end

function PlayablePet:ToggleAbility(slot, force)
    local ability = self.abilities[slot]
    if ability and ability.enabled ~= nil then 
        if force ~= nil then
            ability.enabled = force
        else
            if ability.enabled then
                ability.enabled = false
            else
                ability.enabled = true
            end
        end
    else
        PlayablePets.DebugPrint("PlayablePet: There is no ability in this slot.")
    end
end

function PlayablePet:CanUseAbility(slot, cursor_pos)
    --reminder that cursor pos can be lied about so ensure its valid.
    local ability = self.abilities[slot]
    local pos = self.inst:GetPosition()
    if not ability then
        PlayablePets.DebugPrint("PlayablePet: There is no ability in this slot.")
        return false
    end
    if cursor_pos and (ability.min_dist or ability.max_dist) then
        local dist = self.inst:GetDistanceSqToPoint(cursor_pos.x, 0, cursor_pos.z)
        if (ability.min_dist and dist < ability.min_dist * ability.min_dist) or (ability.max_dist and dist > ability.max_dist * ability.max_dist) then
            return false
        end
    end
    return ability.enabled and ability.validfn(self.inst, cursor_pos)
end

function PlayablePet:TryAbility(slot, cursor_pos)
    if self:CanUseAbility(slot, cursor_pos) then
        local ability = self.abilities[slot]
        ability.activatefn(self.inst, cursor_pos)
    end    
end

function PlayablePet:CooldownAbility(slot, time)
    local ability = self.abilities[slot]
    if ability and ability.name then
        ability.enabled = false
        if time > 0 then
            ability.cooldown = time            
        end
    end
end

function PlayablePet:IsAbilityOnCooldown(slot)
    return self.abilities[slot].cooldown
end

function PlayablePet:IsAbilityEnabled(slot)
    local ability = self.abilities[slot]
    return ability.enabled
end

---------------------------------------------
-- Combat
---------------------------------------------

--deprecated, use combat's new DoAdvancedAttack
function PlayablePet:DoAttack(target, data)
    if not (target and target:IsValid() and target.components.combat and target.components.health) then
        return
    end
    self.inst.components.combat:DoAdvancedAttack(target, data)
end
---------------------------------------------
-- Misc
---------------------------------------------
function PlayablePet:OnUpdate(dt)
    for i, v in ipairs(self.abilities) do
        if v.cooldown and v.cooldown > 0 then
            v.cooldown = math.max(v.cooldown - dt, 0)
            if v.cooldown <= 0 and not v.enabled then
                v.enabled = true
            end
        end
    end
end

function PlayablePet:OnSave()
    --reminder, you can't save entities or functions.
	return {
        foodpoints = self.foodpoints or 0,
        kills = self.kills or 0,
        pvp_kills = self.pvp_kills or 0,
        human_kills = self.human_kills or 0,
        deaths = self.deaths or 0,
        resistances = self.resistances or {},
    }
end

function PlayablePet:OnLoad(data)
	if data then
		self.foodpoints = data.foodpoints or 0
        self.kills = data.kills or 0
        self.pvp_kills = data.pvp_kills or 0
        self.human_kills = data.human_kills or 0
        self.deaths = data.deaths or 0
	end
end

function PlayablePet:OnRemoved()
    --might use this for something.
    self.inst:RemoveTag("playablepet")
end

PlayablePet.OnRemoveFromEntity = PlayablePet.OnRemoved
--PlayablePet.OnRemoveEntity = PlayablePet.OnRemoved

return PlayablePet