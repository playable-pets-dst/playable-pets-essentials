
local function onteams(self, teams)
	if teams then
		self.inst.replica.pm_battlemanager:UpdateTeams(teams)
	end
end

local function ForceUpdateTeams(self, teams)
    onteams(self, self.teams)
end

local PMBattleManager = Class(function(self, inst)
    self.inst = inst
    self.currentturn = 0
    self.team_turn = 1
    self.slot_turn = 1
    self.turntimer = 60
    self.active = true
    self.battle_ended = false
    self.teams = {
        team1 = {},
        team2 = {}
    }
    self.exppools = {
        team1 = 0,
        team2 = 0
    }

    self.inst:StartUpdatingComponent(self)
end, nil,
{
    teams = onteams
})

--==================================================
--          Locals
--==================================================
local TEAM_SLOT_OFFSET = 2.5

local function IsActorValid(actor)
    return actor and not actor:HasTag("_defeated") and not actor.cannotmove
end

local function MakeWaiting(actor)
    actor:AddTag("_waiting")
    actor:RemoveTag("_acting")
end

local function MakeActing(actor)
    actor:PushEvent("pm_onact")
    actor:RemoveTag("_waiting")
    actor:AddTag("_acting")
end

local function OnActorRemove(inst)
    inst.components.pm_unit:LeaveBattle()
end

local function SwitchToActorMode(self, actor)
    actor:ListenForEvent("onremove", OnActorRemove)
    if actor.brain then
        actor.brain:Stop()
    end
    if actor.components.playercontroller then
        actor.components.playercontroller:Enable(false)
    end
    if actor.components.health then
        actor.components.health:SetInvincible(true)
    end
    if actor.components.hunger then
        actor.components.hunger:Pause()
    end
    if actor.components.sanity then
        actor.components.sanity.ignore = true
    end
    RemovePhysicsColliders(actor)
    actor:AddTag("_inbattle")
    MakeWaiting(actor)
    actor:AddTag("notarget")
    if actor.sg then
        actor.sg:GoToState("idle")
    end

    actor.components.pm_unit.stage = self.inst
end

local function SwitchToNormalMode(self, actor)
    actor:RemoveEventCallback("onremove", OnActorRemove)
    if actor.brain then
        actor.AnimState:SetScale(2, 2)
        actor:Remove() --removing all NPC actors for now.
    end
    if actor.components.playercontroller then
        actor.components.playercontroller:Enable(true)
    else
        actor.AnimState:SetScale(2, 2)
        actor:Remove() --removing all NPC actors for now.
    end
    if actor.components.health then
        actor.components.health:SetInvincible(false)
    end
    if actor.components.hunger then
        actor.components.hunger:Pause()
    end
    if actor.components.sanity then
        actor.components.sanity.ignore = false
    end
    actor.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    actor:RemoveTag("_inbattle")
    actor:RemoveTag("_acting")
    actor:RemoveTag("_defeated")
    actor:RemoveTag("_waiting")
    actor:RemoveTag("notarget")
    if actor.sg then
        actor.sg:GoToState("idle")
    end

    actor.components.pm_unit.stage = nil
end

local function PushEventsOnActors(self, event)
    for i, v in ipairs(self.teams) do
        for i2, unit in ipairs(v) do
            unit:PushEvent(event)
        end
    end
end

--==================================================
--          Getter/Setters
--==================================================
--Note: All "team" parameters should be strings!
function PMBattleManager:GetAllActiveUnits(inc_def)
    local actors = {}
    for i, v in ipairs(self.teams) do
        for i2, unit in ipairs(v) do
            if not unit:HasTag("_defeated") or inc_def then
                table.insert(actors, unit)
            end
        end
    end
    return actors
end

function PMBattleManager:GetAllTeams(inc_def)
    return self.teams
end

function PMBattleManager:GetAllUnitsOnTeam(team, inc_def)
    local actors = {}
    for i, v in ipairs(self.teams[team]) do
        if not v:HasTag("_defeated") or inc_def then
            table.insert(actors, v)
        end
    end
    return actors
end

function PMBattleManager:GetTeamOfUnit(target)
    for i, v in ipairs(self.teams.team1) do
        if v == target then
            return "team1"
        end
    end
    for i, v in ipairs(self.teams.team2) do
        if v == target then
            return "team2"
        end
    end
end

function PMBattleManager:GetOpposingTeamOfUnit(target)
    local myteam = self:GetTeamOfUnit(target)
    return myteam == "team1" and "team2" or "team1"
end

function PMBattleManager:GetTeamLevel(team)
    local leveltotal = 0
    for i, v in ipairs(self.teams[team]) do
        leveltotal = leveltotal + v.components.pm_unit.level
    end
    return leveltotal
end

function PMBattleManager:GetHighestLevelOnTeam(team)
	local teams = self.teams[team]
	local levelscore = 0
	for i, v in ipairs(teams) do
		if v.components.pm_unit:GetLevel() > levelscore then
			levelscore = v.components.pm_unit:GetLevel()
		end
	end
	return levelscore
end
--==================================================
--          BattleManagement
--==================================================
function PMBattleManager:RepositionActors(team)
    local slotcount = 0
    for i, v in ipairs(self.teams[team]) do
        if v and v:IsValid() then
            slotcount = slotcount + 1
        end
    end
end

function PMBattleManager:EjectActor(actor, team, slot)
    if actor then
        local pm_unit = actor.components.pm_unit
        SwitchToNormalMode(self, actor)
        self.teams[pm_unit.team][pm_unit.slot] = nil
    elseif team and slot then --if theres no actor here, then empty it out. Can be caused by disconnect.
        self.teams[team][slot] = nil
    end
    actor.components.pm_unit.team = nil
    actor.components.pm_unit.slot = nil
    ForceUpdateTeams(self)
end

function PMBattleManager:InjectActor(actor, team, slot)
    if actor and team and slot then
        if self.teams[team][slot] ~= nil then
            EjectActor(nil, team, slot) --Slot taken, so kick the original slot owner out!
        end
        self.teams[team][slot] = actor
        SwitchToActorMode(self, actor)
        if team == "team2" then 
            actor.Transform:SetPosition(newpos.x - (0.1*slot), 0, newpos.z - (team_slot_offset*-slot))
        else
            actor.Transform:SetPosition(newpos.x + (0.1*slot), 0, newpos.z - (team_slot_offset*slot))
        end
        actor.Transform:SetRotation(team == "team2" and 180 or 0)
        actor.components.pm_unit.team = team
        actor.components.pm_unit.slot = slot
    else
        print("PMBM:Could not inject Actor due to missing/invalid parameters")
    end
    ForceUpdateTeams(self)
end

local function ActorReadyToLeave(actor)
    return not (actor:HasTag("_islevelingup"))
end

function PMBattleManager:PoolExp(amount, team)
    self.exppools[team] = self.exppools[team] + amount
end

function PMBattleManager:EndBattle(victory)
    self.inst:AddTag("_battle_ended")

    for i, v in ipairs(self.teams[victory]) do
        v.components.pm_unit:ObtainExp(self.exppools[victory])
    end
    self.battle_ended = true --have OnUpdate clear out the teams.
end

function PMBattleManager:IsVictory()
    local team1_count = self:GetAllUnitsOnTeam("team1") or 0
    local team2_count = self:GetAllUnitsOnTeam("team2") or 0
    local victory = "none"
    if #team1_count > 0  and #team2_count <= 0 then
        victory = "team1"
    elseif #team2_count > 0  and #team1_count <= 0 then 
        victory = "team2"
    elseif #team2_count <= 0  and #team1_count <= 0 then
        victory = "draw" --shouldn't be possible atm
    end
    if victory == "none" then
        return false
    else
        self:EndBattle(victory)
        return true
    end
end

function PMBattleManager:StartBattle(team1, team2, type)
    --Any rpg component checks should be already checked... right?
    self.inst:EnableCameraFocus(true)
    self.inst:AddTag("active")
    local pos = self.inst:GetPosition()
    local team1_pos = {x = pos.x, y = 0, z = pos.z - 2.5}
    local team2_pos = {x = pos.x, y = 0, z = pos.z + 2.5}
    local team_positions = {team1_pos, team2_pos}
    local team_slot_offset = TEAM_SLOT_OFFSET
	self.teams.team1 = team1
    self.teams.team2 = team2
    for i, v in pairs(self.teams.team1) do
        --print(v.prefab)
        local newpos = team_positions[1]
        SwitchToActorMode(self, v)
        v.Transform:SetPosition(newpos.x - (0.1*i), 0, newpos.z + (-team_slot_offset*i))
        v.Transform:SetRotation(0)
        v.components.pm_unit.team = "team1"
        v.components.pm_unit.slot = i
    end
    for i, v in pairs(self.teams.team2) do
        --print(v.prefab)
        local newpos = team_positions[2]
        SwitchToActorMode(self, v)
        v.Transform:SetPosition(newpos.x + (0.1*i), 0, newpos.z + (team_slot_offset*i))
        v.Transform:SetRotation(180)
        v.components.pm_unit.team = "team2"
        v.components.pm_unit.slot = i
    end
    self.inst:DoUpdateTeams()
    self:StartTurn()
end

function PMBattleManager:PlayAction()
    self.inst:AddTag("acting")
end

function PMBattleManager:EndAction()
    self.inst:RemoveTag("acting")
end


function PMBattleManager:StartTurn()
    if not self:IsVictory() then
        self.currentturn = self.currentturn + 1
        --Do turn based statuses or abilities here.
        PushEventsOnActors(self, "pm_turnstart")
        self.team_turn = 1
        self.slot_turn = 1
        self:StartUnitTurn(self.team_turn, self.slot_turn) --always start with team 1 and first slot in team 1, let the function handle the rest.
        --check if there are any units remaining on the teams, and declare victory if necessary
    end

end

function PMBattleManager:EndTurn() --this gets run after all actors have completed their turns
    --Do turn based statuses or abilities here.
    if not self:IsVictory() then
        PushEventsOnActors(self, "pm_turnend")
        self:StartTurn()
    end
end

function PMBattleManager:StartNextUnitTurn(prevactor)
    if not self:IsVictory() then
        if prevactor then
            MakeWaiting(prevactor)
        end
        self.slot_turn = self.slot_turn + 1
        if self.team_turn == 1 and self.slot_turn > TUNING.PM.MAX_PARTY_SLOTS then
            self.team_turn = 2
            self.slot_turn = 1
        elseif self.team_turn == 2 and self.slot_turn > TUNING.PM.MAX_PARTY_SLOTS then
            self:EndTurn()
        end
        self:StartUnitTurn(self.team_turn, self.slot_turn)
    end
end

function PMBattleManager:StartUnitTurn(team_id, slot_id)
    local actor = (self.teams["team"..team_id][slot_id] and IsActorValid(self.teams["team"..team_id][slot_id])) and self.teams["team"..team_id][slot_id] or nil
    if not actor then
        self:StartNextUnitTurn()
    else
        MakeActing(actor)
        self.currentactor = actor
        self.currentactor:PushEvent("pm_unit_turn_start", {})
    end
end

function PMBattleManager:OnUpdate(dt)
    if self.battle_ended then
        print("Battle is over, removing actors")
        --Battle is over, eject actors when they're ready to go.
        for i, v in ipairs(self.teams.team1) do
            if v and v:IsValid() and ActorReadyToLeave(v) then
                self:EjectActor(v)
            end
        end
        for i, v in ipairs(self.teams.team2) do
            if v and v:IsValid() and ActorReadyToLeave(v) then
                self:EjectActor(v)
            end
        end
        if #self.teams.team1 == 0 and #self.teams.team2 == 0 then
            self.inst:Remove()
        end
    end
end

function PMBattleManager:OnSave()
	
end

function PMBattleManager:OnLoad(data)

end

return PMBattleManager