--Note, this is not meant for human players.

local function onhealthsleep(self, healthsleep)
    if self.sleeper ~= nil and self.sleeper.player_classified ~= nil then
        self.sleeper.player_classified.issleephealing:set(healthsleep)
    end
end

local function onsleeper(self, sleeper, old_sleeper)
    if old_sleeper ~= nil and old_sleeper.player_classified ~= nil then
        old_sleeper.player_classified.issleephealing:set(false)
    end
    if sleeper == nil then
        self.inst:RemoveTag("hassleeper")
    else
        self.inst:AddTag("hassleeper")
        if sleeper.player_classified ~= nil then
            sleeper.player_classified.issleephealing:set(self.healthsleep)
        end
    end
end

local MultiSleepingBag = Class(function(self, inst)
    self.inst = inst
	self.max_slots = 10
    self.healthsleep = true
    self.dryingrate = nil
    self.sleepers = {}
    self.onsleep = nil
    self.onwake = nil
	self.musthavetag = "spider"
end,
nil,
{
    healthsleep = onhealthsleep,
    sleeper = onsleeper,
})

function MultiSleepingBag:SetMaxSlots(num)
	if num and num > 0 then
		self.max_slots = num
	else
		print("MultiSleepingBag Error: You must input a value greater than 0")
	end
end

function MultiSleepingBag:GetSleepers()
    return self.sleepers
end

function MultiSleepingBag:MustHaveTag(tag)
	self.musthavetag = tag
end

function MultiSleepingBag:DoSleep(doer)
    if self.sleepers and #self.sleepers < self.max_slots and doer.sleepingbag == nil and (doer:HasTag(self.musthavetag)) then
		--print("DEBUG: Adding "..doer:GetDisplayName().."to Sleepers")
        table.insert(self.sleepers, doer)
        doer.sleepingbag = self.inst
        if self.onsleep ~= nil then
            self.onsleep(self.inst, doer)
        end
	else
		doer:Show()
		doer.sg:GoToState("idle")
    end
end

function MultiSleepingBag:DoWakeUpAll(nostatechange)
    local sleepers = self.sleepers
    if #sleepers > 0  then
		for i, v in ipairs(sleepers) do
			if v.sleepingbag == self.inst then
				v.sleepingbag = nil
				table.remove(self.sleepers, self.sleepers[v])
				if self.onwake ~= nil then
					self.onwake(self.inst, v, nostatechange)
				end
			end	
		end
    end
end

function MultiSleepingBag:DoWakeUp(sleeper, nostatechange)
	if sleeper and sleeper.sleepingbag == self.inst then
		--print("DEBUG: Removing "..sleeper:GetDisplayName().." to Sleepers")
		sleeper.sleepingbag = nil
		table.remove(self.sleepers, self.sleepers[sleeper])
		if self.onwake ~= nil then
			print("DEBUG: onwake ran")
			self.onwake(self.inst, sleeper, nostatechange)
		end
    end
end

MultiSleepingBag.OnRemoveFromEntity = MultiSleepingBag.DoWakeUpAll
MultiSleepingBag.OnRemoveEntity = MultiSleepingBag.OnRemoveFromEntity

return MultiSleepingBag