

local PigNote = Class(function(self, inst)
    self.inst = inst
    self.text_data = net_string(inst.GUID, "pignote.text_data")

    --[[
    if TheWorld.ismastersim then
        self.classified = SpawnPrefab("disgaeacard_classified")
        self.classified.entity:SetParent(inst.entity)
    elseif self.classified == nil and inst.disgaeacard_classified ~= nil then
        self:AttachClassified(inst.disgaeacard_classified)
        inst.disgaeacard_classified.OnRemoveEntity = nil
        inst.disgaeacard_classified = nil
    end]]
end)

--------------------------------------------------------------------------
function PigNote:SetData(data)
    if data ~= nil then
        self.text_data:set(tostring(data))
    end
end

return PigNote