

local PigShopping = Class(function(self, inst)
    self.inst = inst
    self.inventory = {
        slot1 = net_string(inst.GUID, "pigshopping.inventory.slot1"),
        cost1 = net_int(inst.GUID, "pigshopping.inventory.cost1"),
        slot2 = net_string(inst.GUID, "pigshopping.inventory.slot2"),
        cost2 = net_int(inst.GUID, "pigshopping.inventory.cost2"),
        slot3 = net_string(inst.GUID, "pigshopping.inventory.slot3"),
        cost3 = net_int(inst.GUID, "pigshopping.inventory.cost3"),
        slot4 = net_string(inst.GUID, "pigshopping.inventory.slot4"),
        cost4 = net_int(inst.GUID, "pigshopping.inventory.cost4"),
        slot5 = net_string(inst.GUID, "pigshopping.inventory.slot5"),
        cost5 = net_int(inst.GUID, "pigshopping.inventory.cost5"),
        slot6 = net_string(inst.GUID, "pigshopping.inventory.slot6"),  
        cost6 = net_int(inst.GUID, "pigshopping.inventory.cost6"),          
    }
    self.type = net_string(inst.GUID, "pigshopping.type")
    self.isopen = net_bool(inst.GUID, "pigshopping.isopen")

    --[[
    if TheWorld.ismastersim then
        self.classified = SpawnPrefab("PigShopping_classified")
        self.classified.entity:SetParent(inst.entity)
    elseif self.classified == nil and inst.PigShopping_classified ~= nil then
        self:AttachClassified(inst.PigShopping_classified)
        inst.PigShopping_classified.OnRemoveEntity = nil
        inst.PigShopping_classified = nil
    end]]
end)
--------------------------------------------------------------------------
function PigShopping:SetInventory(data)
    for i, v in ipairs(data) do
        print("Adding "..v.prefab)
        self.inventory["slot"..i]:set(v.prefab)
        self.inventory["cost"..i]:set(v.cost)
    end
end

function PigShopping:SetOpen(open)
    self.isopen:set(open)
end

function PigShopping:SetType(type)
    self.type:set(type)
end

return PigShopping