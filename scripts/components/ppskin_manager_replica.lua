local function OnUnlockdata(inst)
    --print("REPLICA OnUnlockdata")
    local id = inst.replica.ppskin_manager._u_skin_id:value()
    local prefabname = inst.replica.ppskin_manager._u_prefab:value()
    inst:PushEvent("ppunlockskin", {skin_id = id, targetprefab = prefabname})
end

local PPSkin_Manager = Class(function(self, inst)
    self.inst = inst
    self._u_skin_id = net_string(inst.GUID, "ppskin_manager._u_skin_id")
    self._u_prefab = net_string(inst.GUID, "ppskin_manager._u_prefab", "unlockskindata")
    if not TheNet:IsDedicated() then
        self.inst:ListenForEvent("unlockskindata", OnUnlockdata)
    end
    if TheWorld.ismastersim then
        self.classified = inst.player_classified
    elseif self.classified == nil and inst.player_classified ~= nil then
        self:AttachClassified(inst.player_classified)
    end
end)

--------------------------------------------------------------------------
function PPSkin_Manager:OnRemoveFromEntity()
    if self.classified ~= nil then
        if TheWorld.ismastersim then
            self.classified = nil
        else
            self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
            self:DetachClassified()
        end
    end
end

PPSkin_Manager.OnRemoveEntity = PPSkin_Manager.OnRemoveFromEntity

function PPSkin_Manager:AttachClassified(classified)
    self.classified = classified
    self.ondetachclassified = function() self:DetachClassified() end
    self.inst:ListenForEvent("onremove", self.ondetachclassified, classified)
end

function PPSkin_Manager:DetachClassified()
    self.classified = nil
    self.ondetachclassified = nil
end

--------------------------------------------------------------------------

function PPSkin_Manager:Initialize()
    TheSim:GetPersistentString("ppskins", function(load_success, data)
        if load_success then
            PlayablePets.DebugPrint("ppskins data loaded successfully!")
        else
            PlayablePets.DebugPrint("ppskins data not found, creating new data!")
            TheSim:SetPersistentString("ppskins", json.encode({}), false)
        end
    end)
end

function PPSkin_Manager:OwnsSkin(skin)
    local prefab = self.inst.prefab
    local success = false
    TheSim:GetPersistentString("ppskins", function(load_success, data)
        if load_success and data then
            local status, skindata = pcall( function() return json.decode(data) end)
            if type(skindata) ~= "table" then
                print("PP ERROR - Your skin data is invalid.")
            end
            success = (skindata and skindata[prefab]) and skindata[prefab][skin] or false
        end
    end)
    return success
end

function PPSkin_Manager:UnlockSkin(data)
    --print("Replica unlockskin running")
    self._u_skin_id:set(data.skin_id)
    self._u_prefab:set(data.targetprefab)
end

function PPSkin_Manager:IsDefault(skin)
	return skin == "0" or skin == "none" or skin == "default"
end

return PPSkin_Manager