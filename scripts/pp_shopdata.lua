local GENERIC_PRODUCE_COST = 2
return {
    GENERAL = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_banker",
        },
        items = {
            pickaxe = {
                prefab = "pickaxe",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            shovel = {
                prefab = "shovel",
                cost = 5,
                weight = 80,
            },
            axe = {
                prefab = "axe",
                cost = 5,
                weight = 80,
            },
            farm_hoe = {
                prefab = "farm_hoe",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            reskin_tool = {
                prefab = "reskin_tool",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            cannonball_rock_item = {
                prefab = "cannonball_rock_item",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            sewing_tape = {
                prefab = "sewing_tape",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            hammer = {
                prefab = "hammer",
                cost = 5,
                weight = 80,
            },
            umbrella = {
                prefab = "umbrella",
                cost = 10,
                weight = 80,
            },
            boat_item = {
                prefab = "boat_item",
                cost = 8,
                weight = 20,
                atlas = "images/inventoryimages1.xml"
            },
            anchor_item = {
                prefab = "anchor_item",
                cost = 10,
                weight = 20,
                atlas = "images/inventoryimages1.xml"
            },
            steeringwheel_item = {
                prefab = "anchor_item",
                cost = 10,
                weight = 20,
                atlas = "images/inventoryimages1.xml"
            },
            boatpatch = {
                prefab = "boatpatch",
                cost = 3,
                weight = 20,
                atlas = "images/inventoryimages1.xml"
            },
        },
    },
    DELI = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_storeowner",
        },
        items = {
            meat = {
                prefab = "meat",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            fish = {
                prefab = "fish",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            meatballs = {
                prefab = "meatballs",
                cost = 7,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            waffles = {
                prefab = "waffles",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            leafymeatburger = {
                prefab = "leafymeatburger",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            turkeydinner = {
                prefab = "turkeydinner",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            honeyham = {
                prefab = "honeyham",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            dragonpie = {
                prefab = "dragonpie",
                cost = 20,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            monsterlasagna = {
                prefab = "monsterlasagna",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            pumpkincookie = {
                prefab = "pumpkincookie",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            honeynuggets = {
                prefab = "honeynuggets",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            frogglebunwich = {
                prefab = "frogglebunwich",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            perogies = {
                prefab = "perogies",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            bird_egg = {
                prefab = "bird_egg",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            baconeggs = {
                prefab = "baconeggs",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            ratatouille = {
                prefab = "ratatouille",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
        },
    },
    FLORIST = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_florist",
        },
        items = {
            seeds = {
                prefab = "seeds",
                cost = 1,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            asparagus_seeds = {
                prefab = "asparagus_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            carrot_seeds = {
                prefab = "carrot_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            corn_seeds = {
                prefab = "corn_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            dragonfruit_seeds = {
                prefab = "dragonfruit_seeds",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            eggplant_seeds = {
                prefab = "eggplant_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            garlic_seeds = {
                prefab = "garlic_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            onion_seeds = {
                prefab = "onion_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            pepper_seeds = {
                prefab = "pepper_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            pomegranate_seeds = {
                prefab = "pomegranate_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            potato_seeds = {
                prefab = "potato_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            pumpkin_seeds = {
                prefab = "pumpkin_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            durian_seeds = {
                prefab = "durian_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            tomato_seeds = {
                prefab = "tomato_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            watermelon_seeds = {
                prefab = "watermelon_seeds",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
        },
    },
    SPA = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_florist",
        },
        items = {
            antidotep = {
                prefab = "antidotep",
                cost = 5,
                weight = 80,
                atlas = "images/pp_essentials_icons.xml"
            },
            blue_cap = {
                prefab = "blue_cap",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            green_cap = {
                prefab = "green_cap",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            bandage = {
                prefab = "bandage",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            bathbomb = {
                prefab = "bathbomb",
                cost = 5,
                weight = 40,
                atlas = "images/inventoryimages1.xml"
            },
            flowerhat = {
                prefab = "flowerhat",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            lifeinjector = {
                prefab = "lifeinjector",
                cost = 10,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
            compostwrap = {
                prefab = "compostwrap",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            tillweedsalve = {
                prefab = "tillweedsalve",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
        },
    },
    PRODUCE = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_storeowner",
        },
        items = {
            onion = {
                prefab = "onion",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml",
                tex = "quagmire_onion.tex"
            },
            plantmeat = {
                prefab = "plantmeat",
                cost = 4,
                weight = 40,
                atlas = "images/inventoryimages.xml",
            },
            dragonfruit = {
                prefab = "dragonfruit",
                cost = 6,
                weight = 40,
                atlas = "images/inventoryimages.xml",
            },
            durian = {
                prefab = "durian",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml",
            },
            garlic = {
                prefab = "garlic",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml",
                tex = "quagmire_garlic.tex"
            },
            potato = {
                prefab = "potato",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            asparagus = {
                prefab = "asparagus",
                cost = GENERIC_PRODUCE_COST,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            tomato = {
                prefab = "tomato",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages2.xml",
                tex = "quagmire_tomato.tex"
            },
            berries = {
                prefab = "berries",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            ice = {
                prefab = "ice",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            carrot = {
                prefab = "carrot",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            watermelon = {
                prefab = "watermelon",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            drumstick = {
                prefab = "drumstick",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            eggplant = {
                prefab = "eggplant",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            corn = {
                prefab = "corn",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            pumpkin = {
                prefab = "pumpkin",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            meat = {
                prefab = "meat",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            pomegranate = {
                prefab = "pomegranate",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            cave_banana = {
                prefab = "cave_banana",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            froglegs = {
                prefab = "froglegs",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },

        },
    },
    ARCANE = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_erudite",
        },
        items = {
            icestaff = {
                prefab = "icestaff",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            firestaff = {
                prefab = "firestaff",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            amulet = {
                prefab = "amulet",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            blueamulet = {
                prefab = "blueamulet",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            purpleamulet = {
                prefab = "purpleamulet",
                cost = 12,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            livinglog = {
                prefab = "livinglog",
                cost = 6,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            armorslurper = {
                prefab = "armorslurper",
                cost = 12,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            nightsword = {
                prefab = "nightsword",
                cost = 15,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
            armor_sanity = {
                prefab = "armor_sanity",
                cost = 15,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
            onemanband = {
                prefab = "onemanband",
                cost = 13,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
        },
    },
    ODDITIES = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_collector",
        },
        items = {
            silk = {
                prefab = "silk",
                cost = 2,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            houndstooth = {
                prefab = "houndstooth",
                cost = 3,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            gears = {
                prefab = "gears",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            coontail = {
                prefab = "coontail",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            mandrake = {
                prefab = "mandrake",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            wormlight = {
                prefab = "wormlight",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            deerclops_eyeball = {
                prefab = "deerclops_eyeball",
                cost = 15,
                weight = 30,
                atlas = "images/inventoryimages.xml"
            },
            bearger_fur = {
                prefab = "bearger_fur",
                cost = 15,
                weight = 30,
                atlas = "images/inventoryimages.xml"
            },
            goose_feather = {
                prefab = "goose_feather",
                cost = 7,
                weight = 30,
                atlas = "images/inventoryimages.xml"
            },
            dragon_scales = {
                prefab = "dragon_scales",
                cost = 30,
                weight = 10,
                atlas = "images/inventoryimages.xml"
            },
            walrus_tusk = {
                prefab = "walrus_tusk",
                cost = 7,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            steelwool = {
                prefab = "steelwool",
                cost = 10,
                weight = 50,
                atlas = "images/inventoryimages.xml"
            },
        },
    },
    WEAPONS = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_hunter",
        },
        items = {
            cutless = {
                prefab = "cutless",
                cost = 7,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            trap_teeth = {
                prefab = "trap_teeth",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            birdtrap = {
                prefab = "birdtrap",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            halberdp = {
                prefab = "halberdp",
                cost = 7,
                weight = 80,
                atlas = "images/inventoryimages/halberdp.xml"
            },
            blowdart_pipe = {
                prefab = "blowdart_pipe",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            blowdart_sleep = {
                prefab = "blowdart_sleep",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            boomerang = {
                prefab = "boomerang",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            spear = {
                prefab = "spear",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
        },
    },
    HAT = {
        shopkeeper = {
            bank = "townspig",
            build = "pig_hatmaker",
        },
        items = {
            winterhat = {
                prefab = "winterhat",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            catcoonhat = {
                prefab = "catcoonhat",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            walrushat = {
                prefab = "walrushat",
                cost = 20,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            molehat = {
                prefab = "molehat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            tophat = {
                prefab = "tophat",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            featherhat = {
                prefab = "featherhat",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            beefalohat = {
                prefab = "beefalohat",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            rainhat = {
                prefab = "rainhat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            umbrella = {
                prefab = "umbrella",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            mermhat = {
                prefab = "mermhat",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            walterhat = {
                prefab = "walterhat",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            monkey_smallhat = {
                prefab = "monkey_smallhat",
                cost = 5,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            monkey_mediumhat = {
                prefab = "monkey_mediumhat",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            deserthat = {
                prefab = "deserthat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            goggleshat = {
                prefab = "goggleshat",
                cost = 8,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            cookiecutterhat = {
                prefab = "cookiecutterhat",
                cost = 12,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            red_mushroomhat = {
                prefab = "red_mushroomhat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            blue_mushroomhat = {
                prefab = "blue_mushroomhat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            green_mushroomhat = {
                prefab = "green_mushroomhat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            beehat = {
                prefab = "beehat",
                cost = 10,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            batnosehat = {
                prefab = "batnosehat",
                cost = 15,
                weight = 80,
                atlas = "images/inventoryimages1.xml"
            },
            plantregistryhat = {
                prefab = "plantregistryhat",
                cost = 6,
                weight = 80,
                atlas = "images/inventoryimages2.xml"
            },
            strawhat = {
                prefab = "strawhat",
                cost = 4,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            earmuffshat = {
                prefab = "earmuffshat",
                cost = 6,
                weight = 80,
                atlas = "images/inventoryimages.xml"
            },
            dragonheadhat = {
                prefab = "dragonheadhat",
                cost = 4,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
            dragonbodyhat = {
                prefab = "dragonbodyhat",
                cost = 4,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
            dragontailhat = {
                prefab = "dragonbodyhat",
                cost = 4,
                weight = 40,
                atlas = "images/inventoryimages.xml"
            },
        },
    },

}