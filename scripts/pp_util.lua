PlayablePets = {}
PlayablePets.ConfigData = PlayablePets.ConfigData or {}

local PP_SETTING = require("pp_settings")
local MOBTYPE = require("pp_mobtype")

-- Used to call the function listed as a key in this table when the corresponding keyboard code is detected (key pressed)
local RPCKEYS =
{
	SetSleep = MOBZKEY, -- Z
	SpecialAbility1 = MOBXKEY, -- X
	SpecialAbility2 = MOBCKEY, -- C
	SpecialAbility3 = MOBVKEY, -- V
	SpecialAbility4 = MOBBKEY, -- B
	
	ChangeCharacter = MOBPKEY, -- P
}


----------------------------------------
-- Outline
----------------------------------------
-- Table Ultility
-- Debug Functions
-- Config/Settings
-- Modmain Functions

------------------------------------------------------------------
-- Table Ultility
------------------------------------------------------------------

--Table concat
function table.contains(t, element)
  if t ~= nil then	
  for _, value in pairs(t) do
		if value == element then
			return true
		end
  end
  else
	
  return false
  end
end

-- Append the contents of t2 to t1
--function table.concat(t1, t2)
function TableConcat(t1, t2)
	for i=1,#t2 do
		t1[#t1+1] = t2[i]
	end
	
	return t1
end

------------------------------------------------------------------
-- Debug Functions
------------------------------------------------------------------
PlayablePets.Debug = function(val)
	if val then
		PP_DEBUG = val
	end
end

PlayablePets.DebugPrint = function(string)
	if PP_DEBUG then
		print(string)
	end
end

local d_print = PlayablePets.DebugPrint

PlayablePets.DebugPrintData = function(title, data)
	local doprint = d_print
	doprint("/////////// "..title.." //////////")
	for k, v in pairs(data) do
		d_print(tostring(k)..": "..tostring(v))
	end
	doprint("///////////"..string.rep("/", string.len(title)).."//////////")
end

------------------------------------------------------------------
-- Config/Settings
------------------------------------------------------------------

local configuratorModName = "Pet Configuration Manager"
local configuratorModEnabled = KnownModIndex:IsModEnabled(configuratorModName)

for _, mod in ipairs(KnownModIndex:GetModsToLoad()) do
	if mod == configuratorModName then
		configuratorModEnabled = true
	end
end

-- Retrieve the global Playable Pets configuration settings, if applicable
if configuratorModEnabled then
	local modConfig = KnownModIndex:LoadModConfigurationOptions(configuratorModName, false)
	
	-- Build a more convenient global configuration table to access, indexed by option name
	if modConfig and type(modConfig) == "table" then
		for k, v in ipairs(modConfig) do
			PlayablePets.ConfigData[v.name] = {}
			PlayablePets.ConfigData[v.name].default = v.default
			PlayablePets.ConfigData[v.name].saved = v.saved
		end
	end
end

-- The configurator mod is intended to override all individual Playable Pets mods, if an option is set to
-- This function will first consult the configurator settings for an option, only using the local
--  setting if both the saved and default options are set to not override.
PlayablePets.GetModConfigData = function(optionName)
	local configData = PlayablePets.ConfigData[optionName]
	local optionValue = nil
	
	--local dataDump = DataDumper(PlayablePets.ConfigData, nil, false)
	
	-- If the configurator option isn't set to off, use it
	-- Otherwise, use the default if it isn't off
	if configData ~= nil then
		if configData.saved ~= nil and configData.saved ~= PP_SETTING.OFF then
			optionValue = configData.saved
		elseif configData.default  ~= PP_SETTING.OFF then
			optionValue = configData.default
		end
	end
	
	local name = ""
	for i, v in ipairs(PP_SETTING.MODNAMES) do
		if GetModConfigData(optionName, v, false) then
			name = v
		end
	end
	
	-- If the configurator option was set to override the local mod, use it
	-- Otherwise, get the local mod's option for the setting
	-- Forced to use function GetModConfigData(optionname, PP_SETTING.MODNAME, get_local_config) due to this not being in modmain.lua
	--return optionValue ~= nil and optionValue or GetModConfigData(optionName)
	return optionValue ~= nil and optionValue or GetModConfigData(optionName, name, false)
end


function PlayablePets.IsModEnabled(id)
	local isenabled = false
	for k,v in ipairs(KnownModIndex:GetModsToLoad()) do
		if KnownModIndex:GetModInfo(v).folder_name == "workshop-"..id then
			--print(mob.fancyname.."'s mod is enabled!")
			isenabled = true
		end
	end
	return isenabled
end
-- Consults various Configurator preset settings in addition to the default mod's setting for an individual mob to determine
--  whether or not the mob is enabled, and if various actions (like adding to the character select or skins) should be undertaken
function PlayablePets.MobEnabled(mob, modname)
	
	local isenabled = false
	if mob.release_id and not CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs[mob.release_id]) then
		return false --Ain't gonna continue if you not in the game yet boi.
	end

	if mob.mod_id then
		if PlayablePets.IsModEnabled(mob.mod_id) and GetModConfigData(mob.fancyname, modname) == PP_SETTINGS.ENABLE then
			isenabled = true
		end
	elseif GetModConfigData(mob.fancyname, modname) == PP_SETTINGS.ENABLE then
		isenabled = true
	end
	
	if TheNet:GetServerGameMode() == "lavaarena" and not mob.forge then
		isenabled = false
	end
	
	if MOBGIANTS == 0 and mob.mobtype == MOBTYPE.GIANT then
		isenabled = false
	elseif MOBGIANTS == 2 and mob.mobtype ~= MOBTYPE.GIANT then
		isenabled = false
	end	

	if mob.test and not PlayablePets.DebugTest then
		--for experimental mobs
		isenabled = false
	end
	
	return isenabled
end

------------------------------------------------------------------
-- Modmain Functions
------------------------------------------------------------------

local function DefaultPerkFN(inst)
	--Mobs just use their default forge_fn function for now.
end

--Used to fill out some important variables with a default value to
--either avoid crashes or fill in info that doesn't need to be manually
--added.
function PlayablePets.SetGlobalData(mob, data)
	if not STRINGS.CHARACTERS[string.upper(mob)] then
		STRINGS.CHARACTERS[string.upper(mob)] = STRINGS.CHARACTERS.MOBPLAYER
	end
	TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT[string.upper(mob)] = {}
	
	--Reforged compatability, players need a default perk to be playable.
	--And I ain't doin that manually.
	if TheNet:GetServerGameMode() == "lavaarena" then
		AddPerk(mob, "Default", DefaultPerkFN, nil, {atlas = "images/reforged.xml", tex = "forge_icon.tex"}, 0)
		
		TUNING.FORGE.CHARACTER_ICON_INFO[mob] = {atlas = "images/avatars/avatar_"..mob..".xml", tex = "avatar_"..mob..".tex"}
		STRINGS.REFORGED.PERKS[mob] = {
			Default = {
				DESCRIPTION = STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS[mob] or "TODO"
			}
		}
	end

	--data.mimic_prefab is a table containing vanilla/other mod's prefab names.
	--used to "link" player prefabs to their non-playable vanilla counterparts.
	if data and data.mimic_prefab then
		for i, v in ipairs(data.mimic_prefab) do
			PP_MIMICLIST[v] = mob
		end
	end
end

local SkinsPuppet = require "widgets/skinspuppet"

PlayablePets.RegisterPuppetsAndSkins = function(names, puppets, skins)
	if puppets and SkinsPuppet.pp_puppets and skins and SkinsPuppet.pp_skins then
		for i, v in ipairs(names) do
			if puppets[v] then
				SkinsPuppet.pp_puppets[v] = puppets[v]
			end
			if skins[v] then
				SkinsPuppet.pp_skins[v] = skins[v]
			end
		end
	end
end

--Gets a random mob and returns it as a string, every mobplayer should have a puppet so it should
--function nicely as a way to get a random mob.
PlayablePets.GetRandomMob = function() 
	local k_names = {}
    for k in pairs(SkinsPuppet.pp_puppets) do
		table.insert(k_names, k)
    end
	local mob = k_names[math.random(#k_names)]
	d_print("GetRandomMob returned "..mob)
	return mob
end

PlayablePets.CanMimicTarget = function(target) 
	return mob
end
---------------------------------------------------
-- General Utility
---------------------------------------------------
PlayablePets.IsAboveLand = function(pos)
	local valid = (not TheWorld.Map:IsOceanTileAtPoint(pos.x, pos.y, pos.z) and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z)) and not TheWorld.Map:IsInvalidTileAtPoint(pos.x, pos.y, pos.z)
	d_print("PlayablePets.IsAboveLand: "..tostring(valid))
	return valid
end

PlayablePets.IsAboveLandAtPoint = function(x, y, z)
	return PlayablePets.IsAboveLand({x = x, y = y, z = z})
end

PlayablePets.IsAboveVoid = function(pos) --deprecated
	return TheWorld:HasTag("cave") and (IsOceanTile(TheWorld.Map:GetTileAtPoint(pos:Get())) or TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z)) or false
end

PlayablePets.CheckWaterTile = function(pos)
	--deprecated, use the above one instead
	d_print("WARNING: PlayablePets.CheckWaterTile is deprecated, tell the developer if you see this.")
	PlayablePets.IsAboveLand(pos)
end

local function ShouldIgnoreLimits()
	return not TheWorld:HasTag("cave") and TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire"
end

PlayablePets.LimitFlyers = function(inst)
	if not ShouldIgnoreLimits() then
		inst.Physics:CollidesWith(COLLISION.LIMITS)
	end
end

PlayablePets.ReplacePrefab = function(inst, prefab) --used for pp items when dlc mods are enabled.
	local rep = SpawnPrefab(prefab)
	rep.Transform:SetPosition(inst:GetPosition():Get())
	inst:Remove()
end

PlayablePets.SpawnLine = function(inst, prefab, count, distance, rot) 
    if prefab then
		local pos = inst:GetPosition()
		local angle = -inst.Transform:GetRotation() * DEGREES
		local dist = distance or 1
		for i = 1, count do
			local targetpos = Point(pos.x + ((dist*i) * math.cos(angle)), 0, pos.z + ((dist*i) * math.sin(angle)))
			local c = SpawnPrefab(prefab)
			c.Transform:SetPosition(targetpos:Get())
			if rot then
				c.Transform:SetRotation(rot)
			end
		end	
	end
end
------------------------------------------------------------------
-- Mod Compatibility Util
------------------------------------------------------------------
PlayablePets.GetModAssets = function(mod, assets, assetoverride)
	if mod then
		return assetoverride or {}
	else
		return assets
	end
end
------------------------------------------------------------------
-- Skin Functions
------------------------------------------------------------------

--Used only during respawnfromghost event.
PlayablePets.SetSkin = function(inst, mob, wardrobe)
	if not inst:HasTag("playerghost") and inst.components.ppskin_manager then
		inst.components.ppskin_manager:LoadSkin(mob, wardrobe)
	end	
end

------------------------------------------------------------------
--NightVision & Other Visions
------------------------------------------------------------------

local SEASON_COLOURCUBES =
{
    autumn = "day05_cc.tex",
    winter = "snow_cc.tex",
    spring = "spring_day_cc.tex",
    summer = "summer_day_cc.tex",
}

local function GetColourCube()
	if not TheWorld:HasTag("cave") then
		if TheWorld.state.iswinter then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["winter"]
		elseif TheWorld.state.issummer then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["summer"]
		elseif TheWorld.state.isautumn then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["autumn"]
		elseif TheWorld.state.isspring then
			return "images/colour_cubes/"..SEASON_COLOURCUBES["spring"]
		else
			return "images/colour_cubes/ruins_light_cc.tex"
		end
	else
		return "images/colour_cubes/ruins_light_cc.tex"
	end
end

local NIGHTVISION_COLOURCUBES =
{
    day = "images/colour_cubes/ruins_light_cc.tex",
    dusk = "images/colour_cubes/ruins_dim_cc.tex",
    night = "images/colour_cubes/ruins_light_cc.tex",
    full_moon = "images/colour_cubes/purple_moon_cc.tex",
}

local function CanNightVision(inst)
	return (TheWorld.state.isnight or TheWorld:HasTag("cave") or inst:HasTag("inside_interior")) and MOBNIGHTVISION == "Enable" 
end

local function DoNightVision(inst, enable)
	inst:DoTaskInTime(0, function(inst) --some conflicts with DLC mods force us to delay this a bit.
		local enable = enable or true
		if CanNightVision(inst) and enable then
			inst.components.playervision:ForceNightVision(true)
			inst.components.playervision:SetCustomCCTable(NIGHTVISION_COLOURCUBES)
		else
			inst.components.playervision:ForceNightVision(false)
			inst.components.playervision:SetCustomCCTable(nil)
		end
		if inst.userid and inst.userid == "KU_flOiBW2C" then --Ceph is cursed
			inst.components.playervision:ForceNightVision(false)
			inst.components.playervision:SetCustomCCTable(nil)
		end
	end)
end

local function StopNightVision(inst)
	DoNightVision(inst, false)
end

PlayablePets.RemoveNightVision = function(inst)
	DoNightVision(inst, false)
	inst:RemoveEventCallback("enterinterior_client", DoNightVision)
	inst:RemoveEventCallback("leaveinterior_client", PlayablePets.RemoveNightVision)
end

PlayablePets.SetNightVision = function(inst, enable, cctable) 
	DoNightVision(inst) 

	--SetNightVision currently gets called multiple times over a playthrough
	--I don't know if ListenForEvent overrides it again or if it infinitely stacks.
	--So remove the eventcallback every call just incase. Remove it when you make
	--it a single call.
	inst:RemoveEventCallback("enterinterior_client", DoNightVision)
	inst:ListenForEvent("enterinterior_client", DoNightVision) --For ATC, always turn on nightvision in houses.

	inst:RemoveEventCallback("leaveinterior_client", PlayablePets.RemoveNightVision)
	inst:ListenForEvent("leaveinterior_client", PlayablePets.RemoveNightVision)

	inst:WatchWorldState("phase", DoNightVision)
end

local no_cc = "images/colour_cubes/identity_colourcube.tex"
local NO_COLOURCUBES =
{
    day = no_cc,
    dusk = no_cc,
    night = no_cc,
    full_moon = no_cc,
}

PlayablePets.SetNoCC = function(inst, enable, cctable) --FUCK COLOURCUBES
	inst:DoTaskInTime(0, function(inst) --some DLC mods do postinits on playervision, delay this so they get the information they need when spawning in.
    	inst.components.playervision:ForceNightVision(false)
		inst.components.playervision:SetCustomCCTable(NO_COLOURCUBES)
	end)
end

----------------------------------------------------------
-- Transform/Imposter functions
----------------------------------------------------------
PlayablePets.DoMimicry = function(inst, target)
	--Note: Mimicry should really only able to used on those in PP_MIMICLIST and players.
	if target and target:IsValid() then
		if inst.components.seamlessplayerswapper.main_data.transform_fx then
			local fx = SpawnPrefab(inst.components.seamlessplayerswapper.main_data.transform_fx)
			fx.Transform:SetPosition(inst:GetPosition():Get())
		end
		if target:HasTag("player") or PP_MIMICLIST[target.prefab] then
			--If target is a player, we just go ahead and transform into them.
			if inst.components.ppcurrency and inst.components.ppcurrency:GetCurrent() > 0 then
				TheWorld.components.ppbank_manager:Deposit(inst)
				--inst.components.seamlessplayerswapper.main_data.ppcurrency = inst.components.ppcurrency:GetCurrent()
			end
			if inst.components.ppskin_manager and inst.components.ppskin_manager.skin and not inst.components.seamlessplayerswapper.main_data.saved_ppskin then --save the original skin
				inst.components.seamlessplayerswapper.main_data.saved_ppskin = inst.components.ppskin_manager.skin
			end
			inst.components.seamlessplayerswapper.main_data.is_mimic = true
			inst.components.inventory:Close(true) --fixes the vanilla "playerhearing" crash that happens when transforming sometimes.
			inst.components.seamlessplayerswapper:_StartSwap(target:HasTag("player") and target.prefab or PP_MIMICLIST[target.prefab])
		else
			--This shouldn't happen.
			print("PP DoMimicry Error! "..inst:GetDisplayName().." tried to transform into an invalid prefab - "..target.prefab)
		end
	end
end

PlayablePets.UndoMimicry = function(inst)
	inst.components.seamlessplayerswapper.main_data.is_mimic = nil
	if inst.components.seamlessplayerswapper.main_data.transform_fx then
		local fx = SpawnPrefab(inst.components.seamlessplayerswapper.main_data.transform_fx)
		fx.Transform:SetPosition(inst:GetPosition():Get())
	end
	if inst.components.ppcurrency and inst.components.ppcurrency:GetCurrent() > 0 then
		TheWorld.components.ppbank_manager:Deposit(inst)
		inst.components.seamlessplayerswapper.main_data.ppcurrency = inst.components.ppcurrency:GetCurrent()
	end
	if inst.components.seamlessplayerswapper.main_data.saved_ppskin then
		inst.components.seamlessplayerswapper.main_data.setskin = inst.components.seamlessplayerswapper.main_data.saved_ppskin
		inst.components.seamlessplayerswapper.main_data.saved_ppskin = nil
	end
	if inst.components.named then
		inst:RemoveComponent("named")
	end
	inst.components.seamlessplayerswapper.main_data.mimic_name = nil
	inst.components.inventory:Close(true) --fixes the vanilla "playerhearing" crash that happens when transforming sometimes.
	inst.components.seamlessplayerswapper:SwapBackToMainCharacter()
end
----------------------------------------------------------
-- Init Functions
----------------------------------------------------------
PlayablePets.SetSkeleton = function(inst, prefab)
	inst.skeleton_override = prefab
end

local function SetDefaultSkeleton(inst)
	inst.skeleton_prefab = inst.skeleton_override or (inst:HasTag("epic") and "pp_skeleton_giant" or "pp_skeleton_generic")
end

--SetChar is seperate from SetCommonStats, this gets called by things like setskindefault or "respawning from ghost" events. SetCommonStats is for masterpostinit almost exclusively.
PlayablePets.CommonSetChar = function(inst, mob, ignorehunger)
	if not inst:HasTag("playerghost") then
		inst.AnimState:SetBank(mob.bank)
		inst.AnimState:SetBuild(mob.build)
		inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
		inst:SetStateGraph(mob.stategraph)
		if MONSTERHUNGER == "Disable" and not ignorehunger then
			inst.components.hunger:Pause()
		end
		if inst.noplatformhopping then
			inst.components.locomotor:SetAllowPlatformHopping(false)
			inst:DoTaskInTime(2, function(inst) inst.components.locomotor:SetAllowPlatformHopping(false) end) --added delay to ensure it gets set
		end
		SetDefaultSkeleton(inst)
	end
end

PlayablePets.CommonOnEquip = function(inst)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if not hands then
		inst.AnimState:OverrideSymbol("swap_object", inst.AnimState:GetBuild(), "swap_object")
		--inst.AnimState:Hide("ARM_carry_up") --added in the wurt/winona skilltree update. Should only affect pigman mobs.
		inst.AnimState:Hide("ARM_carry")
	else
		inst.AnimState:Show("ARM_carry")
	end
end

PlayablePets.CommonOnEquipPig = function(inst) --lazy fix to fix all the pigmen
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if not hands then
		inst.AnimState:OverrideSymbol("swap_object", inst.AnimState:GetBuild(), "swap_object")
		inst.AnimState:Hide("ARM_carry_up") 
	else
		inst.AnimState:Show("ARM_carry_up")
		inst.AnimState:Hide("ARM_carry")
	end
	inst.AnimState:Show("HEAD")
end

PlayablePets.CommonOnUnequip = function(inst)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if not hands then
		inst.AnimState:Hide("ARM_carry_up") --added in the wurt/winona skilltree update. Should only affect pigman mobs.
		inst.AnimState:Hide("ARM_carry")
	end
end

PlayablePets.MakeHeavyLifter = function(inst) 
	inst.heavylifter = true
	--inst:ListenForEvent("onequip", OnEquip)
end

--deprecated, use GameModeOverrides
PlayablePets.SetGameModeOverrides = function(inst, forge_fn)
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = forge_fn --does this autorun in Reforged?
	end
end

PlayablePets.GameModeOverrides = function(inst, data)
	if data then
		for k, v in pairs(data) do
			if TheNet:GetServerGameMode() == k then
				v(inst)
			end
		end
	end
end

local function FixSanity(inst)
	inst.components.sanity.ignore = nil
	if inst.components.sanity:IsLunacyMode() then
		inst.components.sanity:SetPercent(0)
		inst.components.sanity.ignore = true
	else
		inst.components.sanity:SetPercent(1)
		inst.components.sanity.ignore = true
	end
end

local function SetLunacyImmunity(inst, mode)
	if inst.components.sanity and inst.components.sanity.ignore == true then
		inst:ListenForEvent("sanitymodechanged", FixSanity)
	end
end

PlayablePets.ToggleWalk = function(inst)
	if inst.shouldwalk then
		inst.shouldwalk = false
	elseif not inst.shouldwalk then
		inst.shouldwalk = true
	end
	d_print("Toggle Walk - shouldwalk = "..tostring(inst.shouldwalk))
end

PlayablePets.DoDeath = function(inst)
	--d_print("PlayablePets.DoDeath ran")
	inst.Transform:SetScale(1, 1, 1)
	if MOBGHOST == "Enable" then
		inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = inst.noskeleton and false or MOBSKELETONS})
	else
	   TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
	end
end

--TODO: Make a growable pets version?
PlayablePets.SetCommonStats = function(inst, mob, ishuman, ignorepvpmultiplier, ignoreweb)
	--To clear invasive player_common symbol swaps. E.G. kelpy
	inst.AnimState:ClearAllOverrideSymbols()
	if mob then
		PlayablePets.DebugPrintData("SetCommonStats", mob)
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.hunger:SetMax(mob.hunger)
		inst.components.sanity:SetMax(mob.sanity)
		inst.components.hunger:SetRate(mob.hungerrate or TUNING.WILSON_HUNGER_RATE)
		inst.components.hunger:SetKillRate(mob.hunger_killrate or inst.components.health.maxhealth/TUNING.STARVE_KILL_TIME)
		
		if not inst.components.sleeper then
			inst:AddComponent("sleeper")
			inst.components.sleeper.sleeptestfn = nil
			inst.components.sleeper.waketestfn = nil
		end

		if not inst.components.timer then
			inst:AddComponent("timer") --This does a better job at tracking ability cooldowns.
		end
		
		if MONSTERHUNGER == "Disable" then
			inst.components.hunger:Pause() 
		end
		
		--Combat--
		inst.components.combat.playerdamagepercent = MOBPVP
		if ignorepvpmultiplier then 
			inst.components.combat.pvp_damagemod = 1 --this is for mobs that don't do double damage against non-playable characters.
		else
			inst.components.combat.pvp_damagemod = TUNING.PVP_DAMAGE_MOD
		end		
		inst.components.combat:SetAttackPeriod(mob.attackperiod or 0)
		inst.components.combat:SetRange(mob.range, mob.hit_range or mob.range)
		inst.components.combat:SetDefaultDamage(mob.damage) --does double damage against mobs.
		
		local s = mob.scale or 1
		inst.Transform:SetScale(s, s, s)
		
		if not inst:HasTag("playerghost") then
			inst.AnimState:SetBank(mob.bank)
			inst.AnimState:SetBuild(mob.build)
			inst:SetStateGraph(mob.stategraph)
		end
		inst.components.locomotor.runspeed = (mob.runspeed)
		inst.components.locomotor.walkspeed = (mob.walkspeed)

		--Variables
		inst.mobplayer = true
		inst.ghostbuild = "ghost_monster_build"
		inst.mob_table = mob 

		inst.AnimState:Hide("ARM_carry_up") --didn't want to update this manually for all pigmobs.
		
		if not inst.isshiny then --I don't think this check is needed since this gets overrided by onload stuff anyways, but just incase this gets used somewhere outside of masterpostinit...
			inst.isshiny = 0 --for identifying skins on the mob
		end
		
		if not ishuman then
			--inst.components.talker:IgnoreAll()
			if MONSTERSANITY == "Disable" then
				d_print("Added sanity and lunacy immunity")
				inst.components.sanity.ignore = true
				SetLunacyImmunity(inst)
				inst:DoTaskInTime(0, FixSanity)
			end
			inst.components.locomotor.fasteronroad = false
			if MOBNIGHTVISION ~= "Disable2" and TheNet:GetServerGameMode() ~= "lavaarena" then
				d_print("grue immunity: true")
				inst.components.grue:AddImmunity("mobplayer")
			end
			PlayablePets.SetSkeleton(inst)

			--ATC and other DLC stuff
			inst:DoTaskInTime(0, function(inst) --hayfever isn't added immediately onspawn.
				if inst.components.hayfever then
					inst.components.hayfever.imune = true
				end
			end)
		else
			d_print("humanoid: true")
			inst.ishuman = true
		end
		
		if ignoreweb then
			d_print("mob ignores webs: true")
			inst.components.locomotor:SetSlowMultiplier( 1 )
			inst.components.locomotor:SetTriggersCreep(false)
			inst.components.locomotor.pathcaps = { ignorecreep = true }
		end
	end
	if not inst.components.ppskin_manager then
		inst:AddComponent("ppskin_manager")
	end
	if not inst.components.playablepet then
		inst:AddComponent("playablepet")
	end
	if inst._stormimmune then
		d_print("storm immunity: true")
		PlayablePets.SetStormImmunity(inst, true)
	end
	if inst.components.cursable then
		inst:RemoveComponent("cursable")
	end
	if inst.userid and inst.userid == "KU_flOiBW2C" then --Ceph is cursed
		inst.components.hunger:Resume()
		inst.components.sanity.ignore = false
		inst.components.temperature.maxtemp = 80
		inst.components.temperature.min_temp = 0
		inst.components.moisture:SetInherentWaterproofness(0)
	end
	--For Mimicry stuff
	local main_data = inst.components.seamlessplayerswapper.main_data
	if main_data.ppcurrency then
		inst.components.ppcurrency:SetAmount(main_data.ppcurrency)
		inst.components.seamlessplayerswapper.main_data.ppcurrency = nil
	end
	if main_data.setskin then
		inst.components.ppskin_manager.skin = main_data.setskin
		inst.components.seamlessplayerswapper.main_data.setskin = nil
	end

	inst:ListenForEvent("onfallinvoid", function(inst, data) --I don't want to update every stategraph...
		if not inst.components.health:IsDead() and not inst:HasTag("flying") then
			inst.sg:GoToState("abyss_fall")
		end
	end)
end

----------------Amphibious---------------------------

local function SetLandHealthDrain(inst)
	if inst.landdraintask then
		inst.landdraintask:Cancel()
		inst.landdraintask = nil
	end
	inst.landdraintask = inst:DoPeriodicTask(0.75, function(inst) 
		local pos = inst:GetPosition()
		--doesn't count boat incase someone tries to get a cheesy boat kill.
		if not inst.landprotected and TheWorld.Map:IsVisualGroundAtPoint(pos.x, pos.y, pos.z) and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.y, pos.z) then
			inst.components.health:DoDelta(-inst.components.health.maxhealth/16, false)
		end
	end)
end

local function GetRandomOceanPosition(inst)
		--print("DEBUG: getrandomposition running")
		local pt = inst:GetPosition()
		local ground = TheWorld
		local range = 900
        local result_offset = FindValidPositionByFan(range, range, range, function(offset)
            local test_point = pt + offset
			return ground.Map:IsOceanTileAtPoint(test_point.x, 0, test_point.z) and not TheWorld.Map:GetPlatformAtPoint(test_point.x, test_point.y, test_point.z)
        end)
        local newpt = result_offset and pt + result_offset or nil
		--print(newpt)
		return newpt
end

local function SendToSea(inst)
	--print("DEBUG: SendToSea running")
	local ground = TheWorld
	if TheWorld.Map:IsVisualGroundAtPoint(inst:GetPosition():Get()) or TheWorld.Map:GetPlatformAtPoint(inst:GetPosition():Get()) then
    	local pt = GetRandomOceanPosition(inst)--Point(inst.Transform:GetWorldPosition())
		if pt ~= nil then
            pt.y = 0
            inst.Physics:Stop()
				
            inst.Physics:Teleport(pt.x,pt.y,pt.z)
        else    --inst:Hide()
			d_print("PP Error: Couldn't find an ocean position for "..inst:GetDisplayName().."as"..inst.prefab.."!")
		end	
	end
	inst.landprotected = nil
end

local function SetAmphibious_OnEnterInterior(inst)
	inst.components.locomotor:SetAllowPlatformHopping(false)
end

local function SetAmphibious_OnLeaveInterior(inst)
	inst.components.locomotor:SetAllowPlatformHopping(true)
end

PlayablePets.SetAmphibious = function(inst, bank, bank_water, simple, oceanonly, noplatformhopping)
	inst.components.locomotor:SetAllowPlatformHopping(false)
	inst.noplatformhopping = true
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		if not simple then --ghosts and such don't need this component
			if noplatformhopping == nil then
				inst.noplatformhopping = nil
				inst.components.locomotor:SetAllowPlatformHopping(true)
				inst:ListenForEvent("enterinterior", SetAmphibious_OnEnterInterior)
				inst:ListenForEvent("leaveinterior", SetAmphibious_OnLeaveInterior)
			end	
			inst:AddComponent("amphibiouscreature")
			inst.components.amphibiouscreature:SetBanks(bank, bank_water)
		end
		if inst.components.drownable and not TheWorld:HasTag("cave") then
			inst.components.drownable.enabled = false
		end
		inst.amphibious = true
		if oceanonly then
			inst.components.locomotor:SetAllowPlatformHopping(false)
			inst.oceanonly = true
			inst.landprotected = true
			SetLandHealthDrain(inst)
			inst:DoTaskInTime(0, function(inst)
				SendToSea(inst)
			end)
		else
			if not TheWorld:HasTag("cave") then
				inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
			end
		end
	end
end

PlayablePets.RemoveAmphibious = function(inst)
	inst.components.locomotor:SetAllowPlatformHopping(true)
	inst.noplatformhopping = nil
	if inst.components.drownable then
		inst.components.drownable.enabled = true
	end
	inst.amphibious = nil
end

----------------------------------------------------------

PlayablePets.SetCommonLootdropper = function (inst, prefaboverride)
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefaboverride and prefaboverride or inst.prefab)
end

PlayablePets.SetCommonWeatherResistances = function (inst, heat, cold, wetness)
	if MOBWEATHER > 0 then
		if inst.components.temperature then
			if heat or MOBWEATHER > 1 then
				inst.components.temperature.maxtemp = MOBWEATHER > 1 and 60 or heat --prevents overheating
			end
			if cold or MOBWEATHER > 1  then
				inst.components.temperature.mintemp = MOBWEATHER > 1 and 20 or cold --prevents freezing
			end
			if wetness or MOBWEATHER > 1  then
				inst.components.moisture:SetInherentWaterproofness(1)
			end
		end
	end
end

PlayablePets.OnLoadCommon = function (inst, data)
	PlayablePets.DebugPrint("PPDEBUG: Running OnLoadCommon on "..inst.prefab)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.ocean_location then
			--inst:DoTaskInTime(0, function(inst) inst.Transform:SetPosition(data.ocean_location:Get()) end)
		end
		if inst.onloadfn then
			inst.onloadfn(inst, data)
		end
	end
end

PlayablePets.OnSaveCommon = function (inst, data)
	PlayablePets.DebugPrint("PPDEBUG: Running OnSaveCommon on "..inst.prefab)
	local data = {}
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	if inst.oceanonly then
		data.ocean_location = inst:GetPosition()
	end
	if inst.onsavefn then
		inst.onsavefn(inst, data) --TODO do we want to save onsavefn? Should it ever change?
	end
end

PlayablePets.SetCommonSaveAndLoad = function(inst, savefn, loadfn)
	inst.onsavefn = savefn
	inst.OnSave = PlayablePets.OnSave
	--
	inst.onloadfn = loadfn
	inst.OnLoad = PlayablePets.OnLoadCommon
end

--This runs on the client
PlayablePets.SetSandstormImmunity = function(inst)
	--deprecated
end

PlayablePets.SetStormImmunity = function(inst, force)
	inst._stormimmune = force or true --this triggers a check in the postinit for stormwatcher
end

--This runs on server
PlayablePets.SetSandstormImmune = function(inst, force)
	--deprecated
	PlayablePets.SetStormImmunity(inst, force)
end

PlayablePets.SetCommonStatResistances = function (inst, fire_mult, acid_mult, poison_mult, freeze_mult)
	if fire_mult then
		inst.components.health.fire_damage_scale = fire_mult --this really only accounts for damage caused by fire, not fire attacks
	end
	if acid_mult then
		inst.acidmult = acid_mult 
	end
	
	if poison_mult then --if you're looking to make it immune to poison use SetPoisonImmune instead
		if poison_mult == 0 then
			PlayablePets.SetPoisonImmune(inst, true)
		else
			inst.poisonmult = poison_mult
		end
	end
	if inst.components.freezable then
		--TEMPORARY! TODO MOVE ALL FUNCTION CALLS DOWN!
		inst:DoTaskInTime(0.5, function(inst)
			if inst.components.freezable then
				inst.components.freezable:SetResistance(freeze_mult or 4)
			end
		end)
	end
	PlayablePets.DebugPrintData("SetCommonStatResistances", {fire_res = fire_mult, acid_res = acid_mult, poison_res = poison_mult, freeze_res = freeze_mult})
end


PlayablePets.RevRestore = function(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
	if mob then
		inst.components.locomotor.runspeed = (mob.runspeed)
		inst.components.locomotor.walkspeed = (mob.walkspeed)
	end
	if not ishuman then
		inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
		inst.components.health:SetPercent(1) --Returns health to max health upon reviving
		
		inst.components.locomotor.fasteronroad = false
		
		--inst.components.talker:IgnoreAll()
		if MONSTERHUNGER == "Disable" then
			inst.components.hunger:Pause()
		end
		if MONSTERSANITY == "Disable" then
			inst.components.sanity.ignore = true
			SetLunacyImmunity(inst)
			FixSanity(inst)
		end
		
		if MOBNIGHTVISION ~= "Disable2" then
			inst.components.grue:AddImmunity("mobplayer")
		end
	end
	if isflying then
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end
	if noshadow then
		inst.DynamicShadow:Enable(false)
	end
	
	if inst.amphibious then
		if ShouldIgnoreLimits() then
			if inst.noplatformhopping then
				inst.components.locomotor:SetAllowPlatformHopping(false)
			end
			if inst.oceanonly then
				inst.components.drownable.enabled = false
				inst.landprotected = true
				SendToSea(inst) --will I need to make a check for this?
			else
				inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
			end
		else
			inst.Physics:CollidesWith(COLLISION.LIMITS)	
		end
	else
		inst.components.locomotor:SetAllowPlatformHopping(true)
	end
	
	if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:Enable(true)
    end

    inst.components.health:SetInvincible(false)
    if inst.components.playercontroller ~= nil then
       inst.components.playercontroller:Enable(true)
    end
	
	if inst._stormimmune then
		PlayablePets.SetStormImmunity(inst, true)
	end

    inst:ShowHUD(true)
    inst:SetCameraDistance()

    SerializeUserSession(inst)
end

PlayablePets.LandFlyingCreature = function(inst)
	if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
		LandFlyingCreature(inst)
	end
end

PlayablePets.RaiseFlyingCreature = function(inst)
	--note this for some reason stops you from steering.
	if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
		RaiseFlyingCreature(inst)
	end
end

---------RMB Ability-----------------

local function RMB_NoHoles(pt)
    return not TheWorld.Map:IsGroundTargetBlocked(pt) 
end

local function RMB_ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, RMB_NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, RMB_NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function RMB_GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() then
            return {inst._rmb_action}
        end
    end
    return {}
end

local function RMB_OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil then
        inst.components.playeractionpicker.pointspecialactionsfn = RMB_GetPointSpecialActions
    end
end

PlayablePets.AddRMBAbility = function(inst, action)
	inst:ListenForEvent("setowner", RMB_OnSetOwner)
	inst._rmb_action = action or ACTIONS.PP_RANGEDSPECIAL
	
	inst:AddComponent("reticule")
    inst.components.reticule.targetfn = RMB_ReticuleTargetFn
    inst.components.reticule.ease = true
end

----------------------------------------------------------
-- AnimState/Visual Functions
----------------------------------------------------------
PlayablePets.ApplyMultiBuild = function (inst, symbols, build)
	if symbols and build then
		for pt = 1, #symbols do
			for i, v in ipairs(symbols[pt]) do
				inst.AnimState:OverrideSymbol(v, build.."_pt"..pt, v)
			end
		end
	end
end

----------------------------------------------------------
-- Targeting Functions
----------------------------------------------------------
PlayablePets.GetClampedPosition = function(inst, targetpos, max_dist)
	local pos = inst:GetPosition()
	local dist = math.sqrt(inst:GetDistanceSqToPoint(targetpos.x, 0, targetpos.z))
	
	if dist <= max_dist then
		return targetpos
	end
	
	local dx, dy, dz = targetpos.x - pos.x, 0, targetpos.z - pos.z
	local nx, ny, nz = dx/dist, 0, dz/dist
	local clamped_pos = Point(pos.x + nx * max_dist, 0, pos.z + nz * max_dist)
	
	return clamped_pos
end

PlayablePets.GetExcludeTags = function(inst, addtags, removetags)
	--This function returns tags meant to be used for NOTAG parameters.
	--addtags parameter adds more tags in addition to the automated ones.
	--removetags force excludes any tags that could be included automatically.
	local tags = {"INLIMBO", "playerghost", "notarget"}
	if addtags then
		for i, v in ipairs(addtags) do
			table.insert(tags, v)
		end
	end
	if inst.components.playablepet and inst.components.playablepet.ally_tags then
		for i, v in ipairs(inst.components.playablepet.ally_tags) do
			table.insert(tags, v)
		end
	end

	if inst:HasTag("player") or inst:HasTag("companion") then
		if TheNet:GetServerGameMode() == "lavaarena" then
			table.insert(tags, "player")
			table.insert(tags, "companion")
			table.insert(tags, "abigail")
		end
	end

	--don't want to update the params and theres too many overlapping shadows.
	--so fuck it, just hit shadows anyways.

	table.insert(tags, "shadowcreature")
	if inst.components.sanity and inst.components.sanity:IsInsane() then
		table.remove(tags, "shadowcreature")
	end

	if removetags then
		for i, v in ipairs(removetags) do
			table.remove(tags, v)
		end
	end

	return tags
end

----------------------------------------------------------
-- Combat Functions
----------------------------------------------------------


--TODO Redo
local function GetAttackedMultiplier(inst)
	if inst.components.combat then
		local lastattacked = inst.components.combat.lastwasattackedtime
		local currenttime = GetTime()
		local mult = 0
		if (lastattacked + 20) < currenttime then
			mult = 1
		elseif (lastattacked + 16)	< currenttime then
			mult = 0.66
		elseif (lastattacked + 12) < currenttime then
			mult = 0.25
		elseif (lastattacked + 8) < currenttime then
			mult = 0.05
		end
		return mult
	else
		return 1
	end
end

local function CanRegenHP(inst)
	return not inst.ispoisoned and not inst._forcedsleep and not (inst.components.debuffable and inst.components.debuffable:HasDebuff("sk_poison_debuff"))
end

--TODO write a formula for this shit, its not pleasant!
local function GetAttackMultiplier(inst)
	if inst.components.combat then
		local lastattack = inst.components.combat.laststartattacktime
		local currenttime = GetTime()
		if (lastattack + 20) < currenttime then
			return 1
		elseif (lastattack + 16)	< currenttime then
			return 0.66
		elseif (lastattack + 12) < currenttime then
			return 0.25
		elseif (lastattack + 8) < currenttime then
			return 0.05
		else
			return 0
		end
	else
		return 1
	end
end

PlayablePets.SetPassiveHeal = function(inst, delta)
	if SLEEP_REGEN > 0 then
		if inst.pp_passive_heal_task then
			inst.pp_passive_heal_task:Cancel()
			inst.pp_passive_heal_task = nil
		end
		inst.pp_passive_heal_task = inst:DoPeriodicTask(delta or 3, function(inst)
			local health = inst.components.health
			local combat = inst.components.combat
			if not inst.no_healing and CanRegenHP(inst) then
				local mult = math.min(GetAttackMultiplier(inst), GetAttackedMultiplier(inst))
				if inst.components.hunger and not inst.components.hunger:IsStarving() then
					inst.components.health:DoDelta( (health.maxhealth/12) * mult * SLEEP_REGEN, true)
				end	
				if inst.components.health:GetPercent() < 1 and not health:IsDead() then
					inst.components.hunger:DoDelta(-(inst.components.hunger.max * 0.055) * mult, true)
				end
			end
		end)
	end
end

PlayablePets.StopPassiveHeal = function(inst)
	if inst.pp_passive_heal_task then
		inst.pp_passive_heal_task:Cancel()
		inst.pp_passive_heal_task = nil
	end
end

PlayablePets.SleepHeal = function(inst, forced, mult)
	if not forced and SLEEP_REGEN > 0 and CanRegenHP(inst) then
		local health = inst.components.health
		local combat = inst.components.combat
		if inst.components.hunger and not inst.components.hunger:IsStarving() then
			inst.components.health:DoDelta((health.maxhealth/12) * (mult or 1) * GetAttackedMultiplier(inst) * SLEEP_REGEN, true)
			d_print("//////// PP DEBUG: Sleep Healing ////////")
			d_print("healrate: "..tostring((health.maxhealth/12) * GetAttackedMultiplier(inst) * SLEEP_REGEN).." per tick")
			d_print("attacked mult: "..tostring(GetAttackedMultiplier(inst)))
			d_print("SLEEP_REGEN config: "..tostring(SLEEP_REGEN))
		end	
		if inst.components.health:GetPercent() < 1 and not health:IsDead() then
			inst.components.hunger:DoDelta(-(inst.components.hunger.max * 0.055) * (mult or 1) * GetAttackedMultiplier(inst), true)
			d_print("hungerrate: "..tostring(-(inst.components.hunger.max * 0.055) * GetAttackedMultiplier(inst).." per tick"))
		end
		
		inst.components.sanity:DoDelta(1 * GetAttackedMultiplier(inst), true)
		d_print("sanityrate: "..tostring(1 * GetAttackedMultiplier(inst)).." per tick")
		d_print("////////////////////////////////////")
	end
end

PlayablePets.StartSpecialAttack = function(inst) --push code related to damaging taunts here
	inst:PushEvent("cursed_attack")
end

PlayablePets.GetAllyHelp = function(inst, target, tag)
	if target and tag and not (target.components.playercontroller and target:HasTag(tag)) then
		inst.components.combat:SetTarget(target)
		inst.components.combat:ShareTarget(target, 30, function(dude) return dude:HasTag(tag) and not dude.components.health:IsDead() end, 30)
	end
end

local valid_actions = {ACTIONS.HAMMER, ACTIONS.MINE, ACTIONS.CHOP}

local function IsWorkAction(inst)
	if inst.bufferedaction then
		for i, v in ipairs(valid_actions) do
			if inst.bufferedaction.action == v then
				return true
			end
		end
	else
		return false
	end
end

PlayablePets.DoWork = function (inst, strength)
	if inst.bufferedaction and IsWorkAction(inst) and inst.bufferedaction.target and inst.bufferedaction.target.components.workable then
		local worktar = inst.bufferedaction.target.components.workable
		worktar:SetWorkLeft(math.max(worktar.workleft - (strength - 1), 1)) --subtract one from strength because we perform an action to count as one.
	end
	inst:PerformBufferedAction()
end

local function CreateFoodGroup()

end

PlayablePets.SetCanEatAll = function(inst)
	local foodgroup = {}
	for i, v in pairs(FOODTYPE) do	
		--print("Foodgroup debug adding "..v)
		table.insert(foodgroup, v)
	end
	--print("Foodgroup Debug: number of groups is..."..tostring(#foodgroup))
	inst.components.eater:SetDiet(foodgroup, foodgroup)
end

PlayablePets.CanSetFire = function(inst)
	return TheNet:GetServerGameMode() ~= "lavaarena" and MOBFIRE == "Enable"
end

PlayablePets.LogDestructiveAbility = function(inst, target, attackname)
	local pos = inst:GetPosition()
	if target and target:IsValid() then
		print("PP Log - Destructive Ability: "..inst:GetDisplayName().." damaged "..target.prefab..(attackname and " with "..attackname or " at ")..tostring(math.floor(pos.x))..", "..tostring(math.floor(pos.z)).." as "..inst.prefab)
	else
		print("PP Log - Destructive Ability: "..inst:GetDisplayName().." used "..(attackname and attackname or "destructive ability").." at "..tostring(math.floor(pos.x))..", "..tostring(math.floor(pos.z)).." as "..inst.prefab)
	end
end

local REPEL_RADIUS = 5
local REPEL_RADIUS_SQ = REPEL_RADIUS * REPEL_RADIUS

PlayablePets.KnockbackOther = function(inst, other, radius) --knockback
		if other.sg and other.sg.sg.events.knockback then
			other:PushEvent("knockback", {knocker = inst, radius = radius or 5})
		else
			if other ~= nil and not (other:HasTag("epic") or other:HasTag("largecreature") or other:HasTag("structure")) then
		
				if other:IsValid() and other.entity:IsVisible() and not (other.components.health ~= nil and other.components.health:IsDead()) then
					if other.components.combat ~= nil then
						if other.Physics ~= nil then
							local x, y, z = inst.Transform:GetWorldPosition()
							local distsq = other:GetDistanceSqToPoint(x, 0, z)
					--if distsq < REPEL_RADIUS_SQ then
							if distsq > 0 then
								other:ForceFacePoint(x, 0, z)
							end
							local k = .5 * distsq / REPEL_RADIUS_SQ - 1
							other.speed = 60 * k
							other.dspeed = 2
							other.Physics:ClearMotorVelOverride()
							other.Physics:Stop()
						
							if other.components.inventory and other.components.inventory:ArmorHasTag("heavyarmor") or other:HasTag("heavybody") then 
								--Leo: Need to change this to check for bodyslot for these tags.
								other:DoTaskInTime(0.1, function(inst) 
								other.Physics:SetMotorVelOverride(-1, 0, 0) 
								end)
							else
								other:DoTaskInTime(0, function(inst) 
								other.Physics:SetMotorVelOverride(-3, 0, 0) 
								end)
							end
							other:DoTaskInTime(0.4, function(inst) 
							other.Physics:ClearMotorVelOverride()
							other.Physics:Stop()
						end)
					end
				end
			end
		end
	end
end

--Poison
--This is really old stuff, honestly should be its own debuff prefab at this point.
--Reason why it wasn't already is because this was written before Klei added a 
--debuffable component.
PlayablePets.SetPoisonImmune = function(inst, immune)
	if immune and immune == true then
		inst.poisonimmune = true
		if SW_PP_MODE == true then
			--IA's poison component, this should work...
			if inst.components.poisonable then
				inst.components.poisonable.blockall = true
			end
		end
	end
end

local function CalcPoisonDamage(inst)
	if inst.components.health then
		local mult = inst.poisonmult ~= nil and inst.poisonmult or 1
		local issleeping = (inst.sg and inst.sg:HasStateTag("sleeping")) and 0.5 or 1
		local health = inst.components.health.maxhealth
		if health <= 1000 then 
			d_print("CalcPoisonDamage - "..tostring(health * 0.3 / -8 * mult * issleeping))
			return health * 0.3 / -8 * mult * issleeping--Will take up 30% of the user's health.		
		else
		
			return health * (TheNet:GetServerGameMode() == "lavaarena" and 0.05 or 0.15) / -8 * mult * issleeping--Giants get reduced damage
		end
	else
		print("Poison Error: Target's health is nil! Setting tick damage to 4")
		return -4
	
	end	
end

local function GetPoisonCalc(inst)
	local health = inst.components.health.maxhealth
	local damage = -20
	if inst:HasTag("player") then
		damage = math.min(health * 0.3 / 8, -50)
	elseif inst:HasTag("epic") then
		damage = -50
	end
	return damage
end

local function CalcPoisonDamage_Forge(inst)
	if inst.components.health then
		local mult = inst.poisonmult ~= nil and inst.poisonmult or 1
		local health = inst.components.health.maxhealth
		
		return math.max(-50, GetPoisonCalc(inst)) * mult
	end	
end

local function DoPoison(inst)
if inst.ispoisoned and not inst:HasTag("playerghost") then 
	if inst and inst.components.health and not inst.components.health:IsDead() and not inst:HasTag("structure") then
		
		if math.random()<0.20 then
			local fx = SpawnPrefab("poisonbubblep_short")
			fx.Transform:SetPosition(inst:GetPosition():Get())
			if fx and (inst:HasTag("player") or inst.poison_symbol) then
				--players for some reason can't be a parent, but supposedly this method works.
				fx.entity:AddFollower()
				fx.Follower:FollowSymbol(inst.GUID, inst.poison_symbol and inst.poison_symbol or "torso", 0, 0, 0)		
			else
				--for mobs
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(0,0.1,0)
			end
			if inst:HasTag("largecreature") or inst:HasTag("epic") then
				fx.Transform:SetScale(3, 3, 3)
			end
		end	
		if inst:HasTag("player") and inst.mobplayer == true then
			inst.components.health:DoDelta(-0.5, true, "poison")
			inst.components.sanity:DoDelta(-1, true, "poison")
		else
			if TheNet:GetServerGameMode() == "lavaarena" and inst.poisoner and inst.poisoner:IsValid() then
				inst.components.health:DoDelta(CalcPoisonDamage_Forge(inst), true, "poison", nil, nil, true) 
				inst.poisoner:SpawnChild("damage_number"):UpdateDamageNumbers(inst.poisoner, inst, CalcPoisonDamage_Forge(inst), nil, nil, false, inst.poisoner.playercolour)
			else
				inst.components.health:DoDelta(CalcPoisonDamage(inst), true, "poison", nil, nil, true) --do more damage on mobs.
			end
			d_print("PoisonDamage Taken = "..CalcPoisonDamage(inst))
		end
		--Normal Poison: 0.5 health per 2 secs. 120 in one day.
		--Strong Poison: 1 health per 2 secs. 240 in one day.


	inst:DoTaskInTime(2, function(inst) 
		if inst:HasTag("poisoned") then --check to make sure the colors aren't messed up when cured.
			inst.AnimState:SetMultColour(0.5,1,0.2,1) 
		end 
	end) 

	
	end
else --you're cured!
	--inst:RemoveTag("poisoned")
	inst.ispoisoned = nil
	inst.poisoner = nil
	inst.AnimState:SetMultColour(1,1,1,1)
		if inst.poisontask ~= nil then
			inst.poisontask = nil
		end
	end
end

PlayablePets.SetPoison = function(inst, other, duration, perma) --Poison workaround.
	PlayablePets.DebugPrintData("SetPoison", {victim = other.prefab, maxtime = duration, permanent = perma})
	if other and not other.poisonimmune and other.components.health and not other.components.health:IsDead() and MOBPOISON == "Enable" then --make this a debuff, not this hack shit.
		--other:AddTag("poisoned")
		other.poisoner = inst
		other.ispoisoned = true
		if other.poisontask == nil then
			other.AnimState:SetMultColour(0.5,1,0.2,1)
			other.poisontask = other:DoPeriodicTask(2, DoPoison)
			if not other:HasTag("player") or other.mobplayer and not perma then --Players need to craft a cure, so mob players will have theirs cured intime.
				other.poisonwearofftask = other:DoTaskInTime(duration and duration or 16, function(inst) 
				if inst.poisontask then inst.poisontask:Cancel() 
					inst.poisontask = nil 
					inst.AnimState:SetMultColour(1,1,1,1) 
							--inst:RemoveTag("poisoned") 
					inst.ispoisoned = nil
					inst.poisoner = nil
				end 
				end)
			end
		elseif other.poisontask and other.poisonwearofftask then --if already poisoned, reset timer.
			other.poisonwearofftask:Cancel()
			other.poisonwearofftask = other:DoTaskInTime(duration and duration or 16, function(inst) 
				if inst.poisontask then inst.poisontask:Cancel() 
					inst.poisontask = nil 
					inst.AnimState:SetMultColour(1,1,1,1) 
							--inst:RemoveTag("poisoned") 
					inst.ispoisoned = nil
					inst.poisoner = nil
				end 
			end)
		end
	end
end	

------------------------------------------------------------------
-- Reforged Functions
------------------------------------------------------------------
PlayablePets.SetProjectileSource = function(inst, source)
	inst.attacker = source
end

PlayablePets.SetForgeStats = function(inst, MOB) 
	PlayablePets.DebugPrintData("SetForgeStats", MOB)
	inst:AddComponent("shader_manager")
	if MOB then
		inst:DoTaskInTime(0, function(inst)
		inst.components.health:SetMaxHealth(MOB.HEALTH)		
	
		inst.components.combat:SetDefaultDamage(MOB.DAMAGE)
		inst.components.combat:SetAttackPeriod(MOB.ATTACKPERIOD)
	
		inst.components.locomotor.walkspeed = MOB.WALKSPEED
		inst.components.locomotor.runspeed = MOB.RUNSPEED
	
		inst.components.combat:SetRange(MOB.ATTACK_RANGE, MOB.HIT_RANGE)
		if MOB.AOE_RANGE and MOB.AOE_DMGMULT then
			inst.components.combat:SetAreaDamage(MOB.AOE_RANGE, MOB.AOE_DMGMULT)
		end	
		
		inst.components.locomotor:SetAllowPlatformHopping(false)
		inst.noplatformhopping = true
		inst.Physics:CollidesWith(COLLISION.LIMITS)	
		end)
	else
		print("WARNING: You didn't set up a tuning table for this name!")		
	end	
end	

	local function CommonActualRez(inst)
    inst.player_classified.MapExplorer:EnableUpdate(true)

    if inst.components.revivablecorpse ~= nil then
        inst.components.inventory:Show()
    else
        inst.components.inventory:Open()
    end

    inst.components.health.canheal = true
    if not GetGameModeProperty("no_hunger") then
        inst.components.hunger:Resume()
    end
    if not GetGameModeProperty("no_temperature") then
        inst.components.temperature:SetTemp() --nil param will resume temp
    end
    inst.components.frostybreather:Enable()

	--TODO! This is an issue, make a common function for setting burnable/freezables so we can easily reapply the correct stats on revive.
    MakeMediumBurnableCharacter(inst, "torso")
    inst.components.burnable:SetBurnTime(TUNING.PLAYER_BURN_TIME)
    inst.components.burnable.nocharring = true

    MakeLargeFreezableCharacter(inst, "torso")
    inst.components.freezable:SetResistance(4)
    inst.components.freezable:SetDefaultWearOffTime(TUNING.PLAYER_FREEZE_WEAR_OFF_TIME)

    inst:AddComponent("grogginess")
    inst.components.grogginess:SetResistance(3)

    inst.components.moisture:ForceDry(false)

    inst.components.sheltered:Start()

    inst.components.debuffable:Enable(true)

    inst.components.sanity.ignore = GetGameModeProperty("no_sanity")

    inst.components.age:ResumeAging()

    if inst.rezsource ~= nil then
        local announcement_string = GetNewRezAnnouncementString(inst, inst.rezsource)
        if announcement_string ~= "" then
            TheNet:AnnounceResurrect(announcement_string, inst.entity)
        end
        inst.rezsource = nil
    end
    inst.remoterezsource = nil
end

local function DoActualRezFromCorpse(inst, source)
    if not inst:HasTag("corpse") then
        return
    end

    SpawnPrefab("lavaarena_player_revive_from_corpse_fx").entity:SetParent(inst.entity)

    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")

    inst.sg:GoToState("corpse_rebirth")

    inst.player_classified:SetGhostMode(false)

    local respawn_health_precent = inst.components.revivablecorpse ~= nil and inst.components.revivablecorpse:GetReviveHealthPercent() or 1

    if source ~= nil and source:IsValid() then
        if source.components.talker ~= nil then
            source.components.talker:Say(GetString(source, "ANNOUNCE_REVIVED_OTHER_CORPSE"))
        end

        if source.components.corpsereviver ~= nil then
            respawn_health_precent = respawn_health_precent + source.components.corpsereviver:GetAdditionalReviveHealthPercent()
        end
    end

    CommonActualRez(inst)

    inst.components.health:SetCurrentHealth(inst.components.health:GetMaxWithPenalty() * math.clamp(respawn_health_precent, 0, 1))
    inst.components.health:ForceUpdateHUD(true)

    inst.components.revivablecorpse:SetCorpse(false)
    if TheWorld.components.lavaarenaevent ~= nil and not TheWorld.components.lavaarenaevent:IsIntermission() then
        inst:AddTag("NOCLICK")
    end

    inst:PushEvent("ms_respawnedfromghost", { corpse = true, reviver = source })
end

PlayablePets.OnRespawnFromMobCorpse = function(inst, data)
    if not inst:HasTag("corpse") then
        return
    end

    inst.deathclientobj = nil
    inst.deathcause = nil
    inst.deathpkname = nil
    inst.deathbypet = nil
    if inst.components.talker ~= nil then
        inst.components.talker:ShutUp()
    end

    inst:DoTaskInTime(0, DoActualRezFromCorpse, data and data.source or nil)
    inst.remoterezsource = nil

    inst.rezsource =
        data ~= nil and (
            (data.source ~= nil and data.source.prefab ~= "reviver" and data.source.name) or
            (data.user ~= nil and data.user:GetDisplayName())
        ) or
        STRINGS.NAMES.SHENANIGANS
end

--Spiral Knights
local function CanDebuff(inst, type, chance)
	if type then --Gross, make this a table instead. Sheesh.
		if type == "stun" and inst.stun_resist == 0 then
			return false
		end
	
		if type == "fire" and inst.fire_resist == 0 then
			return false
		end
	
		if type == "freeze" and inst.freeze_resist == 0 then
			return false
		end
	
		if type == "shock" and inst.shock_resist == 0 then
			return false
		end
		
		if type == "poison" and inst.poison_resist == 0 then
			return false
		end
	
		if type == "curse" and inst.curse_resist == 0 then
			return false
		end
		
		if type == "sleep" and inst.sleep_resist == 0 then
			return false
		end
		
		return true
	else
		return true
	end
end

local function IsAlive(inst)
	return not inst:HasTag("structure") and not inst:HasTag("wall") 
end

--not using debuffable component so it can be global, also allow inflicter tracking
--TODO Klei added debuffable to the entityscript sometime after the above comment so fuck me I guess.
PlayablePets.InflictStatus = function(inst, target, type, chance, strength)
	if target and CanDebuff(target, type) and IsAlive(target) and type ~= "freeze" and target.sg and target.components.combat and target.components.health and not target.components.health:IsDead() then
		if math.random() <= chance then	
			if target.has_debuff and target.has_debuff["sk_"..type.."_debuff"] then
				--do nothing for right now, change below to use the actual entity instead of prefab name and have this extend it.
			else
				local debuff = SpawnPrefab("sk_"..type.."_debuff")
				debuff.inflicter = inst
				debuff.components.debuff:AttachTo("sk_"..type.."_debuff", target, target.components.debuffable and target.components.debuffable.followsymbol or nil, target.components.debuffable and target.components.debuffable.followoffset or {x = 0, y= 0, z = 0})
				if not target.has_debuff then
					target.has_debuff = {}
				end
				target.has_debuff[debuff.prefab] = true
				debuff.duration = debuff.duration * (strength or 1)
			end	
		end
	end
end

PlayablePets.SetFamily = function(inst, family)
	local fam = family or nil
	if fam then
		if fam ~= SK_FAMILIES.UNKNOWN then
			inst.sk_family = fam.family
			if not inst:HasTag("player") then
				inst:AddTag(fam.tag)
			end
		end	
		inst.normal_stat = fam.normal_stat
		inst.shadow_stat = fam.shadow_stat
		inst.piercing_stat = fam.piercing_stat
		inst.elemental_stat = fam.elemental_stat
		
		inst.stun_resist = fam.stun_resist or 1
		inst.fire_resist = fam.fire_resist or 1
		inst.shock_resist = fam.shock_resist or 1
		inst.poison_resist = fam.poison_resist or 1
		inst.freeze_resist = fam.freeze_resist or 1
		inst.curse_resist = fam.curse_resist or 1
		inst.sleep_resist = fam.sleep_resist or 1
		
	else
		print("ERROR: Invalid family called")
	end
end

--This shit is disgusting, clean this shit up man!
PlayablePets.SetDamageResistances = function (inst, normal, piercing, elemental, shadow)
	--Will most likely be unused but putting this here anyways. damage * x
	if normal then
		inst.normal_stat = normal
	end
	
	if piercing then
		inst.piercing_stat = piercing
	end
	
	if elemental then
		inst.elemental_stat = elemental
	end
	
	if shadow then
		inst.shadow_stat = shadow
	end
end

PlayablePets.SetCommonStatusResistances = function (inst, stun, fire, shock, poison, freeze, curse, sleep)
	--These just reduce the timer on the status effects. Ofc 0 is full immunity. time * x_resist
	if stun then
		inst.stun_resist = stun
	end
	
	if fire then
		inst.fire_resist = fire
	end
	
	if shock then
		inst.shock_resist = shock
	end
	
	if poison then
		inst.poison_resist = poison
	end
	
	if freeze then
		inst.freeze_resist = freeze
	end
	
	if curse then
		inst.curse_resist = curse
	end
	
	if sleep then
		inst.sleep_resist = sleep
	end
end

local function ZombieCapped(inst)
	if TheNet:GetServerGameMode() == "lavaarena" then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, 50, {"zombie"}, {"player"})
		return #ents >= 15
	else
		return false
	end
end

--For Zombies
PlayablePets.ZombieOnKill = function(inst, data)
	if data.victim and data.victim:IsValid() and not ZombieCapped(inst) and not data.victim.isinfected and not data.victim:HasTag("undead") and (data.victim:HasTag("character") or data.victim:HasTag("player") or (data.victim:HasTag("LA_mob") and not data.victim:HasTag("structure"))) then
		data.victim.isinfected = true
		inst:DoTaskInTime(0.5 * math.random(1,3), function(inst)
			local pos = data.victim:GetPosition()
			print(inst.variant)
			if math.random(1, 100) <= 10 and inst.variant ~=  "_curse" then
				local zombie = SpawnPrefab(inst.variant and "sk_bombie"..inst.variant or inst.prefab)
				zombie.Transform:SetPosition(pos.x, 0, pos.z)
				zombie.sg:GoToState("spawn")
				if data.victim:HasTag("player") then
					zombie.components.named:SetName(data.victim:GetDisplayName())
				end
				if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") then
					zombie:AddTag("companion")
					zombie.components.lootdropper:SetLoot(nil)
				end
			else
				local zombie = SpawnPrefab(inst.variant and "sk_zombie"..inst.variant or inst.prefab)
				zombie.Transform:SetPosition(pos.x, 0, pos.z)
				zombie.sg:GoToState("spawn")
				if data.victim:HasTag("player") then
					zombie.components.named:SetName(data.victim:GetDisplayName())
				end
				if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") then
					zombie:AddTag("companion")
					zombie.components.lootdropper:SetLoot(nil)
				end
			end	
			
		end)
	end
	--elseif data.victim and data.victim:IsValid() and (data.victim:HasTag("character") or data.victim:HasTag("player")) and (data.victim:HasTag("largecreature") or data.victim:HasTag("giant")) then
		--Deadnaughts will be spawned here
end
------------------------------------------------------------------
-- RPC Keyhandler functions
------------------------------------------------------------------

--Checks to see if there is any type-able pop ups. Prevents RPCs from being activated.
local function IsDefaultScreen()
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name and type(TheFrontEnd:GetActiveScreen().name) == "string" and TheFrontEnd:GetActiveScreen().name == "HUD" then
		return true
	else
		return false
	end
end

PlayablePets.PurchasedItem = function(player, prefab, delta)
	if player and prefab and delta then
		player.components.ppcurrency:DoDelta(delta)
		local item = SpawnPrefab(prefab)
		player.components.inventory:GiveItem(item)
	end
end

PlayablePets.BattleMenuSelect = function(player, stage, target, skill)
	print("BattleMenuSelect running")
	if player then
		player:PushEvent("pm_doaction", {stage = stage, targets = {target}, skill = skill})
	end
end

--TODO? Make it a table of functions and have a mob just manually set what they which they want to do?

-- Allows a player mob to sleep. Does what it says on the tin
PlayablePets.SetSleep = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("Z ability pressed by "..player:GetDisplayName())
	-----legacy---------
	if player.specialsleep ~= nil and player.specialsleep == true and not player.sg:HasStateTag("busy") then
		player.sg:GoToState("special_sleep")
	elseif player.specialsleep ~= nil and player.specialsleep == false and (not player.sg:HasStateTag("busy") or player.sg:HasStateTag("caninterrupt")) then
		player.sg:GoToState("special_wake")
	end	
	
	if player.wants_to_sleep ~= nil and player.wants_to_sleep == false and player.sg:HasStateTag("attacking") and not player.sg:HasStateTag("nointerrupt") then
		player.wants_to_sleep = true --This allows tentacle players to stop attacking when they want to.
	end
	
	if player.mobsleep ~= nil and player.mobsleep == true and player.sg:HasStateTag("sleeping") and not player._forcedsleep then
		player.sg:GoToState("wake")	
	elseif player.mobsleep ~= nil and player.mobsleep == true and not player.sg:HasStateTag("busy") and not player.sg:HasStateTag("sleeping") and not player.sg:HasStateTag("specialsleep") then
		player.sg:GoToState("sleep")	
	end
	
	if player.sg:HasStateTag("specialsleep") and not player.sg:HasStateTag("busy") then
		player.sg:GoToState("wake")
	end

	--PP Component----
	if player.components.playablepet then
		player.components.playablepet:TryAbility(PP_ABILITY_SLOTS.SLEEP, cursor_pos)
	end
end

-- Performs a special abillity, usually a taunt, when the "x" key is pressed.
PlayablePets.SpecialAbility1 = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("X ability pressed by "..player:GetDisplayName())
	--Legacy--
	if not player.sg:HasStateTag("busy") then
		if player.taunt or player.israged_special or (player:HasTag("special_atk1") and not player.sg:HasStateTag("sleeping") ) then
			player.sg:GoToState("special_atk1")
		end
		
		if player.taunt_multistate and not player.sg:HasStateTag("special_toggle") then
			player.sg:GoToState("special_atk1")		
		end
		
		if player.isdragonrage ~= nil then
			if player.isdragonrage == true then
				player.sg:GoToState("transform_normal")
			else
				player.sg:GoToState("transform_fire")
			end
		end
	else
		if player.taunt_multistate and player.sg:HasStateTag("special_toggle") then
			player.sg:GoToState("special_atk1_pst")			
		end
	end
	--PP Component----
	if player.components.playablepet then
		player.components.playablepet:TryAbility(PP_ABILITY_SLOTS.SKILL1, cursor_pos)
	end
end

-- Performs a secondary special abillity when "c" is pressed.
PlayablePets.SpecialAbility2 = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("C ability pressed by "..player:GetDisplayName())
	if not player.sg:HasStateTag("busy") and not player.canhide then
		if player._isflying ~= nil and player._isflying == true and not player.sg:HasStateTag("busy") then
			player.sg:GoToState("land")
		end
			
		if player:HasTag("special_atk2") and not player.sg:HasStateTag("sleeping") then
			player.sg:GoToState("special_atk2")
		end
						
		if player.taunt2 and player.taunt2 ~= false and not player._isflying then
			player.sg:GoToState("special_atk2")
		end
			
		if player.specialatk2 and player.specialatk2 ~= false and not player._isflying then --shouldn't be called but just incase
			player.sg:GoToState("special_atk2")
		end
			
		if player.israged ~= nil then
			player.sg:GoToState("rage")
		end
		
		if player._isunder then
			player.sg:GoToState("special_exit")
		end

		if player.taunt2 and player.taunt2_exit then
			player.sg:GoToState(player.taunt2_exit)
		end
			
	elseif player.canhide ~= nil then
		--print("Hide check passed")
		if player.sg:HasStateTag("hiding") then
			player.sg:GoToState("hide_pst")
		elseif player.canhide == true and not player.sg:HasStateTag("busy") then
			player.sg:GoToState("hide_pre")
		end
	end

	--PP Component----
	if player.components.playablepet then
		player.components.playablepet:TryAbility(PP_ABILITY_SLOTS.SKILL2, cursor_pos)
	end
end

PlayablePets.SpecialAbility3 = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("V ability pressed by "..player:GetDisplayName())
	if not player.sg:HasStateTag("busy") then
		if player.taunt3 and player.taunt3 ~= false and not (player.noactions or player._isflying) then
			player.sg:GoToState("special_atk3")
		end
		if player.taunt3 and player.taunt3_exit then
			player.sg:GoToState(player.taunt3_exit)
		end
	end
	--PP Component----
	if player.components.playablepet then
		player.components.playablepet:TryAbility(PP_ABILITY_SLOTS.SKILL3, cursor_pos)
	end
end

PlayablePets.SpecialAbility4 = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("B ability pressed by "..player:GetDisplayName())
	if not player.sg:HasStateTag("busy") then
		if player.taunt4 and player.taunt4 ~= false and not (player.noactions or player._isflying) then
			player.sg:GoToState("special_atk4")
		end
	end
	--PP Component----
	if player.components.playablepet then
		player.components.playablepet:TryAbility(PP_ABILITY_SLOTS.SKILL4, cursor_pos)
	end
end

-- Sends the player to the character select screen when "P" is pressed
PlayablePets.ChangeCharacter = function(player, x, y, z)
	local cursor_pos = Point(x, y, z)
	d_print("P ability pressed by "..player:GetDisplayName())
	--If you're an imposter, remove disguse regardless of settings.
	if player and not player.noactions and not player.sg:HasStateTag("busy") and player.components.seamlessplayerswapper and player.components.seamlessplayerswapper.main_data.is_mimic then
		PlayablePets.UndoMimicry(player)
	elseif MOBCHANGE == PP_SETTING.ENABLE and TheNet:GetServerGameMode() ~= "lavaarena" then
		if not player.sg:HasStateTag("busy") and not player.sg:HasStateTag("home") and not player.isdespawning == true then
			player.isdespawning = true
			player:PushEvent("pp_despawn", {method = "key"})
			player.components.inventory:DropEverything(true)
			local portal = FindEntity(player, 9001, nil, {"multiplayer_portal"}) or nil
			if portal and portal._savedata then
				portal._savedata[player.userid] = player:SaveForReroll()
			end
			--TODO this is a bandaid fix for players losing money when despawning.
			--Look into portal save data to see if its possible to save currency
			--there and if it can be reapplied to player onspawn.
			if player.components.ppcurrency and player.components.ppcurrency:GetCurrent() > 0 then
				--[[
				local bag = SpawnPrefab("wadebag")
				bag:SetValue(player.components.ppcurrency:GetCurrent())
				bag.Transform:SetPosition(player:GetPosition():Get())		
				bag.components.named:SetName(player:GetDisplayName().."'s Stash of Oincs")		]]
				TheWorld.components.ppbank_manager:Deposit(player)
			end
			player:DoTaskInTime(0.1, function(inst) 
				inst.isdespawning = true 
				TheWorld:PushEvent("ms_playerdespawnanddelete", inst) 
			end)
		end
	end
end
------------------------------------------------------------------
-- PLAYABLE PETS INIT FUNCTION (MAKES STUFF GO)
------------------------------------------------------------------

PlayablePets.Init = function(modName)
	if not PP_SETTINGS.MODNAMES[modName] then
		PP_SETTING.MODNAMES[modName] = modName
	end
end

PlayablePets.SetUpRPCs= function(modname)
	for fn, keycode in pairs(RPCKEYS) do
		AddModRPCHandler(modname, fn, PlayablePets[fn])
		
		TheInput:AddKeyDownHandler(keycode, function()
			if ThePlayer and not ThePlayer.HUD:IsChatInputScreenOpen() and not ThePlayer.HUD:IsConsoleScreenOpen() and not ThePlayer.HUD:IsCraftingOpen() and IsDefaultScreen() then
				local pos = TheInput:GetWorldPosition()
				SendModRPCToServer(GetModRPC(modname, fn), pos.x, 0, pos.z)
			end
		end)
	end
end

------------------------------------------------------------------
-- Fun Stuff
------------------------------------------------------------------
local spirit_teams = {
	{94/255, 1, 1},
	{1, 183/255, 0},
	{1, 61/255, 61/255},
	{1, 1, 1}
}
PlayablePets.Spirify = function(inst, team)
	inst.AnimState:SetSaturation(0)
	local curfaction = team or 1
	local curteam = spirit_teams[curfaction]
	inst.AnimState:SetMultColour(curteam[1], curteam[2], curteam[3], 0.8)
	inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
	inst.AnimState:SetLightOverride(1)	
	inst.AnimState:SetAddColour(curteam[1]/7, curteam[2]/7, curteam[3]/7, 1)
end

PlayablePets.Unspirify = function(inst, team)
	inst.AnimState:SetSaturation(1)
	local curfaction = team or 1
	local curteam = spirit_teams[curfaction]
	inst.AnimState:SetMultColour(1, 1, 1, 1)
	inst.AnimState:SetBloomEffectHandle(nil)
	inst.AnimState:SetLightOverride(0)	
	inst.AnimState:SetAddColour(0, 0, 0, 1)
end

local function Discofy2(target, remove)
	if remove then
		if target.disco_task then
			target.disco_task:Cancel()
			target.disco_task = nil
			target.AnimState:SetMultColour(1, 1, 1, 1)
			target.AnimState:SetLightOverride(0)
		end
	else
		target.disco_task = target:DoPeriodicTask(0.1, function(inst)
			inst.AnimState:SetMultColour(math.random(), math.random(), math.random(), 1)
			inst.AnimState:SetLightOverride(1)
		end)
	end
end

PlayablePets.Discofy = function(inst, aoe)
	if aoe then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, aoe)
		for i, v in ipairs(ents) do
			if v:IsValid() and v.AnimState then
				Discofy2(v)
			end
		end
	else
		Discofy2(inst)
	end
end

PlayablePets.UnDiscofy = function(inst, aoe)
	if aoe then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, aoe)
		for i, v in ipairs(ents) do
			if v:IsValid() and v.disco_task then
				Discofy2(v, true)
			end
		end
	else
		Discofy2(inst, true)
	end
end

PlayablePets.PaperFlip = function(inst)
	local rot = inst.Transform:GetRotation()
	rot = rot + 180
	if rot > 360 then
		rot = rot - 360
	end
	inst.Transform:SetRotation(rot)
	inst.AnimState:SetScale(-1, 1)
	--TODO make component for this
	if inst.papertask then
		inst.papertask:Cancel()
		inst.papertask = nil
		inst.curscale = nil
	end
	inst.papertask = inst:DoPeriodicTask(0, function(inst)
		if not inst.curscale then
			inst.curscale = -1
		end
		local tick = 2/20
		if inst.curscale > 2 then
			return
		end
		if inst.curscale < 1 then
			inst.curscale = inst.curscale + tick
			inst.AnimState:SetScale(inst.curscale, 1)
		elseif inst.curscale >= 1 and inst.curscale < 10 then
			inst.curscale = 10
			inst.AnimState:SetScale(1, 1)
		end
	end)
end

PlayablePets.Convert = function(inst)
	inst.components.health:SetMaxHealth(9999)
	inst:AddTag("cultist_elder")
	inst.components.locomotor.walkspeed = 4
	inst.shouldwalk = true
	inst.mobsleep = nil
	inst.taunt = true
	inst.taunt2 = false
	inst.taunt3 = false
	inst.specialsleep = true
	inst.AnimState:SetBank("elder_cultist_l")
	inst.AnimState:SetBuild("elder_cultist_l")
	inst:SetStateGraph("SGelder_cultistp")
	inst:AddTag("noplayerindicator")
	inst.DynamicShadow:Enable(false)
	inst.MiniMapEntity:SetIcon("")
	inst.Transform:SetTwoFaced()
	inst.AnimState:SetScale(1.75, 1.75)
	RemovePhysicsColliders(inst)
end

PlayablePets.MakeGhost = function(inst)
	inst.SoundEmitter:SetMute(true)
	inst:AddTag("noplayerindicator")
	inst.MiniMapEntity:SetIcon("")
	inst.components.health:SetInvincible(true)
	inst.AnimState:SetMultColuor(0.5, 0.5, 1, 0.5)
	inst.DynamicShadow:Enable(false)
	inst:AddTag("NOCLICK")
	inst:AddTag("notarget")
	inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
	RemovePhysicsColliders(inst)
	inst.components.talker:IgnoreAll("ghost")
end

PlayablePets.MakeRainbow = function(inst, remove)
	if remove then
		if inst._rainbow_task then
			inst._rainbow_task:Cancel()
			inst._rainbow_task = nil
		end
		inst._rainbow_hue = 0
		inst.AnimState:SetHue(1)
	else
		inst._rainbow_hue = 0
		if inst._rainbow_task then
			inst._rainbow_task:Cancel()
			inst._rainbow_task = nil
		end
		inst._rainbow_task = inst:DoPeriodicTask(0, function(inst)
			inst._rainbow_hue = inst._rainbow_hue + 0.01
			if inst._rainbow_hue == 1 then
				inst._rainbow_hue = 0
			end
			inst.AnimState:SetHue(inst._rainbow_hue)
		end)
	end
end

PlayablePets.FixWalls = function(inst, amount)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, 10, {"wall"})
	for i, v in ipairs(ents) do
		if v.components.health and v.components.health:GetPercent() < (amount or 0.5) then
			v.components.health:SetPercent(amount or 0.5)
			if not amount or amount > 0 then
				v.Physics:SetActive(true)
				v._ispathfinding:set(true)
			end
		end
	end
end
local boat_colors = {
	red = {1, 0.5, 0.5},
	blue = {0.5,0.5,1}
}
PlayablePets.Boat = function(color, hue)
	local boat = c_spawn("boat")
	local pos = boat:GetPosition()
	boat.installations = {}

	local wheel = SpawnPrefab("steeringwheel")
	wheel.Transform:SetPosition(pos.x + 3, pos.y, pos.z)
	table.insert(boat.installations, wheel)

	local rudder = SpawnPrefab("boat_rotator")
	rudder.Transform:SetPosition(pos.x + 2.8, pos.y, pos.z + 2)
	table.insert(boat.installations, rudder)

	local anchor = SpawnPrefab("anchor")
	anchor.Transform:SetPosition(pos.x - 3, pos.y, pos.z)
	table.insert(boat.installations, anchor)

	local mast1 = SpawnPrefab("mast")
	mast1.Transform:SetPosition(pos.x + 1, pos.y, pos.z)
	table.insert(boat.installations, mast1)

	local mast2 = SpawnPrefab("mast")
	mast2.Transform:SetPosition(pos.x - 1, pos.y, pos.z)
	table.insert(boat.installations, mast2)

	local cannon1 = SpawnPrefab("boat_cannon")
	cannon1.Transform:SetPosition(pos.x + 1, pos.y, pos.z - 3)
	cannon1.Transform:SetRotation(90)
	table.insert(boat.installations, cannon1)
	
	local cannon2 = SpawnPrefab("boat_cannon")
	cannon2.Transform:SetPosition(pos.x + 1, pos.y, pos.z + 3)
	cannon2.Transform:SetRotation(270)
	table.insert(boat.installations, cannon2)

	local light1 = SpawnPrefab("lantern_boat")
	light1.Transform:SetPosition(pos.x + 2.6, pos.y, pos.z + 2.6)
	table.insert(boat.installations, light1)

	local light2 = SpawnPrefab("lantern_boat")
	light2.Transform:SetPosition(pos.x - 2.6, pos.y, pos.z + 2.6)
	table.insert(boat.installations, light2)

	local light3 = SpawnPrefab("lantern_boat")
	light3.Transform:SetPosition(pos.x + 2.6, pos.y, pos.z - 2.6)
	table.insert(boat.installations, light3)

	local light4 = SpawnPrefab("lantern_boat")
	light4.Transform:SetPosition(pos.x - 2.6, pos.y, pos.z - 2.6)
	table.insert(boat.installations, light4)

	local barrel = SpawnPrefab("barrel_l")
	local ammo_count = 60
	local patch_count = 20
	local oar_count = 2
	local hat_count = 2
	local amulet_count = 3
	table.insert(boat.installations, barrel)
	barrel.Transform:SetPosition(pos.x - 1, pos.y, pos.z - 1)
	if barrel and barrel.components.container then
		for i = 1, ammo_count do
			barrel.components.container:GiveItem(SpawnPrefab("cannonball_rock_item"))
		end
		for i = 1, patch_count do
			barrel.components.container:GiveItem(SpawnPrefab("boatpatch"))
		end
		for i = 1, oar_count do
			barrel.components.container:GiveItem(SpawnPrefab("oar"))
		end
		for i = 1, hat_count do
			barrel.components.container:GiveItem(SpawnPrefab("monkey_smallhat"))
		end
		barrel.components.container:GiveItem(SpawnPrefab("monkey_mediumhat"))
		for i = 1, amulet_count do
			barrel.components.container:GiveItem(SpawnPrefab("amulet"))
		end
	end
	if color and boat_colors[color] then
		local bcolor = boat_colors[color]
		boat.AnimState:SetMultColour(bcolor[1], bcolor[2], bcolor[3], 1)
		for i, v in ipairs(boat.installations) do
			v.AnimState:SetMultColour(bcolor[1], bcolor[2], bcolor[3], 1)
		end
	end
	if hue then
		boat.AnimState:SetHue(hue)
		for i, v in ipairs(boat.installations) do
			v.AnimState:SetHue(hue)
		end
	end
end

PlayablePets.HeartAttack = function(target)
	if target and not target.heartattack_task then
		target.heartattack_task = target:DoPeriodicTask(5, function(inst)
			if inst.components.health and not inst.components.health:IsDead() and math.random() < 0.05 then
				inst.components.health:DoDelta(-inst.components.health.maxhealth*3, nil, "pp_heart_attack", true, nil, true)
			end
		end)
	end
end

return PlayablePets