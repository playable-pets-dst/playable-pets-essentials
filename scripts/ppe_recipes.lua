local antidoterrecipe = AddRecipe("antidotep", {Ingredient("venomglandp", 1, "images/pp_essentials_icons.xml"),Ingredient("kelp", 3), Ingredient("saltrock", 2)}, GLOBAL.RECIPETABS.SURVIVAL, GLOBAL.TECH.SCIENCE_TWO, nil, nil, nil, nil, nil, "images/pp_essentials_icons.xml")
local midas_statuep = AddRecipe2(
		"cotl_midas_statuep", 
        {Ingredient("goldnugget", 5), Ingredient("meat", 1)}, --ingredients
        GLOBAL.TECH.MAGIC_TWO, --tech tier
        {
            atlas = "images/pp_essentials_icons.xml", 
            image = "cotl_midas_statuep.tex",
            builder_tag = nil,
			placer = "cotl_midas_statuep_placer",
			min_spacing = 0.1,
			--nounlock = true,
        },{"STRUCTURES"} 
    )

local cotl_goldpile_smallp = AddRecipe2(
		"cotl_goldpile_smallp", 
        {Ingredient("goldnugget", 20)}, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/pp_essentials_icons.xml", 
            image = "cotl_goldpile_smallp.tex",
            builder_tag = nil,
			placer = "cotl_goldpile_smallp_placer",
			min_spacing = 0.5,
			--nounlock = true,
        },{"STRUCTURES"} 
    )

local cotl_goldpile_largep = AddRecipe2(
		"cotl_goldpile_largep", 
        {Ingredient("goldnugget", 100)}, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/pp_essentials_icons.xml", 
            image = "cotl_goldpile_largep.tex",
            builder_tag = nil,
			placer = "cotl_goldpile_largep_placer",
			min_spacing = 0.5,
			--nounlock = true,
        },{"STRUCTURES"} 
    )

local godstatues = {"leshy", "heket", "kallamar", "shamura"}
for i, v in ipairs(godstatues) do
    local godstatuep = AddRecipe2(
		"cotl_godstatue_"..v.."p", 
        {Ingredient("cutstone", 10), Ingredient("nightmarefuel", 20)}, --ingredients
        GLOBAL.TECH.MAGIC_TWO, --tech tier
        {
            atlas = "images/pp_essentials_icons.xml", 
            image = "cotl_godstatue_"..v.."p.tex",
            builder_tag = "cotl_oldfaith",
			placer = "cotl_godstatue_"..v.."p_placer",
			min_spacing = 0.5,
			--nounlock = true,
        },{"STRUCTURES"} 
    )
end