-----------------------------------------------
--					Combat
-----------------------------------------------
local d_print = PlayablePets.DebugPrint
local combat = require "components/combat"

--experiment for a different mod

combat._combat_resistances = {slash = 1, pierce = 1, blunt = 1}

function combat:SetCombatResistances(data)
	self._combat_resistances = {}
	for k, v in pairs(data) do
		self._combat_resistances[k] = v
	end
end

function combat:AddResistance(resist, value)
	if not self._combat_resistances then
		self._combat_resistances = {}
	end
	self._combat_resistances[resist] = value
end

function combat:GetResistances()
	if not self._combat_resistances then
		self._combat_resistances = {}
	end
	return self._combat_resistances
end

function combat:DoAdvancedAttack(target, data, overridemult)
	if not (target and target:IsValid() and target.components.combat and target.components.health) then
		return
	end
	local ispvp = target:HasTag("player")
	local data = data or {}
	local damage = 0
	local resistances = target.components.combat:GetResistances()
	if data.stats then
		for k, v in pairs(data.stats) do
			local res = (resistances and resistances[k]) and resistances[k] or 1
			damage = damage + (v*res)
			d_print("---------------------")
			d_print(k)
			d_print("---------------------")
			d_print("attack stat: "..tostring(v))
			d_print("defense stat: "..tostring(res))
		end
	else
		d_print("no stat data, using defaultdamage")
		damage = self.defaultdamage
	end
	d_print("total base damage: "..tostring(damage))
	if ispvp and self.pvp_damagemod then
		damage = damage * self.pvp_damagemod
		d_print("damage after pvp check: "..tostring(damage))
	end
	damage = damage * (overridemult or 1) * (1*self.externaldamagemultipliers:Get())
	d_print("damage after modifiers: "..tostring(damage))
	if data.iscrit then
		damage = damage * 3
		d_print("damage after crit: "..tostring(damage))
	end
	if data.poise and target.components.poise and target.components.poise.current > 0 then
		target.components.poise:DoPoiseDelta(data.poise, {attacker = self.inst})
	end
	d_print("final damage to target: "..tostring(damage))
	target.components.combat:GetAttacked(self.inst, damage, nil, data.stimuli)
end
---------------------------------------------------------------------

--stops allies from autotargeting each other.
--TODO This is shit and I should be ashamed of myself, even if this was written back in 2016.
local comb_rep = require "components/combat_replica"
local old_IsAlly = comb_rep.IsAlly
function comb_rep:IsAlly(guy,...)
	if self.inst.prefab == "beequeenp" and guy.prefab == "beeguard" then
		return true
	elseif self.inst:HasTag("spiderwhisperer") and self.inst.prefab ~= "webber" and guy:HasTag("spider") and not guy:HasTag("player") then
		return true
	elseif self.inst.prefab == "antlionp" and guy:HasTag("groundspike")	then
		return true
	elseif self.inst:HasTag("dragonfly") and inst.enraged ~= nil and inst.enraged == true and guy:HasTag("lavae") then
		return true	
	elseif self.inst:HasTag("stalker") and guy.stalker_minion ~= nil then
		return true
	elseif self.inst.prefab == "boarriorp" and guy:HasTag("boarrior_pet") then
		return true
	elseif self.inst:HasTag("LA_mob") and guy:HasTag("battlestandard") then
		return true
	elseif self.inst:HasTag("pugalisk") and guy.host and guy.host == self.inst then
		return true	
	elseif self.inst:HasTag("ant") and guy:HasTag("ant") then
		return true
	elseif self.inst:HasTag("ancient_robot") and guy:HasTag("ancient_robot") then
		return true
	elseif self.inst.prefab == "heraldp" and guy:HasTag("herald_minion") and not (guy.components.follower and guy.components.follower.leader == self.inst) then
		return true
	elseif self.inst.prefab == "rocp" and self.inst.body and guy.body == self.inst.body then
		return true	
	end
	return old_IsAlly(self,guy,...)
end