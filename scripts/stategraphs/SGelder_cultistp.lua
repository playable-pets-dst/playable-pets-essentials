require("stategraphs/commonstates")
require("stategraphs/ppstates")
--this is also varglet's sg.
local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "walk_pst"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetInvisible(inst)
	inst.components.health:SetInvincible(true)
	inst:AddTag("notarget")
	inst.components.locomotor:SetExternalSpeedMultiplier(inst, 3, 3)
	inst.components.talker:IgnoreAll("hiding")
	inst:Hide()
end

local function SetVisible(inst)
	inst.components.health:SetInvincible(false)
	inst:RemoveTag("notarget")
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 3)
	inst.components.talker:StopIgnoringAll("hiding")
	inst:Show()
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{
   State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "special_atk1",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
            local posx, posy, posz = inst.Transform:GetWorldPosition()
            local angle = -inst.Transform:GetRotation() * DEGREES
            local offset = 4.25
            local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
            local dagger = SpawnPrefab("cultist_dagger_l")
            dagger.Transform:SetPosition(targetpos:Get())
            dagger:OnSpawn()
        end,

    },

	State{
        name = "special_sleep",
        tags = { "busy", "howling" },

        onenter = function(inst, count)
            inst.Physics:Stop()
            if inst.noactions then
                inst.noactions = nil
                SetVisible(inst)
                inst.AnimState:PlayAnimation("appear")
            else
                inst.AnimState:PlayAnimation("disappear")
                inst.noactions = true
            end
            inst.SoundEmitter:PlaySound("dontstarve/creatures/krampus/bag_jumpinto")
        end,

        timeline =
        {

        },

        onexit = function(inst)
            
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.noactions then
                    SetInvisible(inst)
                end
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/krampus/bag_jumpinto")
            inst.AnimState:PlayAnimation("disappear")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					if MOBGHOST== "Enable" then
                     inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
					else
						TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
					end
                end
            end),
        },

    },
}

CommonStates.AddWalkStates(states,
{
	walktimeline = {

	},
})
PP_CommonStates.AddKnockbackState(states, nil, "walk_pst") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
		},
	
	},
	--anims = 
	{
		corpse = "disappear",
		corpse_taunt = "appear"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddHomeState(states, nil, "walk_pst", "idle_loop", true)
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_pst",
	
	steer_pre = "walk_pst",
	steer_idle = "idle_loop",
	steer_turning = "walk_pst",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "jump",
	leap_loop = "jump_loop",
	leap_pst = "jump_pst",
	
	lunge_pre = "atk_pre",
	lunge_loop = "atk",
	lunge_pst = "atk_pst",
	
	superjump_pre = "atk_pre",
	superjump_loop = "atk",
	superjump_pst = "atk_pst",
	
	castspelltime = 10,
})
	
return StateGraph("elder_cultistp", states, events, "idle", actionhandlers)

