require("stategraphs/commonstates")

PP_CommonStates = {}
PP_CommonHandlers = {}

--//////////////////////////////////////////////////
--					EventHandlers		
--//////////////////////////////////////////////////
--==================================================
--                   QoL
--==================================================
local function itemranout(inst, data)
	if inst.components.inventory:GetEquippedItem(data.equipslot) == nil then
        local sameTool = inst.components.inventory:FindItem(function(item)
			return item.prefab == data.prefab and item.components.equippable ~= nil and
                   item.components.equippable.equipslot == data.equipslot
             end)
        if sameTool ~= nil then
            inst.components.inventory:Equip(sameTool)
        end
    end
end

local function CanHitStun(inst)
	return inst.hit_recovery and ((inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime()) or true
end
--==================================================
--					Attacked
--==================================================
local function onattacked(inst, data)
	if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
		inst.sg:GoToState("hit")
	elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
		inst.sg:GoToState("hit", data.stimuli)
	elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") and CanHitStun(inst) then 
		inst.sg:GoToState("hit") 
	end 
end

PP_CommonHandlers.OnAttacked = function()
    return EventHandler("attacked", onattacked)
end

--==================================================
--					Eaten
--==================================================
local function oneaten(inst, data)
	inst.sg:GoToState("death_instant", "vore")
end
--added in AddCommonHandlers
--==================================================
--					OpenGift
--==================================================
local function opengift(inst)
    if not inst.sg:HasStateTag("busy") then
        inst.sg:GoToState("opengift")
    end
end

PP_CommonHandlers.OpenGift = function()
    return EventHandler("ms_opengift", opengift)
end

--==================================================
--					Locomote
--==================================================
	local function SetSleeperAwakeState(inst, forcetaunt)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
        inst:OnWakeUp()
        inst.components.inventory:Show()
        inst:ShowActions(true)	
        --inst.sg:GoToState(forcetaunt and "special_atk1" or "idle")
	end
	
	local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
	
	if inst.components.shedder ~= nil then
	inst.components.shedder:StopShedding()
	end
	
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
	end

	local function onlocomote_advanced(inst, data)
        if inst.sg:HasStateTag("busy") or inst.cannotmove then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                if inst.sleepingbag.components.sleepingbag then
					inst.sleepingbag.components.sleepingbag:DoWakeUp()
				elseif inst.sleepingbag.components.multisleepingbag then
					inst.sleepingbag.components.multisleepingbag:DoWakeUp(inst)
				end
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
		elseif is_moving and not should_move then
			if inst.components.sailor and inst.components.sailor.boat then --IA
				inst.sg:GoToState("sailing_stop")
			else
				if inst.shouldwalk and inst.shouldwalk == true then
					inst.sg:GoToState("walk_stop")
				else
					inst.sg:GoToState("run_stop")
				end
			end	
        elseif not is_moving and should_move then
			if inst.components.sailor and inst.components.sailor.boat then --IA
				inst.sg:GoToState("sailing")
			else
				if inst.shouldwalk and inst.shouldwalk == true then
					inst.sg:GoToState("walk_start")
				else
					inst.sg:GoToState("run_start")
				end
			end	
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
	end	
	
PP_CommonHandlers.OnLocomoteAdvanced = function()
    return EventHandler("locomote", onlocomote_advanced)
end	
--==================================================
--						Sink
--==================================================

local function DoVoidFall(inst, skip_vfx)
    if not skip_vfx then
        local x, y, z = inst.Transform:GetWorldPosition()
        SpawnPrefab("fallingswish_clouds").Transform:SetPosition(x, y, z)
        SpawnPrefab("fallingswish_lines").Transform:SetPosition(x, y, z)
    end
    inst:Hide()
end

local function onsink(inst)
	if not inst.components.health:IsDead() and not inst:HasTag("flying") and not inst.amphibious and not inst:HasTag("aquatic") then
        inst.sg:GoToState("sink_fast")
    end
end

local function onfallinvoid(inst, data)
    if not inst.components.health:IsDead() and not inst:HasTag("flying") then
        inst.sg:GoToState("abyss_fall", data)
    end
end
	
PP_CommonHandlers.OnSink = function()
    return EventHandler("onsink", onsink)
end	
--==================================================
--					Death/Corpse
--==================================================

	local function ondeath(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death", data)
        end
    end
	
PP_CommonHandlers.OnDeath = function()
    return EventHandler("death", ondeath)
end		

--==================================================
--					RespawnGhost
--==================================================

	local function onrespawn(inst, data)
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end

        inst.components.health:SetInvincible(false)
        inst:ShowHUD(true)
        inst:SetCameraDistance()

        SerializeUserSession(inst) 
	end
	
PP_CommonHandlers.OnRespawn = function()
    return EventHandler("respawnfromghost", onrespawn)
end	

--==================================================
--					Knockback
--==================================================
local function onknockback(inst, data)
	local armor = inst.components.inventory and inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
    if not inst.components.health:IsDead() and not (armor and armor:HasTag("heavyarmor")) and not inst.sg:HasStateTag("nointerrupt") and not inst:HasTag("epic") and not inst.knockbackimmune and not inst.sg:HasStateTag("parrying") then
		inst.sg:GoToState("knockback", data)
	end
end

PP_CommonHandlers.OnKnockback = function()
    return EventHandler("knockback", onknockback)
end

--//////////////////////////////////////////////////
--						States		
--//////////////////////////////////////////////////
local function idleonanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("idle")
    end
end

local function get_loco_anim(inst, override, default)
    return (override == nil and default)
        or (type(override) ~= "function" and override)
        or override(inst)
end


local function runonanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("run")
    end
end

local function runontimeout(inst)
    inst.sg:GoToState("run")
end

PP_CommonStates.AddRunStates = function(states, timelines, anims, softstop, delaystart, fns)
    table.insert(states, State{
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			if fns ~= nil and fns.startonenter ~= nil then
				fns.startonenter(inst)
			end
			if not delaystart then
	            inst.components.locomotor:RunForward()
			end
            inst.AnimState:PlayAnimation(get_loco_anim(inst, anims ~= nil and anims.startrun or nil, "run_pre"))
			if fns ~= nil and fns.startonenter ~= nil then
				fns.startonenter(inst)
			end
        end,

        timeline = timelines ~= nil and timelines.starttimeline or nil,

		onupdate = fns ~= nil and fns.startonupdate or nil,

		onexit = fns ~= nil and fns.startonexit or nil,

        events =
        {
            EventHandler("animover", runonanimover),
        },
    })

    table.insert(states, State{
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			if fns ~= nil and fns.runonenter ~= nil then
				fns.runonenter(inst)
			end
            inst.components.locomotor:RunForward()
            local anim_to_play = get_loco_anim(inst, anims ~= nil and anims.run or nil, "run_loop")
            inst.AnimState:PlayAnimation(anim_to_play, true)
        end,

        timeline = timelines ~= nil and timelines.runtimeline or nil,

		onupdate = fns ~= nil and fns.runonupdate or nil,

		onexit = fns ~= nil and fns.runonexit or nil,

        ontimeout = runontimeout,

        events = {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end),
        }
    })

    table.insert(states, State{
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst)
			if fns ~= nil and fns.endonenter ~= nil then
				fns.endonenter(inst)
			end
            inst.components.locomotor:StopMoving()
            if softstop == true or (type(softstop) == "function" and softstop(inst)) then
                inst.AnimState:PushAnimation(get_loco_anim(inst, anims ~= nil and anims.stoprun or nil, "run_pst"), false)
            else
                inst.AnimState:PlayAnimation(get_loco_anim(inst, anims ~= nil and anims.stoprun or nil, "run_pst"))
            end
			if fns ~= nil and fns.endonenter ~= nil then
				fns.endonenter(inst)
			end
        end,

        timeline = timelines ~= nil and timelines.endtimeline or nil,

		onupdate = fns ~= nil and fns.endonupdate or nil,

		onexit = fns ~= nil and fns.endonexit or nil,

        events =
        {
            EventHandler("animqueueover", idleonanimover),
        },
    })
end
--------------------------------------------------------------------------
local function walkonanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("walk")
    end
end

local function walkontimeout(inst)
    inst.sg:GoToState("walk")
end

PP_CommonStates.AddWalkStates = function(states, timelines, anims, softstop, delaystart, fns)
    table.insert(states, State{
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if fns ~= nil and fns.startonenter ~= nil then -- this has to run before WalkForward so that startonenter has a chance to update the walk speed
				fns.startonenter(inst)
			end
			if not delaystart then
	            inst.components.locomotor:WalkForward()
			end
            inst.AnimState:PlayAnimation(get_loco_anim(inst, anims ~= nil and anims.startwalk or nil, "walk_pre"))
        end,

        timeline = timelines ~= nil and timelines.starttimeline or nil,

		onupdate = fns ~= nil and fns.startonupdate or nil,

		onexit = fns ~= nil and fns.startonexit or nil,

        events =
        {
            EventHandler("animover", walkonanimover),
        },
    })

    table.insert(states, State{
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if fns ~= nil and fns.walkonenter ~= nil then
				fns.walkonenter(inst)
			end
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation(get_loco_anim(inst, anims ~= nil and anims.walk or nil, "walk_loop"), true)
        end,

        timeline = timelines ~= nil and timelines.walktimeline or nil,

		onupdate = fns ~= nil and fns.walkonupdate or nil,

		onexit = fns ~= nil and fns.walkonexit or nil,

        events =
        {
            EventHandler("animover", walkonanimover),
        },
    })

    table.insert(states, State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
			if fns ~= nil and fns.exitonenter ~= nil then
				fns.exitonenter(inst)
			end
            inst.components.locomotor:StopMoving()
            if softstop == true or (type(softstop) == "function" and softstop(inst)) then
                inst.AnimState:PushAnimation(get_loco_anim(inst, anims ~= nil and anims.stopwalk or nil, "walk_pst"), false)
            else
                inst.AnimState:PlayAnimation(get_loco_anim(inst, anims ~= nil and anims.stopwalk or nil, "walk_pst"))
            end
        end,

        timeline = timelines ~= nil and timelines.endtimeline or nil,

		onupdate = fns ~= nil and fns.endonupdate or nil,

		onexit = fns ~= nil and fns.endonexit or nil,

        events =
        {
            EventHandler("animqueueover", idleonanimover),
        },
    })
end
--------------------------------------------------------------------------
local function GetOverrideAnim(inst, override, default)
    return override and (type(override) == "function" and override(inst) or override) or default
end
--------------------------------------------------------------------------
PP_CommonStates.AddOpenGiftStates = function(states, anim, timelines, sounds, fns, events)
	table.insert(states, State {
		name = "opengift",
		tags = {"busy", "pausepredict"},

		onenter = function(inst, data)
			inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "taunt"), true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)
		end,

		timeline = timelines and timelines.opengifttimeline or 
		{
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "taunt"), true)
            end),
        },
		
		events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "taunt"))
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
	})
end
--------------------------------------------------------------------------
--------------------------------------------------------------------------
PP_CommonStates.AddJumpInStates = function(states, timelines, anim, sounds, fns, events)
	table.insert(states, State{
		name = "jumpin_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
			if not (inst.noactions or inst._isflying) then
				inst.Physics:Stop()
				inst:PerformBufferedAction()
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "run_pst"))
			else
				inst.sg:GoToState("idle")
			end
			--print("DEBUG: JUMPIN PRE ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.bufferedaction ~= nil then
                        inst:PerformBufferedAction()
						--print("DEBUG: Buffered Action did not return nil! "..inst.bufferedaction)
                    else
						--print("DEBUG: Buffered Action did return nil!")
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    })
	
	table.insert(states, State{
		name = "jumpin",
        tags = {"doing", "busy"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			--print("DEBUG: Jumpin State ran")
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "run_pst"))
			
			inst.components.locomotor:Stop()

            inst.sg.statemem.target = data.teleporter
            inst.sg.statemem.heavy = inst.components.inventory:IsHeavyLifting()

            if data.teleporter ~= nil and data.teleporter.components.teleporter ~= nil then
				--print("DEBUG: DATA IS NOT NIL!")
                data.teleporter.components.teleporter:RegisterTeleportee(inst)
            end
			
            local pos = data ~= nil and data.teleporter and data.teleporter:GetPosition() or nil
			
			inst.sg.statemem.teleportarrivestate = "idle"
        end,

		timeline=
        {
			--TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.target ~= nil and
                        inst.sg.statemem.target:IsValid() and
                        inst.sg.statemem.target.components.teleporter ~= nil then
                        --Unregister first before actually teleporting
                        inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
                        if inst.sg.statemem.target.components.teleporter:Activate(inst) then
                            inst.sg.statemem.isteleporting = true
                            inst.components.health:SetInvincible(true)
                            if inst.components.playercontroller ~= nil then
                                inst.components.playercontroller:Enable(false)
                            end
                            inst:Hide()
                            inst.DynamicShadow:Enable(false)
                            return
                        end
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end

            if inst.sg.statemem.isteleporting then
                inst.components.health:SetInvincible(false)
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
                inst:Show()
                inst.DynamicShadow:Enable(true)
            elseif inst.sg.statemem.target ~= nil
                and inst.sg.statemem.target:IsValid()
                and inst.sg.statemem.target.components.teleporter ~= nil then
                inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
            end
        end,
    })
	
	table.insert(states, State{
		name = "jumpout", --unused but is functional
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
			--print("DEBUG: JUMPOUT ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                        inst.sg:GoToState("idle")
                end
            end),
        },
    })
end
--------------------------------------------------------------------------
--[[
PP_CommonState.AddForgeStates = function(states, timelines, anims, fns, events)
	

end]]

--------------------------------------------------------------------------
PP_CommonStates.AddCorpseStates = function(states, isflying, timelines, anims, sounds, fns, events, nofx)
	table.insert(states, State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

			if fns and fns.corpse then
				fns.corpse(inst)
			end
			
            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anims and anims.corpse, "death"))
			if sounds and sounds.corpse then
				inst.SoundEmitter:PlaySound(sounds.corpse)
			end
        end,
		
		timeline = timelines and timelines.corpse,
		
		ontimeout = fns.corpse_timeout and fns.corpse_timeout(inst),

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    })
	
	table.insert(states, State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

			if fns and fns.corpse_rebirth then
				fns.corpse_rebirth(inst)
			end
			
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
			TimeEvent((96) * FRAMES, function(inst)
				if not nofx then
					local pos = inst:GetPosition()
					local fire = SpawnPrefab("lavaarena_portal_player_fx")
					fire.Transform:SetPosition(pos.x, 0, pos.z)
					if inst:HasTag("largecreature") or inst:HasTag("epic") then
						fire.Transform:SetScale(3, 3, 3)
					end
				end	
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    })
	
	table.insert(states, State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
			if not isflying then
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
			end
			
			if fns and fns.corpse_taunt then
				fns.corpse_taunt(inst)
			end

			if sounds and sounds.corpse_taunt then
				inst.SoundEmitter:PlaySound(sounds.corpse_taunt)
			end
			
			inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anims and anims.corpse_taunt, "taunt"))
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

		timeline= timelines and timelines.corpse_taunt,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
			if not isflying then
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)
			end

            SerializeUserSession(inst)
        end,
    })
end
--------------------------------------------------------------------------
--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end
--

PP_CommonStates.AddActionStates = function(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events, opts)
	table.insert(states, State {
		name = "death_instant",
        tags = {"busy"},

        onenter = function(inst, cause)
			if cause then
				inst.sg.statemem.cause = cause
			end
            inst.Physics:Stop()
			inst:Hide()
			inst.DynamicShadow:Enable(false)
			if inst.components.inventory and TheNet:GetServerGameMode() ~= "lavaarena" then
				inst.components.inventory:DropEverything(true)
			end
			inst:Hide()
			inst.sg:SetTimeout(90 * FRAMES)
        end,

		ontimeout = function(inst)
			if inst:HasTag("player") then
				inst:Show()
				inst.components.health:SetPercent(0, 0, inst.sg.statemem.cause or nil)
				inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
				inst.sg.statemem.cause = nil
			end
        end, 
    })
	table.insert(states, State {
        name = "usewardrobe",
        tags = { "doing" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, exitanim, "idle_loop"))
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
	})
	
	table.insert(states, State {
		name = "openwardrobe",
        tags = { "inwardrobe", "busy", "pausepredict" },

        onenter = function(inst, data)
            inst.sg.statemem.isopeninggift = data.openinggift
            if not inst.sg.statemem.isopeninggift then
                inst.components.locomotor:Stop()
                inst.components.locomotor:Clear()
                inst:ClearBufferedAction()

                if enteranim then
					inst.AnimState:PlayAnimation(GetOverrideAnim(inst, enteranim, "idle_loop"))
				end
				if loopanim then
					inst.AnimState:PushAnimation(GetOverrideAnim(inst, loopanim, "idle_loop"), true)
				end

                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:RemotePausePrediction()
                    inst.components.playercontroller:EnableMapControls(false)
                    inst.components.playercontroller:Enable(false)
                end
                inst.components.inventory:Hide()
                inst:PushEvent("ms_closepopups")
                inst:ShowActions(false)
            elseif inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end            
            if data.target and data.target.components.groomer then
                assert(data.target.components.groomer.occupant,"Grooming station had not occupant")
                inst:ShowPopUp(POPUPS.GROOMER, true, data.target.components.groomer.occupant, inst)
            else
                inst:ShowPopUp(POPUPS.WARDROBE, true, data.target)
            end
        end,

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NOWARDROBEONFIRE"))
                end
            end),
        },

        onexit = function(inst)
            inst:ShowPopUp(POPUPS.GROOMER, false)
            inst:ShowPopUp(POPUPS.WARDROBE, false)
            if not inst.sg.statemem.ischanging then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
                if not inst.sg.statemem.isclosingwardrobe then
                    inst.sg.statemem.isclosingwardrobe = true
                    POPUPS.WARDROBE:Close(inst)
                end
            end
        end,
    })
	
	table.insert(states, State{
		name = "changeinwardrobe",
        tags = { "inwardrobe", "busy", "nopredict", "silentmorph" },

        onenter = function(inst, delay)
            --This state is only valid as a substate of openwardrobe
            inst:Hide()
            inst.DynamicShadow:Enable(false)
            inst.sg.statemem.isplayerhidden = true

            inst.sg:SetTimeout(0.5)
        end,

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, exitanim, "idle_loop"))
            inst:Show()
            inst.DynamicShadow:Enable(true)
            inst.sg.statemem.isplayerhidden = nil
            PlayablePets.SetSkin(inst, inst.mob_table, true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if not inst.sg.statemem.isplayerhidden and inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.task ~= nil then
                inst.sg.statemem.task:Cancel()
                inst.sg.statemem.task = nil
            end
            if inst.sg.statemem.isplayerhidden then
                inst:Show()
                inst.DynamicShadow:Enable(true)
                inst.sg.statemem.isplayerhidden = nil
            end
            --Cleanup from openwardobe state
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:EnableMapControls(true)
                inst.components.playercontroller:Enable(true)
            end
            inst.components.inventory:Show()
            inst:ShowActions(true)
            if not inst.sg.statemem.isclosingwardrobe then
                inst.sg.statemem.isclosingwardrobe = true
                POPUPS.WARDROBE:Close(inst)
            end
        end,
	})
	
	table.insert(states, State { 
		name = "dressupwardrobe",
        tags = { "busy", "pausepredict", "nomorph", "nodangle" },

        onenter = function(inst, cb)
			inst.sg.statemem.cb = cb
			inst.sg:SetTimeout(1)
			if enteranim then
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, enteranim, "idle_loop"))
			end
			if loopanim then
				inst.AnimState:PushAnimation(GetOverrideAnim(inst, loopanim, "idle_loop"), true)
			end

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		ontimeout = function(inst)
			inst.AnimState:PlayAnimation(GetOverrideAnim(inst, exitanim, "idle_loop"), false)
            if inst.sg.statemem.cb ~= nil then
                inst.sg.statemem.cb()
                inst.sg.statemem.cb = nil
            end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.cb ~= nil then
                -- in case of interruption
                inst.sg.statemem.cb()
                inst.sg.statemem.cb = nil
            end
            --Cleanup from openwardobe state
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:EnableMapControls(true)
                inst.components.playercontroller:Enable(true)
            end
            inst.components.inventory:Show()
            inst:ShowActions(true)
            if not inst.sg.statemem.isclosingwardrobe then
                inst.sg.statemem.isclosingwardrobe = true
                POPUPS.WARDROBE:Close(inst)
            end
        end,
	})

    table.insert(states, State {
		name = "pp_empty",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst)
            inst:Hide()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "run_pst"), false)
        end,
    })
	
	table.insert(states, State {
		name = "action",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
			if (inst.noactions or inst.isflying) then
				inst.sg:GoToState("idle")
			end
            inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "run_pst"), false)
			if anim2 then
				inst.AnimState:PushAnimation(GetOverrideAnim(inst, anim2, "run_pst"), false)
			end
        end,

		timeline = timelines and timelines or
		{
			TimeEvent(0 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
		},

        onexit = function(inst)
            --test fix for crafting queue bug.
            inst:ClearBufferedAction()
        end,

        events = events or {
			EventHandler("animqueueover", function(inst)
				inst.sg:GoToState("idle") 
			end),
        },
    })
	
	table.insert(states, State {
	
        name = "action_loop",
        tags = { "idle", "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
			if enteranim then
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, enteranim, "idle_loop"))
			end
			if loopanim then
				inst.AnimState:PushAnimation(GetOverrideAnim(inst, loopanim, "idle_loop"), true)
			end
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            if exitanim then
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, exitanim, "run_pst"))
			end
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    })
	
	table.insert(states, State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
			if (inst.noactions or inst.isflying) then
				inst.sg:GoToState("idle")
			else	
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
				)
			end
            
        end,
    })
	
	table.insert(states, State
    {
        name = "fishing_pre",
        tags = { "prefish", "fishing" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(enteranim or loopanim)
        end,

        timeline =
        {
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_cast") end),
            TimeEvent(15*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_baitsplash")
                    inst.sg:GoToState("fishing")
                end
            end),
        },
    })
	
	table.insert(states, State
    {
        name = "fishing",
        tags = { "fishing" },

        onenter = function(inst, pushanim)
            inst.AnimState:PlayAnimation(loopanim, true)
            local equippedTool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if equippedTool and equippedTool.components.fishingrod then
                equippedTool.components.fishingrod:WaitForFish()
            end
        end,

        events =
        {
            EventHandler("fishingnibble", function(inst) inst.sg:GoToState("fishing_nibble") end),
        },
    })
	
	table.insert(states, State
    {
        name = "fishing_pst",

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(exitanim)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })

	table.insert(states, State
    {
        name = "fishing_nibble",
        tags = { "fishing", "nibble" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(loopanim, true)
            inst.sg:SetTimeout(1 + math.random())
            inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_fishinwater", "splash")
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fishing", "bite_light_pst")
        end,

        events =
        {
            EventHandler("fishingstrain", function(inst) inst.sg:GoToState("fishing_strain") end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("splash")
        end,
    })
	
	table.insert(states, State
    {
        name = "fishing_strain",
        tags = { "fishing" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(loopanim)
            inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_fishinwater", "splash")
            inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_strain", "strain")
        end,

        events =
        {
            EventHandler("fishingcatch", function(inst, data)
                inst.sg:GoToState("catchfish")
            end),
            EventHandler("fishingloserod", function(inst)
                inst.sg:GoToState("loserod")
            end),

        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("splash")
            inst.SoundEmitter:KillSound("strain")
        end,
    })
	
	table.insert(states, State
    {
                name = "catchfish",
        tags = { "fishing", "catchfish", "busy" },

        onenter = function(inst, build)
            inst.AnimState:PlayAnimation(exitanim)
            --print("Using ", build, " to swap out fish01")
            --inst.AnimState:OverrideSymbol("fish01", build, "fish01")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_fishland") end),
            TimeEvent(0*FRAMES, function(inst)
                local equippedTool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                if equippedTool and equippedTool.components.fishingrod then
                    equippedTool.components.fishingrod:Collect()
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            --inst.AnimState:ClearOverrideSymbol("fish01")
        end,
    })
	
	table.insert(states, State
    {
        name = "loserod",
        tags = { "busy", "nopredict" },

        onenter = function(inst)
            local equippedTool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if equippedTool and equippedTool.components.fishingrod then
                equippedTool.components.fishingrod:Release()
                equippedTool:Remove()
            end
            inst.AnimState:PlayAnimation(exitanim)
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_lostrod") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })
	
	table.insert(states, State
    {
        name = "startchanneling",
        tags = { "doing", "busy", "prechanneling", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(enteranim or loopanim)
            inst.AnimState:PushAnimation(loopanim, true)
            inst.sg:SetTimeout(.7)
        end,

        timeline =
        {
            TimeEvent(7 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
            TimeEvent(9 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation(exitanim)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })
	
	table.insert(states, State
    {
        name = "channeling",
        tags = { "doing", "channeling", "nodangle" },

        onenter = function(inst, target)
            inst:AddTag("channeling")
            inst.components.locomotor:Stop()
            if not inst.AnimState:IsCurrentAnimation(loopanim) then
                inst.AnimState:PlayAnimation(loopanim, true)
            end
            inst.sg.statemem.target = target
        end,

        onupdate = function(inst)
            if not CanEntitySeeTarget(inst, inst.sg.statemem.target) then
                inst.sg:GoToState("stopchanneling")
            end
        end,

        events =
        {
            EventHandler("ontalk", function(inst)
                if not (inst.AnimState:IsCurrentAnimation(loopanim) or inst:HasTag("mime")) then
                    inst.AnimState:PlayAnimation(loopanim, true)
                end
            end),
            EventHandler("donetalking", function(inst)
                if not inst.AnimState:IsCurrentAnimation(loopanim) then
                    inst.AnimState:PlayAnimation(loopanim, true)
                end
            end),
        },

        onexit = function(inst)
            inst:RemoveTag("channeling")
            if inst.sg.statemem.talktask ~= nil then
                inst.sg.statemem.talktask:Cancel()
                inst.sg.statemem.talktask = nil
            end
            if not inst.sg.statemem.stopchanneling and
                inst.sg.statemem.target ~= nil and
                inst.sg.statemem.target:IsValid() and
                inst.sg.statemem.target.components.channelable ~= nil then
                inst.sg.statemem.target.components.channelable:StopChanneling(true)
            end
        end,
    })
	
	table.insert(states, State
    {
		name = "stopchanneling",
        tags = { "idle", "nodangle" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(exitanim)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })
	
	table.insert(states, State --construct
    {
        name = "startconstruct",

        onenter = function(inst)
            inst.sg:GoToState("construct", (inst:HasTag("fastbuilder") or inst.fastbuilder) and .5 or 1)
        end,
    })
	
	table.insert(states, State --construct
    {
        name = "construct",
        tags = { "doing", "busy", "nodangle" },

        onenter = function(inst, timeout)
            inst.components.locomotor:Stop()
            inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            if timeout ~= nil then
                inst.sg:SetTimeout(timeout)
                inst.sg.statemem.delayed = true
                inst.AnimState:PlayAnimation(enteranim or loopanim)
                inst.AnimState:PushAnimation(loopanim, true)
            else
                inst.sg:SetTimeout(.7)
                inst.AnimState:PlayAnimation(enteranim or loopanim)
                inst.AnimState:PushAnimation(loopanim, true)
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                if inst.sg.statemem.delayed then
                    inst.sg:RemoveStateTag("busy")
                end
            end),
            TimeEvent(9 * FRAMES, function(inst)
                if not (inst.sg.statemem.delayed or inst:PerformBufferedAction()) then
                    inst.sg:RemoveStateTag("busy")
                end
            end),
        },

        ontimeout = function(inst)
            if not inst.sg.statemem.delayed then
                inst.SoundEmitter:KillSound("make")
                inst.AnimState:PlayAnimation(exitanim)
            elseif not inst:PerformBufferedAction() then
                inst.SoundEmitter:KillSound("make")
                inst.AnimState:PlayAnimation(exitanim)
            end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.constructing then
                inst.SoundEmitter:KillSound("make")
            end
        end,
    })
	
	table.insert(states, State --construct
    {
        name = "constructing",
        tags = { "doing", "busy", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            if not inst.SoundEmitter:PlayingSound("make") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            end
            if not inst.AnimState:IsCurrentAnimation(loopanim) then
                if inst.AnimState:IsCurrentAnimation(loopanim) then
                    inst.AnimState:PlayAnimation(exitanim)
                    inst.AnimState:PushAnimation(loopanim, true)
                else
                    inst.AnimState:PlayAnimation(loopanim, true)
                end
            end
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        onupdate = function(inst)
            if not CanEntitySeeTarget(inst, inst) then
                inst.AnimState:PlayAnimation(exitanim)
                inst.sg:GoToState("idle", true)
            end
        end,

        events =
        {
            EventHandler("stopconstruction", function(inst)
                inst.AnimState:PlayAnimation(exitanim)
                inst.sg:GoToState("idle", true)
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.constructing then
                inst.SoundEmitter:KillSound("make")
                inst.components.constructionbuilder:StopConstruction()
            end
        end,
    })
	
	table.insert(states, State 
    {
        name = "dotalk",
        tags = { "busy" },

        onenter = function(inst, target)
            if target and inst.components.talker.ignoring then
				inst.components.talker:StopIgnoringAll()
				inst.components.talker:Say(target.components.inspectable.getspecialdescription)
				--inst.components.talker:IgnoreAll()
			end
        end,

        timeline =
        {
--            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_fishland") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)

        end,
    })
	
	table.insert(states, State --construct
    {
         name = "construct_pst",
        tags = { "doing", "busy", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            if not inst.SoundEmitter:PlayingSound("make") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            end
            inst.AnimState:PlayAnimation(enteranim or loopanim)
            inst.AnimState:PushAnimation(loopanim, true)
            inst.sg:SetTimeout(inst:HasTag("fastbuilder") and .5 or 1)
        end,

        ontimeout = function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.AnimState:PlayAnimation(exitanim)
            inst.sg.statemem.finished = true
            inst.components.constructionbuilder:OnFinishConstruction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("make")
            if not inst.sg.statemem.finished then
                inst.components.constructionbuilder:StopConstruction()
            end
        end,
    })
	
	table.insert(states, State --bundle
    {
        name = "bundle",
        tags = { "doing", "busy", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            if enteranim then
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, enteranim, "idle_loop"))
			end
			if loopanim then
				inst.AnimState:PushAnimation(GetOverrideAnim(inst, loopanim, "idle_loop"), true)
			end
            inst.sg:SetTimeout(.7)
        end,

        timeline =
        {
            TimeEvent(7 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
            TimeEvent(9 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        ontimeout = function(inst)
            inst.SoundEmitter:KillSound("make")
			if exitanim then
				inst.AnimState:PlayAnimation(exitanim)
			end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.bundling then
                inst.SoundEmitter:KillSound("make")
            end
        end,
    })
	
	table.insert(states, State --bundle
    {
        name = "bundling",
        tags = { "doing", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            if not inst.SoundEmitter:PlayingSound("make") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            end
            inst.AnimState:PlayAnimation(loopanim, true)
        end,

        onupdate = function(inst)

        end,

        onexit = function(inst)
            if not inst.sg.statemem.bundling then
                inst.SoundEmitter:KillSound("make")
                inst.components.bundler:StopBundling()
            end
        end,
    })
	
	table.insert(states, State --bundle
    {
        name = "bundle_pst",
        tags = { "doing", "busy", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            if not inst.SoundEmitter:PlayingSound("make") then
                inst.SoundEmitter:PlaySound("dontstarve/wilson/make_trap", "make")
            end
            inst.AnimState:PlayAnimation(loopanim, true)
            inst.sg:SetTimeout(.7)
        end,

        ontimeout = function(inst)
            inst.sg:RemoveStateTag("busy")
            if exitanim then
				inst.AnimState:PlayAnimation(exitanim)
			end
            inst.sg.statemem.finished = true
            inst.components.bundler:OnFinishBundling()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("make")
            if not inst.sg.statemem.finished then
                inst.components.bundler:StopBundling()
            end
        end,
    })
	
	--fishing--
	table.insert(states, State 
    {
        name = "fishing_ocean_pre",

        onenter = function(inst)
            inst:PerformBufferedAction()
			inst.sg:GoToState("idle")
        end,
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_cast",
        tags = { "prefish", "fishing" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(enteranim and enteranim or anim)
            inst.AnimState:PushAnimation(loopanim, true)
        end,

        timeline =
        {
            TimeEvent(13*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_cast")
				inst.sg:RemoveStateTag("prefish")
				inst:PerformBufferedAction()
			end),
        },

        events =
        {
            EventHandler("newfishingtarget", function(inst, data)
				if data ~= nil and data.target ~= nil and not data.target:HasTag("projectile") then
					inst.sg.statemem.hooklanded = true
		            inst.AnimState:PushAnimation(exitanim, false)
				end
			end),

            EventHandler("animqueueover", function(inst)
	            if inst.sg.statemem.hooklanded and inst.AnimState:AnimDone() then
                    inst.sg:GoToState("oceanfishing_idle")
                end
            end),
        },
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_idle",
        tags = { "fishing", "canrotate" },

        onenter = function(inst)
			inst:AddTag("fishing_idle")
            local rod = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			local target = (rod ~= nil and rod.components.oceanfishingrod ~= nil) and rod.components.oceanfishingrod.target or nil
			if target ~= nil and target.components.oceanfishinghook ~= nil and TUNING.OCEAN_FISHING.IDLE_QUOTE_TIME_MIN > 0 then
				inst.sg:SetTimeout(TUNING.OCEAN_FISHING.IDLE_QUOTE_TIME_MIN + math.random() * TUNING.OCEAN_FISHING.IDLE_QUOTE_TIME_VAR)
			end
        end,

		onupdate = function(inst)
            local rod = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			rod = (rod ~= nil and rod.components.oceanfishingrod ~= nil) and rod or nil
			local target = rod ~= nil and rod.components.oceanfishingrod.target or nil
			if target ~= nil then
				if target.components.oceanfishinghook ~= nil or rod.components.oceanfishingrod:IsLineTensionLow() then
					inst.SoundEmitter:KillSound("unreel_loop")
					inst.AnimState:PlayAnimation(loopanim, true)
				elseif rod.components.oceanfishingrod:IsLineTensionGood() then
					inst.SoundEmitter:KillSound("unreel_loop")
					inst.AnimState:PlayAnimation(loopanim, true)
				end
			end
		end,

        ontimeout = function(inst)
			if inst.components.talker ~= nil then
				inst.components.talker:Say(GetString(inst, "ANNOUNCE_OCEANFISHING_IDLE_QUOTE"), nil, nil, true)

				inst.sg:SetTimeout(inst.sg.timeinstate + TUNING.OCEAN_FISHING.IDLE_QUOTE_TIME_MIN + math.random() * TUNING.OCEAN_FISHING.IDLE_QUOTE_TIME_VAR)
			end
        end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("unreel_loop")
			inst:RemoveTag("fishing_idle")
		end,
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_reel",
        tags = { "fishing", "doing", "reeling", "canrotate" },

        onenter = function(inst)
			inst:AddTag("fishing_idle")
            inst.components.locomotor:Stop()

            local rod = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
			rod = (rod ~= nil and rod.components.oceanfishingrod ~= nil) and rod or nil
			local target = rod ~= nil and rod.components.oceanfishingrod.target or nil
			if target == nil then
	            inst:ClearBufferedAction()
			else
				if inst:PerformBufferedAction() then
					if target.components.oceanfishinghook ~= nil or rod.components.oceanfishingrod:IsLineTensionLow() then
                        inst.SoundEmitter:KillSound("reel_loop")
						inst.AnimState:PlayAnimation(loopanim, true)
					elseif rod.components.oceanfishingrod:IsLineTensionGood() then
						inst.SoundEmitter:KillSound("reel_loop")
						inst.AnimState:PlayAnimation(loopanim, true)
					end

					inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
				end

			end
        end,

        timeline =
        {
            TimeEvent(TUNING.OCEAN_FISHING.REEL_ACTION_REPEAT_DELAY, function(inst) inst.sg.statemem.allow_repeat = true end),
        },

        ontimeout = function(inst)
			inst.sg:GoToState("oceanfishing_idle")
        end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("reel_loop")
			inst:RemoveTag("fishing_idle")
		end,
    })
	
	table.insert(states, State 
    {
         name = "oceanfishing_sethook",
        tags = { "fishing", "doing", "busy" },

        onenter = function(inst)
			inst:AddTag("fishing_idle")
            inst.components.locomotor:Stop()

            inst.AnimState:PlayAnimation(enteranim and enteranim or anim)
			inst:PerformBufferedAction()
        end,

        timeline =
        {
--            TimeEvent(2*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("oceanfishing_idle") end),
        },

		onexit = function(inst)
			inst.SoundEmitter:KillSound("sethook_loop")
			inst:RemoveTag("fishing_idle")
		end,
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_catch",
        tags = { "fishing", "catchfish", "busy" },

        onenter = function(inst, build)
            inst.AnimState:PlayAnimation(exitanim)
        end,

        timeline =
        {
--            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_fishland") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)

        end,
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_stop",
        tags = { "fishing" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(exitanim)

			if data ~= nil and data.escaped_str and inst.components.talker ~= nil then
				inst.components.talker:Say(GetString(inst, data.escaped_str), nil, nil, true)
			end
        end,

        timeline =
        {
--            TimeEvent(18*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })
	
	table.insert(states, State 
    {
        name = "oceanfishing_linesnapped",
        tags = { "busy", "nomorph" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(exitanim)
			inst.sg.statemem.escaped_str = data ~= nil and data.escaped_str or nil
        end,

        timeline =
        {
            TimeEvent(7 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/common/fishingpole_linebreak")
            end),
            TimeEvent(29*FRAMES, function(inst) 
				if inst.components.talker ~= nil then 
					inst.components.talker:Say(GetString(inst, inst.sg.statemem.escaped_str or "ANNOUNCE_OCEANFISHING_LINESNAP"), nil, nil, true)
				end
			end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    })
	--plant registry
	table.insert(states, State 
    {
        name = "plantregistry_open",
        tags = { "doing" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation(loopanim, true)
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

		onupdate = function(inst)
			if not CanEntitySeeTarget(inst, inst) then
                inst.sg:GoToState("plantregistry_close")
			end
		end,
		
		events = {
			EventHandler("ms_closepopup", function(inst, data)
				if data.popup == POPUPS.PLANTREGISTRY then
					inst.sg:GoToState("plantregistry_close")
				end
			end),
		},

        onexit = function(inst)
		    inst:ShowPopUp(POPUPS.PLANTREGISTRY, false)
        end,
    })
	
	table.insert(states, State 
    {
        name = "plantregistry_close",
        tags = { "idle", "nodangle" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.sg:GoToState("idle")
        end,
    })
	
	table.insert(states, State
	{
		name = "quicktele",
        tags = { "doing", "busy", "canrotate" },

        onenter = function(inst)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else
				inst.components.locomotor:Stop()
				inst.AnimState:PlayAnimation(exitanim)
				inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_weapon")
				--called by blinkstaff component
				inst.sg.statemem.onstartblinking = function()
					inst.sg:AddStateTag("noattack")
					inst.components.health:SetInvincible(true)
				end
				inst.sg.statemem.onstopblinking = function()
					inst.sg:RemoveStateTag("noattack")
				end
			end
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
        end,
	})

    table.insert(states, State
    {
        name = "acting_idle",
        tags = { "idle", "forcedangle", "acting"},

        onenter = function(inst, pre)
            inst.AnimState:PlayAnimation((opts and opts.actidle) and opts.actidle or anim)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("acting_idle")
            end),
        },
    })

    table.insert(states, State
    {
        name = "acting_talk",
        tags = { "idle", "forcedangle", "acting"},

        onenter = function(inst, pre)
            inst.sg:GoToState("acting_idle")
        end,

        events =
        {
           
        },
    })

    table.insert(states, State
    {
        name = "acting_talk",
        tags = { "talking", "acting" },

        onenter = function(inst, noanim)
            local function gettalk()
                return (opts and opts.acttalk) and opts.acttalk or anim
            end

            if not noanim then
                inst.AnimState:PlayAnimation(gettalk(),false)
            end
            --DoTalkSound(inst)
            inst.sg:SetTimeout(1.5 + math.random() * .5)
        end,

        ontimeout = function(inst)
            inst.sg.statemem.talkdone = true
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.sg.statemem.talkdone then
                    inst.sg:GoToState("acting_idle")
                else
                    local function gettalk()
                        return (opts and opts.acttalk) and opts.acttalk or anim
                    end
                    inst.AnimState:PlayAnimation(gettalk())
                end
            end),
            EventHandler("donetalking", function(inst)
                inst.sg.statemem.talkdone = true
            end),
        },

        onexit = function(inst)
            --StopTalkSound(inst)
        end,
    })

    table.insert(states, State{
        name = "acting_action",
        tags = { "talking", "acting" },

        onenter = function(inst, data)
            local loop = false
            if data.animtype == "loop" then
                loop = true
                inst.sg.statemem.loop = true
            end
            if data.animtype == "hold" then
                inst.sg.statemem.hold = true
            end

            if type(data.anim) == "table" then
                for i,animation in ipairs(data.anim)do
                    inst.sg.statemem.queue = true
                    if i == 1 then
                        if #data.anim == 1 and loop then
                            inst.AnimState:PlayAnimation((opts and opts.actidle) and opts.actidle or anim, true)
                        else
                            inst.AnimState:PlayAnimation((opts and opts.actidle) and opts.actidle or anim, false)
                        end
                    elseif i == #data.anim then
                        inst.AnimState:PushAnimation((opts and opts.actidle) and opts.actidle or anim, loop)
                    else 
                        inst.AnimState:PushAnimation((opts and opts.actidle) and opts.actidle or anim, false)
                    end
                end
            else
                inst.AnimState:PlayAnimation((opts and opts.actidle) and opts.actidle or anim, loop)
            end
            if data.line then
                --DoTalkSound(inst)
            end
        end,

        events =
        {
            EventHandler("donetalking", function(inst)
                StopTalkSound(inst)
                if not inst.sg.statemem.loop and not inst.sg.statemem.hold then
                    inst.sg:GoToState("acting_idle")
                end
            end),
            EventHandler("animover", function(inst)
                if not inst.sg.statemem.loop and not inst.sg.statemem.hold then
                    if not inst.sg.statemem.queue then
                        inst.sg:GoToState("acting_idle")
                    end
                end
            end),  
            EventHandler("animqueueover", function(inst)
                if not inst.sg.statemem.loop and not inst.sg.statemem.hold then
                    if inst.sg.statemem.queue then
                        inst.sg:GoToState("acting_idle")
                    end
                end
            end),
        },

        onexit = function(inst)
            --StopTalkSound(inst)
        end,
    })

    table.insert(states, State{
        name = "acting_bow",
        tags = { "nopredict", "forcedangle", "acting"},

        onenter = function(inst, target)
            inst.AnimState:PlayAnimation(exitanim,false)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("acting_idle")
            end),
        },
    })
	
end

----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------
PP_CommonStates.AddKnockbackState = function(states, timeline, anim, sounds, fns)
	table.insert(states, State {
        name = "knockback",
        tags = {"busy", "nopredict", "nomorph", "nodangle"},

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

			if fns and fns.anim then
				fns.anim(inst)
			else
				inst.AnimState:PlayAnimation(GetOverrideAnim(inst, anim, "hit"), true)
				if sounds then
					inst.SoundEmitter:PlaySound(sounds)
				end
				--inst.AnimState:SetTime(10* FRAMES)
			end

            if data and data.radius and data.knocker and data.knocker:IsValid() then
                local x, y, z = data.knocker.Transform:GetWorldPosition()
                local distsq = inst:GetDistanceSqToPoint(x, y, z)
                local rangesq = data.radius * data.radius
                local rot = inst.Transform:GetRotation()
                local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                local drot = math.abs(rot - rot1)
                while drot > 180 do
                    drot = math.abs(drot - 360)
                end
                local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                inst.sg.statemem.dspeed = 0
                if drot > 90 then
                    inst.sg.statemem.reverse = true
                    inst.Transform:SetRotation(rot1 + 180)
                    inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                else
                    inst.Transform:SetRotation(rot1)
                    inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                end
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed then
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.dspeed
                if inst.sg.statemem.speed < 0 then
                    inst.sg.statemem.dspeed = inst.sg.statemem.dspeed + .075
                    inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
                else
                    inst.sg.statemem.speed = nil
                    inst.sg.statemem.dspeed = nil
                    inst.Physics:Stop()
                end
            end
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.speed then
                inst.Physics:Stop()
            end
        end,
    })
end
--------------------------------------------------------------------------
PP_CommonStates.AddHomeState = function(states, timeline, enteranim, exitanim, forcetaunt, fns)
	table.insert(states, State {
        name = "home",
        tags = {"busy", "silentmorph", "invisible" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local target = inst:GetBufferedAction() and inst:GetBufferedAction().target or nil
            local failreason =
			   (target ~= nil and target.components.burnable ~= nil and
                    target.components.burnable:IsBurning() and
                    "ANNOUNCE_NOSLEEPONFIRE") 
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
				or ((target and target.components.multisleepingbag and not inst:HasTag(target.components.multisleepingbag.musthavetag) and "ANNOUNCE_NOHUNGERSLEEP"))
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            inst.AnimState:PlayAnimation(enteranim or "taunt")
            inst.sg:SetTimeout(11 * FRAMES)

            if fns and fns.onenter then
                fns.onenter(inst)
            end

            SetSleeperSleepState(inst)
        end,

        ontimeout = function(inst)
            local bufferedaction = inst:GetBufferedAction()
            if bufferedaction == nil then
                inst.AnimState:PlayAnimation(exitanim or "taunt")
                inst.sg:GoToState("idle", true)
                return
            end
            local home = bufferedaction.target
            if home == nil or                --home:HasTag("hassleeper") or
                --home:HasTag("siestahut") ~= TheWorld.state.isday or
                (home.components.burnable ~= nil and home.components.burnable:IsBurning()) then
                --Edge cases, don't bother with fail dialogue
                --Also, think I will let smoldering pass this one
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.AnimState:PlayAnimation(exitanim or "taunt")
                inst.sg:GoToState("idle", true)
            else
                inst:PerformBufferedAction()
                inst.components.health:SetInvincible(true)
                inst:Hide()
                if inst.Physics ~= nil then
                    inst.Physics:Teleport(inst.Transform:GetWorldPosition())
                end
                if inst.DynamicShadow ~= nil then
                    inst.DynamicShadow:Enable(false)
                end
                inst.sg:AddStateTag("sleeping")
				inst.sg:AddStateTag("home")
                inst.sg:RemoveStateTag("busy")
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
            end
        end,

        onexit = function(inst)
            if fns and fns.onexit then
                fns.onexit(inst)
            end
            inst.components.health:SetInvincible(false)
            inst:Show()
            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(true)
            end
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
				if inst.sleepingbag.components.sleepingbag then
					inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
				elseif inst.sleepingbag.components.multisleepingbag then
					inst.sleepingbag.components.multisleepingbag:DoWakeUp(inst)
				end
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst, forcetaunt or nil)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst, forcetaunt or nil)
            end
        end,
    })

    table.insert(states, State{
        name = "bedroll",
        tags = { "bedroll", "busy", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local failreason =
                (TheWorld.state.isday and
                    (TheWorld:HasTag("cave") and "ANNOUNCE_NODAYSLEEP_CAVE" or "ANNOUNCE_NODAYSLEEP")
                )
                --or (IsNearDanger(inst) and "ANNOUNCE_NODANGERSLEEP")
                -- you can still sleep if your hunger will bottom out, but not absolutely
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            if fns.bedroll and fns.bedroll.onenter then
                fns.bedrll.onenter(inst)
            else
                inst.AnimState:PlayAnimation("sleep_pre")
                inst.AnimState:PushAnimation("sleep_loop", false)
            end

            SetSleeperSleepState(inst)
        end,

        timeline =
        {
            TimeEvent(20 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/wilson/use_bedroll")
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                if inst.sg:HasStateTag("sleeping") then
                    inst.sg.statemem.iswaking = true
                    inst.sg:GoToState("wakeup")
                end
            end),
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    if TheWorld.state.isday or
                        (inst.components.health ~= nil and inst.components.health.takingfiredamage) or
                        (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
                        inst:PushEvent("performaction", { action = inst.bufferedaction })
                        inst:ClearBufferedAction()
                        inst.sg.statemem.iswaking = true
                        inst.sg:GoToState("wakeup")
                    elseif inst:GetBufferedAction() then
                        inst:PerformBufferedAction() 
                        if inst.components.playercontroller ~= nil then
                            inst.components.playercontroller:Enable(true)
                        end
                        inst.sg:AddStateTag("sleeping")
                        inst.sg:AddStateTag("silentmorph")
                        inst.sg:RemoveStateTag("nomorph")
                        inst.sg:RemoveStateTag("busy")
                        inst.AnimState:PlayAnimation("sleep_loop", true)
                    else
                        inst.sg.statemem.iswaking = true
                        inst.sg:GoToState("wakeup")
                    end
                end
            end),
        },

        onexit = function(inst)
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    })
end
------------------------------------------------------------------------
--					IA & ATC
------------------------------------------------------------------------
PP_CommonStates.AddSailStates = function(states, timeline, anim, anim2)
	table.insert(states, State {
        name = "embark",
        tags = {"busy"},

        onenter = function(inst, data)

			inst.AnimState:PlayAnimation(anim)
			local BA = inst:GetBufferedAction()
			inst.sg.statemem.targetpos = BA.target and BA.target:GetPosition()
            if BA.target and BA.target.components.sailable and not BA.target.components.sailable:IsOccupied() then
                BA.target.components.sailable.isembarking = true
                if inst.components.sailor and inst.components.sailor.boat then
                    inst.components.sailor:Disembark(nil, true)
                else
					inst.Transform:SetPosition(inst.sg.statemem.targetpos:Get())
                end
                inst.components.sailor:Embark(BA.target)
				inst.Transform:SetPosition(inst.sg.statemem.targetpos:Get())
            end
			inst:PerformBufferedAction()
            local boat = inst.components.sailor.boat
            if boat and boat.landsound then
                inst.SoundEmitter:PlaySound(boat.landsound)
            end
			boat.components.sailable:OnEmbarked(inst)
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            
        end,
    })
	
	table.insert(states, State {
        name = "disembark",
        tags = {"busy"},

        onenter = function(inst, data)
			inst.AnimState:PlayAnimation(anim)
			inst.components.locomotor:Stop()
			inst:PerformBufferedAction()            
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            
        end,
    })

    table.insert(states, State {
        name = "jumpboat",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation(anim)
			inst.components.locomotor:Stop()
            local boatjumper = inst.components.boatjumper
            local pos = Point(boatjumper.disembark_x, 0, boatjumper.disembark_z)
			--inst:PerformBufferedAction()  
			if pos then
				inst.Transform:SetPosition(pos.x, 0, pos.z)
			end
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            
        end,
    })
	
	table.insert(states, State {
        name = "jumpoffboatstart",
        tags = {"busy"},

        onenter = function(inst, pos)
			inst.AnimState:PlayAnimation(anim)
			inst.components.locomotor:Stop()
			--inst:PerformBufferedAction()  
			if pos then
				inst.Transform:SetPosition(pos.x, 0, pos.z)
			end
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            
        end,
    })
	
	table.insert(states, State {
        name = "sailing",
        tags = {"idle", "canrotate", "running", "sailing", "autopredict", "moving"},

        onenter = function(inst, pos)
			inst.AnimState:PlayAnimation(anim2, true)
			inst.components.locomotor:RunForward()
			
			local boat = inst.replica.sailor:GetBoat()

            local loopsound = nil 
            local flapsound = nil 

            if boat and boat.replica.container and boat.replica.container.hasboatequipslots then 
                local sail = boat.replica.container:GetItemInBoatSlot(BOATEQUIPSLOTS.BOAT_SAIL)
                if sail then 
                    loopsound = sail.loopsound
                    flapsound = sail.flapsound
                end 
            elseif boat and boat.replica.sailable.sailsound then
                loopsound = boat.replica.sailable.sailsound
            end
           
            if not inst.SoundEmitter:PlayingSound("sail_loop") and loopsound then 
                inst.SoundEmitter:PlaySound(loopsound, "sail_loop", nil, true)
            end 

            if flapsound then 
                inst.SoundEmitter:PlaySound(flapsound, nil, nil, true) 
            end
			
			if boat and boat.replica.sailable.creaksound then
                inst.SoundEmitter:PlaySound(boat.replica.sailable.creaksound, nil, nil, true)
            end
			
			if boat and boat.components.rowboatwakespawner then 
                --boat.components.rowboatwakespawner:StartSpawning()
            end 
        end,

        timeline = timeline,
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("sail_loop")
			local boat = inst.replica.sailor:GetBoat()
			if boat and boat.components.rowboatwakespawner then 
				boat.components.rowboatwakespawner:StopSpawning()
			end 
		end,

        events = {
            EventHandler("animover", function(inst)

            end),
        },

        onexit = function(inst)
            
        end,
    })
	
	table.insert(states, State {
        name = "sailing_stop",
        tags = {"idle", "canrotate", "running"},

        onenter = function(inst, pos)
			inst.AnimState:PlayAnimation(anim2)
			inst.components.locomotor:Stop()
			local boat = inst.replica.sailor:GetBoat()
			if boat and boat.components.rowboatwakespawner then 
				boat.components.rowboatwakespawner:StopSpawning()
			end 
        end,

        timeline = timeline,

        events = {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = function(inst)
            
        end,
    })
end 

-------------------------------------------------------------------
--				Ocean Stuff
-------------------------------------------------------------------
PP_CommonStates.AddSailorStates = function(states, timelines, anims)
	table.insert(states, State {
        name = "stop_steering",
        tags = { },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(anims.stop_steering or "walk_pst")        
            inst:PerformBufferedAction()
        end,    

        events = 
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle", true)
            end),          
        }
    })
	
	table.insert(states, State {
        name = "furl",
        tags = {"doing", "busy"},

        onenter = function(inst)
			inst.sg.mem.furl_target = inst.bufferedaction.target or inst.sg.mem.furl_target
            inst.AnimState:PlayAnimation(anims.stop_steering or "walk_pst")        
            inst:PerformBufferedAction()
			inst.sg.mem.furl_target.components.mast:SailFurled()
        end,    
		
		onexit = function(inst)
            if not inst.sg.statemem.not_interrupted then
               -- inst.sg.mem.furl_target.components.mast:RemoveSailFurler(inst)                
                --inst:RemoveTag("is_furling")
                --inst:RemoveTag("is_heaving")
            end
        end,

        events = 
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle", true)
            end),          
        }
    })
	
	table.insert(states, State {
        name = "mount_plank",
        tags = { "idle" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(anims.plank_idle or "plank_idle_pre")
			if anims and anims.plank_idle_loop then
				inst.AnimState:PushAnimation(anims.plank_idle_loop, true) 
			end	
            inst:AddTag("on_walkable_plank")        
            inst:PerformBufferedAction()       

            inst.sg:SetTimeout(180 * FRAMES)
        end,    

        onexit = function(inst)            
            if inst.components.walkingplankuser and inst.components.walkingplankuser.current_plank then
				inst.components.walkingplankuser.current_plank.components.walkingplank:StopMounting()
			end
            inst:RemoveTag("on_walkable_plank")
        end,

        ontimeout = function(inst)               
            inst.AnimState:PlayAnimation(anims.plank_idle_pst or "walk_pst")        
            inst.sg:GoToState("idle", true)
        end, 
    })
	
	table.insert(states, State {
        name = "abandon_ship",
        tags = { "busy" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation(anims.plank_hop_pre or "plank_hop_pre") 
			if anims and anims.plank_hop then
				inst.AnimState:PushAnimation(anims.plank_hop, false) 
			end
			inst.sg:SetTimeout(130 * FRAMES)
        end,    

		onexit = function(inst)
			inst:Show()
		end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("idle")
        end, 
		
        events = 
        {
            EventHandler("animqueueover", function(inst)
				local splash = SpawnPrefab("splash_green")
				splash.Transform:SetPosition(inst:GetPosition():Get())
				if inst:HasTag("epic") or inst:HasTag("largecreature") then
					splash.Transform:SetScale(2, 2, 2)
					inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
				end
                if inst.components.drownable then
					inst.components.drownable:OnFallInOcean()
					inst.components.drownable:DropInventory()
					inst.components.drownable:WashAshore() -- TODO: try moving this into the timeline
				end
				inst:Hide()
            end),
        }
    })
	
	table.insert(states, State {
        name = "steer_boat_idle_pre",
        tags = { "is_using_steering_wheel" },

        onenter = function(inst, skip_pre)
            inst.AnimState:PlayAnimation(anims.steer_pre or "walk_pst")        
            inst:PerformBufferedAction()
        end,    

        events = 
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("steer_boat_idle_loop")
            end),          
        }
    })
	
	table.insert(states, State {
        name = "steer_boat_idle_loop",
        tags = { "is_using_steering_wheel" },

        onenter = function(inst, skip_pre)
            inst.AnimState:PushAnimation(anims.steer_idle or "idle_loop", true)
        end, 
    })
	
	table.insert(states, State {
         name = "steer_boat_turning",
        tags = { "is_using_steering_wheel" },

        onenter = function(inst, data)
            inst.AnimState:PlayAnimation(anims.steer_turning or "walk_pst") 
			inst.SoundEmitter:PlaySound("turnoftides/common/together/boat/steering_wheel/turn")
            inst:PerformBufferedAction()
        end,


        timeline = (timelines and timelines.steer_turning) and timelines.steer_turning or 
		{

		},
		
		onexit = function(inst)
            inst.AnimState:SetSortWorldOffset(0, 0, 0)             
        end,    

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("steer_boat_idle_loop")
            end),
            --EventHandler("steer_boat_stop_turning", function(inst)
            --    inst.sg:GoToState("steer_boat", "steer_left_pst")
            --end),
        },
    })
	
	table.insert(states, State {
        name = "row",
        tags = { "rowing", "doing" },

        onenter = function(inst)
            local locomotor = inst.components.locomotor
            local target_pos = locomotor.bufferedaction.pos
            if target_pos == nil then
                target_pos = locomotor.bufferedaction.target:GetPosition()
            end
            inst.AnimState:PlayAnimation(anims.row or "walk_pst")
            locomotor:Stop() 
			
            local my_x, my_y, my_z = inst.Transform:GetWorldPosition()
            local boat_x, boat_y, boat_z = 0, 0, 0
            local boat = TheWorld.Map:GetPlatformAtPoint(my_x, my_z)
            if boat ~= nil then
                boat_x, boat_y, boat_z = boat.Transform:GetWorldPosition()
            end
			
			local target_x, target_z = target_pos.x, target_pos.z

            if inst.components.playercontroller.isclientcontrollerattached then
                local dir_x, dir_z = VecUtil_Normalize(my_x - boat_x, my_z - boat_z)
                target_x, target_z = my_x + dir_x, my_z + dir_z
            end  

            inst:ForceFacePoint(target_x, 0, target_z)
        end,    

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)      
				inst:PerformBufferedAction() 
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/small")
            end),

            TimeEvent(5 * FRAMES, function(inst)          
                inst.sg:RemoveStateTag("rowing")
            end),            
        },

        events = 
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),          
        },
    })
	
	table.insert(states, State {
        name = "sink_fast",
        tags = { "busy", "nopredict", "nomorph", "nointerrupt", "sink_fast" },

        onenter = function(inst, data)
			if inst.components.locomotor then
				inst.components.locomotor:Stop()
			end
			local splash = SpawnPrefab("splash_green")
			if inst.components.inventory and TheNet:GetServerGameMode() ~= "lavaarena" then
				inst.components.inventory:DropEverything(true)
			end
			splash.Transform:SetPosition(inst:GetPosition():Get())
			if inst:HasTag("epic") or inst:HasTag("largecreature") then
				splash.Transform:SetScale(2, 2, 2)
				inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
			end
			inst:Hide()
			inst.sg:SetTimeout(90 * FRAMES)
        end,

		ontimeout = function(inst)
			if inst:HasTag("player") then
				inst:Show()
				if TheNet:GetServerGameMode() == "lavaarena" then
					print("teleporting to land")
					inst.components.health:SetPercent(inst.components.health:GetPercent()/2)
					local target_pos = TheWorld.multiplayerportal and TheWorld.multiplayerportal:GetPosition() or Vector3(0,0,0)
					inst.Physics:Teleport(target_pos.x, 0, target_pos.z)
					inst.sg:GoToState("idle")
				else
					inst.components.health:SetPercent(0, 0, "drowning")
                    PlayablePets.DoDeath(inst)
				end
			end
        end,     
    })

    table.insert(states, State{
        name = "abyss_fall",
        tags = { "busy", "nopredict", "nomorph", "falling", "nointerrupt", "nowake" },

        onenter = function(inst, data)
            inst:ClearBufferedAction()

            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()

			inst.sg.statemem.collisionmask = inst.Physics:GetCollisionMask()
	        inst.Physics:SetCollisionMask(COLLISION.GROUND)

			if inst.DynamicShadow ~= nil then
			    inst.DynamicShadow:Enable(false)
			end

		    if inst.brain ~= nil then
				inst.brain:Stop()
			end

			DoVoidFall(inst)
            inst.sg:SetTimeout(90 * FRAMES)
        end,

        ontimeout = function(inst)
			if inst:HasTag("player") then
				inst:Show()
				if TheNet:GetServerGameMode() == "lavaarena" then
					--print("teleporting to land")
					inst.components.health:SetPercent(inst.components.health:GetPercent()/2)
					local target_pos = TheWorld.multiplayerportal and TheWorld.multiplayerportal:GetPosition() or Vector3(0,0,0)
					inst.Physics:Teleport(target_pos.x, 0, target_pos.z)
					inst.sg:GoToState("idle")
				else
					inst.components.health:SetPercent(0, 0, "pp_falling")
                    PlayablePets.DoDeath(inst)
				end
			end
        end,   

        events =
        {
            EventHandler("animover", function(inst)
                if inst.sg.statemem.has_anim and inst.AnimState:AnimDone() then
					DoVoidFall(inst)
				end
            end),

            EventHandler("on_void_arrive", function(inst)
				inst.sg:GoToState("abyss_drop")
			end),
        },

        onexit = function(inst)
			if inst.sg.statemem.collisionmask ~= nil then
				inst.Physics:SetCollisionMask(inst.sg.statemem.collisionmask)
			end

            if inst.sg.statemem.isteleporting then
				if inst.components.health ~= nil then
					inst.components.health:SetInvincible(false)
				end
				inst:Show()
			end

			if inst.DynamicShadow ~= nil then
				inst.DynamicShadow:Enable(true)
			end

			if inst.components.herdmember ~= nil then
				inst.components.herdmember:Leave()
			end

			if inst.components.combat ~= nil then
				inst.components.combat:DropTarget()
			end

		    if inst.brain ~= nil then
				inst.brain:Start()
			end
        end,
    })

    table.insert(states, State{
        name = "abyss_drop",
        tags = { "doing", "busy", "nopredict", "silentmorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            if type(anims.voiddrop) == "table" then
                for i, v in ipairs(anims.voiddrop) do
                    if i == 1 then
                        inst.AnimState:PlayAnimation(v)
                    else
                        inst.AnimState:PushAnimation(v, false)
                    end
                end
            elseif anims.voiddrop ~= nil then
                inst.AnimState:PlayAnimation(anims.voiddrop)
            else
                inst.AnimState:PlayAnimation("sleep_loop")
                inst.AnimState:PushAnimation("sleep_pst", false)
            end

            if inst.brain ~= nil then
                inst.brain:Stop()
            end

            local x, y, z = inst.Transform:GetWorldPosition()
            SpawnPrefab("fallingswish_clouds_fast").Transform:SetPosition(x, y, z)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
		    if inst.brain ~= nil then
				inst.brain:Start()
			end
        end,
	})
end 



-------------------------------------------------------------------
--				Forge AoE States
-------------------------------------------------------------------
local function OnRemoveCleanupTargetFX(inst)
    if inst.sg.statemem.targetfx.KillFX ~= nil then
        inst.sg.statemem.targetfx:RemoveEventCallback("onremove", OnRemoveCleanupTargetFX, inst)
        inst.sg.statemem.targetfx:KillFX()
    else
        inst.sg.statemem.targetfx:Remove()
    end
end

local function ToggleOffPhysics(inst)
    inst.sg.statemem.isphysicstoggle = true
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
end

local function ToggleOnPhysics(inst)
    inst.sg.statemem.isphysicstoggle = nil
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)
end

local function IsWeaponEquipped(inst, weapon)
    return weapon ~= nil
        and weapon.components.equippable ~= nil
        and weapon.components.equippable:IsEquipped()
        and weapon.components.inventoryitem ~= nil
        and weapon.components.inventoryitem:IsHeldBy(inst)
end

PP_CommonStates.AddAoeStates = function(states, timelines, anims)
	table.insert(states, State {
        name = "combat_lunge_start",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(anims.lunge_pre or anims.generic)
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/common/twirl", nil, nil, true)
            end),
        },

        events =
        {
            EventHandler("combat_lunge", function(inst, data)
                inst.sg:GoToState("combat_lunge", data)
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.AnimState:IsCurrentAnimation(anims.lunge_pre or anims.generic) then
                        inst.AnimState:PlayAnimation(anims.lunge_loop or anims.generic, true)
                        inst:PerformBufferedAction()
                    else
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    })
	
	table.insert(states, State {
        name = "combat_lunge",
        tags = { "aoe", "doing", "busy", "nopredict", "nomorph" },

        onenter = function(inst, data)
            if data ~= nil and
                data.targetpos ~= nil and
                data.weapon ~= nil and
                data.weapon.components.aoeweapon_lunge ~= nil and
                inst.AnimState:IsCurrentAnimation(anims.lunge_loop or anims.generic) then
                inst.AnimState:PlayAnimation(anims.lunge_pst or anims.generic)
                inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_weapon")
                local pos = inst:GetPosition()
                if pos.x ~= data.targetpos.x or pos.z ~= data.targetpos.z then
                    inst:ForceFacePoint(data.targetpos:Get())
                end
                if data.weapon.components.aoeweapon_lunge:DoLunge(inst, pos, data.targetpos) then
                    inst.SoundEmitter:PlaySound("dontstarve/common/lava_arena/fireball")
                    inst.Physics:Teleport(data.targetpos.x, 0, data.targetpos.z)
                    inst.components.bloomer:PushBloom("lunge", "shaders/anim.ksh", -2)
                    inst.components.colouradder:PushColour("lunge", 1, 1, 0, 0)
                    inst.sg.statemem.flash = 1
                    return
                end
            end
            --Failed
            inst.sg:GoToState("idle")
        end,

        onupdate = function(inst)
            if inst.sg.statemem.flash > 0 then
                inst.sg.statemem.flash = math.max(0, inst.sg.statemem.flash - .1)
                inst.components.colouradder:PushColour("lunge", inst.sg.statemem.flash, inst.sg.statemem.flash, 0, 0)
            end
        end,

        timeline =
        {
            TimeEvent(12 * FRAMES, function(inst)
                inst.components.bloomer:PopBloom("lunge")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.bloomer:PopBloom("lunge")
            inst.components.colouradder:PopColour("lunge")
        end,
    })
	
	table.insert(states, State {
        name = "combat_leap_start",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(anims.leap_pre or anims.generic)

            local weapon = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if weapon ~= nil and weapon.components.aoetargeting ~= nil and weapon.components.aoetargeting.targetprefab ~= nil then
                local buffaction = inst:GetBufferedAction()
                if buffaction ~= nil and buffaction.pos ~= nil then
                    inst.sg.statemem.targetfx = SpawnPrefab(weapon.components.aoetargeting.targetprefab)
                    if inst.sg.statemem.targetfx ~= nil then
                        inst.sg.statemem.targetfx.Transform:SetPosition(buffaction:GetActionPoint():Get())
                        inst.sg.statemem.targetfx:ListenForEvent("onremove", OnRemoveCleanupTargetFX, inst)
                    end
                end
            end
        end,

        events =
        {
            EventHandler("combat_leap", function(inst, data)
                inst.sg.statemem.leap = true
                inst.sg:GoToState("combat_leap", {
                    targetfx = inst.sg.statemem.targetfx,
                    data = data,
                })
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.AnimState:IsCurrentAnimation(anims.leap_pre or anims.generic) then
                        inst.AnimState:PlayAnimation(anims.leap_loop or anims.generic, true)
                        inst:PerformBufferedAction()
                    else
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.leap and inst.sg.statemem.targetfx ~= nil and inst.sg.statemem.targetfx:IsValid() then
                OnRemoveCleanupTargetFX(inst)
            end
        end,
    })
	
	table.insert(states, State {
         name = "combat_leap",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nopredict", "nomorph" },

        onenter = function(inst, data)
            if data ~= nil then
                inst.sg.statemem.targetfx = data.targetfx
                data = data.data
                if data ~= nil and
                    data.targetpos ~= nil and
                    data.weapon ~= nil and
                    data.weapon.components.aoeweapon_leap ~= nil and
                    inst.AnimState:IsCurrentAnimation(anims.leap_loop or anims.generic) then
                    ToggleOffPhysics(inst)
                    inst.AnimState:PlayAnimation(anims.leap_pst or anims.generic)
                    inst.SoundEmitter:PlaySound("dontstarve/common/deathpoof")
                    inst.sg.statemem.startingpos = inst:GetPosition()
                    inst.sg.statemem.weapon = data.weapon
                    inst.sg.statemem.targetpos = data.targetpos
                    inst.sg.statemem.flash = 0
                    if inst.sg.statemem.startingpos.x ~= data.targetpos.x or inst.sg.statemem.startingpos.z ~= data.targetpos.z then
                        inst:ForceFacePoint(data.targetpos:Get())
                        inst.Physics:SetMotorVel(math.sqrt(distsq(inst.sg.statemem.startingpos.x, inst.sg.statemem.startingpos.z, data.targetpos.x, data.targetpos.z)) / (12 * FRAMES), 0 ,0)
                    end
                    return
                end
            end
            --Failed
            inst.sg:GoToState("idle")
        end,

        onupdate = function(inst)
            if inst.sg.statemem.flash and inst.sg.statemem.flash > 0 then
                inst.sg.statemem.flash = math.max(0, inst.sg.statemem.flash - .1)
                local c = math.min(1, inst.sg.statemem.flash)
                inst.components.colouradder:PushColour("leap", c, c, 0, 0)
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                if inst.sg.statemem.targetfx ~= nil then
                    if inst.sg.statemem.targetfx:IsValid() then
                        OnRemoveCleanupTargetFX(inst)
                    end
                    inst.sg.statemem.targetfx = nil
                end
            end),
            TimeEvent(10 * FRAMES, function(inst)
                inst.components.colouradder:PushColour("leap", .1, .1, 0, 0)
            end),
            TimeEvent(11 * FRAMES, function(inst)
                inst.components.colouradder:PushColour("leap", .2, .2, 0, 0)
            end),
            TimeEvent(12 * FRAMES, function(inst)
                inst.components.colouradder:PushColour("leap", .4, .4, 0, 0)
                ToggleOnPhysics(inst)
                inst.Physics:Stop()
                inst.Physics:SetMotorVel(0, 0, 0)
                inst.Physics:Teleport(inst.sg.statemem.targetpos.x, 0, inst.sg.statemem.targetpos.z)
            end),
            TimeEvent(13 * FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .015, .8, inst, 20)
                inst.components.bloomer:PushBloom("leap", "shaders/anim.ksh", -2)
                inst.components.colouradder:PushColour("leap", 1, 1, 0, 0)
                inst.sg.statemem.flash = 1.3
                inst.sg:RemoveStateTag("nointerrupt")
                
            end),
            TimeEvent(25 * FRAMES, function(inst)
                inst.components.bloomer:PopBloom("leap")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					if inst.sg.statemem.weapon:IsValid() then
						ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .015, .8, inst, 20)
						inst.sg.statemem.weapon.components.aoeweapon_leap:DoLeap(inst, inst.sg.statemem.startingpos, inst.sg.statemem.targetpos)
					end
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
                inst.Physics:Stop()
                inst.Physics:SetMotorVel(0, 0, 0)
                local x, y, z = inst.Transform:GetWorldPosition()
                if TheWorld.Map:IsPassableAtPoint(x, 0, z) and not TheWorld.Map:IsGroundTargetBlocked(Vector3(x, 0, z)) then
                    inst.Physics:Teleport(x, 0, z)
                else
                    inst.Physics:Teleport(inst.sg.statemem.targetpos.x, 0, inst.sg.statemem.targetpos.z)
                end
            end
            inst.components.bloomer:PopBloom("leap")
            inst.components.colouradder:PopColour("leap")
            if inst.sg.statemem.targetfx ~= nil and inst.sg.statemem.targetfx:IsValid() then
                OnRemoveCleanupTargetFX(inst)
            end
        end,
    })
	
	table.insert(states, State {
        name = "combat_superjump_start",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(anims.superjump_pre or anims.generic)

            local weapon = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if weapon ~= nil and weapon.components.aoetargeting ~= nil and weapon.components.aoetargeting.targetprefab ~= nil then
                local buffaction = inst:GetBufferedAction()
                if buffaction ~= nil and buffaction.pos ~= nil then
                    inst.sg.statemem.targetfx = SpawnPrefab(weapon.components.aoetargeting.targetprefab)
                    if inst.sg.statemem.targetfx ~= nil then
                        inst.sg.statemem.targetfx.Transform:SetPosition(buffaction:GetActionPoint():Get())
                        inst.sg.statemem.targetfx:ListenForEvent("onremove", OnRemoveCleanupTargetFX, inst)
                    end
                end
            end
        end,

        events =
        {
            EventHandler("combat_superjump", function(inst, data)
                inst.sg.statemem.superjump = true
                inst.sg:GoToState("combat_superjump", {
                    targetfx = inst.sg.statemem.targetfx,
                    data = data,
                })
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.AnimState:IsCurrentAnimation(anims.superjump_pre or anims.generic) then
                        inst.AnimState:PlayAnimation(anims.superjump_loop or anims.generic, true)
                        inst:PerformBufferedAction()
                    else
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.superjump and inst.sg.statemem.targetfx ~= nil and inst.sg.statemem.targetfx:IsValid() then
                OnRemoveCleanupTargetFX(inst)
            end
        end,
    })
	
	table.insert(states, State {
        name = "combat_superjump",
        tags = { "aoe", "doing", "busy", "nointerrupt", "nopredict", "nomorph" },

        onenter = function(inst, data)
            if data ~= nil then
                inst.sg.statemem.targetfx = data.targetfx
                inst.sg.statemem.data = data
                data = data.data
                if data ~= nil and
                    data.targetpos ~= nil and
                    data.weapon ~= nil and
                    data.weapon.components.aoeweapon_leap ~= nil and
                    inst.AnimState:IsCurrentAnimation(anims.superjump_loop or anims.generic) then
                    ToggleOffPhysics(inst)
                    inst.AnimState:PlayAnimation(anims.superjump_loop or anims.generic)
                    inst.components.colouradder:PushColour("superjump", .1, .1, .1, 0)
                    inst.sg.statemem.data.startingpos = inst:GetPosition()
                    inst.sg.statemem.weapon = data.weapon
                    if inst.sg.statemem.data.startingpos.x ~= data.targetpos.x or inst.sg.statemem.data.startingpos.z ~= data.targetpos.z then
                        inst:ForceFacePoint(data.targetpos:Get())
                    end
                    inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt", nil, .4)
                    inst.SoundEmitter:PlaySound("dontstarve/common/deathpoof")
                    inst.sg:SetTimeout(1)
                    return
                end
            end
            --Failed
            inst.sg:GoToState("idle", true)
        end,

        onupdate = function(inst)
            if inst.sg.statemem.dalpha ~= nil and inst.sg.statemem.alpha > 0 then
                inst.sg.statemem.dalpha = math.max(.1, inst.sg.statemem.dalpha - .1)
                inst.sg.statemem.alpha = math.max(0, inst.sg.statemem.alpha - inst.sg.statemem.dalpha)
            end
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.sg:AddStateTag("noattack")
                inst.components.colouradder:PushColour("superjump", .3, .3, .2, 0)
                inst:PushEvent("dropallaggro")
                if inst.sg.statemem.weapon ~= nil and inst.sg.statemem.weapon:IsValid() then
                    inst.sg.statemem.weapon:PushEvent("superjumpstarted", inst)
                end
            end),
            TimeEvent(2 * FRAMES, function(inst)
                inst.AnimState:SetMultColour(0, 0, 0, 1)
                inst.components.colouradder:PushColour("superjump", .6, .6, .4, 0)
            end),
            TimeEvent(3 * FRAMES, function(inst)
                inst.sg.statemem.alpha = 1
                inst.sg.statemem.dalpha = .5
            end),
            TimeEvent(1 - 7 * FRAMES, function(inst)
                if inst.sg.statemem.targetfx ~= nil then
                    if inst.sg.statemem.targetfx:IsValid() then
                        OnRemoveCleanupTargetFX(inst)
                    end
                    inst.sg.statemem.targetfx = nil
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst:Hide()
                    inst.Physics:Teleport(inst.sg.statemem.data.data.targetpos.x, 0, inst.sg.statemem.data.data.targetpos.z)
                end
            end),
        },

        ontimeout = function(inst)
            inst.sg.statemem.superjump = true
            inst.sg.statemem.data.isphysicstoggle = inst.sg.statemem.data.isphysicstoggle
            inst.sg.statemem.data.targetfx = nil
            inst.sg:GoToState("combat_superjump_pst", inst.sg.statemem.data)
        end,

        onexit = function(inst)
            if not inst.sg.statemem.superjump then
                inst.components.health:SetInvincible(false)
                if inst.sg.statemem.isphysicstoggle then
                    ToggleOnPhysics(inst)
                end
                inst.components.colouradder:PopColour("superjump")
                if inst.sg.statemem.weapon ~= nil and inst.sg.statemem.weapon:IsValid() then
                    inst.sg.statemem.weapon:PushEvent("superjumpcancelled", inst)
                end
            end
            if inst.sg.statemem.targetfx ~= nil and inst.sg.statemem.targetfx:IsValid() then
                OnRemoveCleanupTargetFX(inst)
            end
            inst:Show()
        end,
    })
	
	table.insert(states, State {
        name = "parry_pre",
        tags = { "preparrying", "busy", "nomorph" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation(anims.parry_pre or anims.generic)
            inst.AnimState:PushAnimation(anims.parry_loop or anims.generic, true)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
            --V2C: using animover results in a slight hang on last frame of parry_pre

            local function oncombatparry(inst, data)
                inst.sg:AddStateTag("parrying")
                if data ~= nil then
                    if data.direction ~= nil then
                        inst.Transform:SetRotation(data.direction)
                    end
                    inst.sg.statemem.parrytime = data.duration
                    inst.sg.statemem.item = data.weapon
                    if data.weapon ~= nil then
                        inst.components.combat.redirectdamagefn = function(inst, attacker, damage, weapon, stimuli)
                            return IsWeaponEquipped(inst, data.weapon)
                                and data.weapon.components.parryweapon ~= nil
                                and data.weapon.components.parryweapon:TryParry(inst, attacker, damage, weapon, stimuli)
                                and data.weapon
                                or nil
                        end
                    end
                end
            end
            --V2C: using EventHandler will result in a frame delay, but we want this to trigger
            --     immediately during PerformBufferedAction()
            inst:ListenForEvent("combat_parry", oncombatparry)
            inst:PerformBufferedAction()
            inst:RemoveEventCallback("combat_parry", oncombatparry)
        end,

        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst)
                if inst.sg.statemem.item ~= nil and
                    inst.sg.statemem.item.components.parryweapon ~= nil and
                    inst.sg.statemem.item:IsValid() then
                    --This is purely for stategraph animation sfx, can actually be bypassed!
                    inst.sg.statemem.item.components.parryweapon:OnPreParry(inst)
                end
            end),
        },

        events =
        {
            EventHandler("unequip", function(inst, data)
                -- We need to handle this because the default unequip
                -- handler is ignored while we are in a "busy" state.
                inst.sg:GoToState("idle")
            end),
        },

        ontimeout = function(inst)
            if inst.sg:HasStateTag("parrying") then
                inst.sg.statemem.parrying = true
                --Transfer talk task to parry_idle state
                local talktask = inst.sg.statemem.talktask
                inst.sg.statemem.talktask = nil
                inst.sg:GoToState("parry_idle", { duration = inst.sg.statemem.parrytime, pauseframes = 30, talktask = talktask })
            else
                inst.AnimState:PlayAnimation(anims.parry_pst or anims.generic)
                inst.sg:GoToState("idle", true)
            end
        end,

        onexit = function(inst)
            if inst.sg.statemem.talktask ~= nil then
                inst.sg.statemem.talktask:Cancel()
                inst.sg.statemem.talktask = nil
                StopTalkSound(inst)
            end
            if not inst.sg.statemem.parrying then
                inst.components.combat.redirectdamagefn = nil
            end
        end,
    })
	
	table.insert(states, State {
         name = "parry_idle",
        tags = { "notalking", "parrying", "nomorph" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()

            if data ~= nil and data.duration ~= nil then
                if data.duration > 0 then
                    inst.sg.statemem.task = inst:DoTaskInTime(data.duration, function(inst)
                        inst.sg.statemem.task = nil
                        inst.AnimState:PlayAnimation(anims.parry_pst or anims.generic)
                        inst.sg:GoToState("idle")
                    end)
                else
                    inst.AnimState:PlayAnimation(anims.parry_pst or anims.generic)
                    inst.sg:GoToState("idle", true)
                    return
                end
            end

            if not inst.AnimState:IsCurrentAnimation(anims.parry_loop or anims.generic) then
                inst.AnimState:PushAnimation(anims.parry_loop or anims.generic, true)
            end

            --Transferred over from parry_pre so it doesn't cut off abrubtly
            inst.sg.statemem.talktask = data ~= nil and data.talktask or nil

            if data ~= nil and (data.pauseframes or 0) > 0 then
                inst.sg:AddStateTag("busy")
                inst.sg:AddStateTag("pausepredict")

                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:RemotePausePrediction(data.pauseframes <= 7 and data.pauseframes or nil)
                end
                inst.sg:SetTimeout(data.pauseframes * FRAMES)
            else
                inst.sg:AddStateTag("idle")
            end
        end,

        ontimeout = function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("pausepredict")
            inst.sg:AddStateTag("idle")
        end,

        events =
        {
            EventHandler("unequip", function(inst, data)
                if not inst.sg:HasStateTag("idle") then
                    -- We need to handle this because the default unequip
                    -- handler is ignored while we are in a "busy" state.
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.task ~= nil then
                inst.sg.statemem.task:Cancel()
                inst.sg.statemem.task = nil
            end
            if inst.sg.statemem.talktask ~= nil then
                inst.sg.statemem.talktask:Cancel()
                inst.sg.statemem.talktask = nil
                StopTalkSound(inst)
            end
            if not inst.sg.statemem.parrying then
                inst.components.combat.redirectdamagefn = nil
            end
        end,
    })
	
	table.insert(states, State {
        name = "parry_knockback",
        tags = { "parrying", "parryhit", "busy", "nopredict", "nomorph" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation(anims.parry_loop or anims.generic)
            inst.SoundEmitter:PlaySound("dontstarve/wilson/hit")

            if data ~= nil then
                if data.timeleft ~= nil then
                    inst.sg.statemem.timeleft0 = GetTime()
                    inst.sg.statemem.timeleft = data.timeleft
                end
                data = data.knockbackdata
                if data ~= nil and data.radius ~= nil and data.knocker ~= nil and data.knocker:IsValid() then
                    local x, y, z = data.knocker.Transform:GetWorldPosition()
                    local distsq = inst:GetDistanceSqToPoint(x, y, z)
                    local rangesq = data.radius * data.radius
                    local rot = inst.Transform:GetRotation()
                    local rot1 = distsq > 0 and inst:GetAngleToPoint(x, y, z) or data.knocker.Transform:GetRotation() + 180
                    local drot = math.abs(rot - rot1)
                    while drot > 180 do
                        drot = math.abs(drot - 360)
                    end
                    local k = distsq < rangesq and .3 * distsq / rangesq - 1 or -.7
                    inst.sg.statemem.speed = (data.strengthmult or 1) * 12 * k
                    if drot > 90 then
                        inst.sg.statemem.reverse = true
                        inst.Transform:SetRotation(rot1 + 180)
                        inst.Physics:SetMotorVel(-inst.sg.statemem.speed, 0, 0)
                    else
                        inst.Transform:SetRotation(rot1)
                        inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
                    end
                end
            end

            inst.sg:SetTimeout(6 * FRAMES)
        end,

        onupdate = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.sg.statemem.speed = .75 * inst.sg.statemem.speed
                inst.Physics:SetMotorVel(inst.sg.statemem.reverse and -inst.sg.statemem.speed or inst.sg.statemem.speed, 0, 0)
            end
        end,

        events =
        {
            EventHandler("unequip", function(inst, data)
                -- We need to handle this because the default unequip
                -- handler is ignored while we are in a "busy" state.
                inst.sg.statemem.unequipped = true
            end),
        },

        ontimeout = function(inst)
            if inst.sg.statemem.unequipped then
                inst.sg:GoToState("idle")
            else
                inst.sg.statemem.parrying = true
                inst.sg:GoToState("parry_idle", inst.sg.statemem.timeleft ~= nil and { duration = math.max(0, inst.sg.statemem.timeleft + inst.sg.statemem.timeleft0 - GetTime()) } or nil)
            end
        end,

        onexit = function(inst)
            if inst.sg.statemem.speed ~= nil then
                inst.Physics:Stop()
            end
            if not inst.sg.statemem.parrying then
                inst.components.combat.redirectdamagefn = nil
            end
        end,
    })
	
	table.insert(states, State {
        name = "combat_superjump_pst",
        tags = { "aoe", "doing", "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst, data)
            if data ~= nil and data.data ~= nil then
                inst.sg.statemem.startingpos = data.startingpos
                inst.sg.statemem.isphysicstoggle = data.isphysicstoggle
                data = data.data
                inst.sg.statemem.weapon = data.weapon
                if inst.sg.statemem.startingpos ~= nil and
                    data.targetpos ~= nil and
                    data.weapon ~= nil and
                    data.weapon.components.aoeweapon_leap ~= nil and
                    inst.AnimState:IsCurrentAnimation(anims.superjump_loop or anims.generic) then
                    inst.AnimState:PlayAnimation(anims.superjump_pst or anims.generic)
                    inst.sg.statemem.targetpos = data.targetpos
                    inst.sg.statemem.flash = 0
                    if not inst.sg.statemem.isphysicstoggle then
                        ToggleOffPhysics(inst)
                    end
                    inst.Physics:Teleport(data.targetpos.x, 0, data.targetpos.z)
                    inst.components.health:SetInvincible(true)
                    inst.sg:SetTimeout(22 * FRAMES)
                    return
                end
            end
            --Failed
            inst.sg:GoToState("idle", true)
        end,

        onupdate = function(inst)
            if inst.sg.statemem.flash > 0 then
                inst.sg.statemem.flash = math.max(0, inst.sg.statemem.flash - .1)
                local c = math.min(1, inst.sg.statemem.flash)
                inst.components.colouradder:PushColour("superjump", c, c, 0, 0)
            end
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_weapon")
                inst.AnimState:SetMultColour(.7, .7, .7, .7)
                inst.components.colouradder:PushColour("superjump", .1, .1, 0, 0)
            end),
            TimeEvent(2 * FRAMES, function(inst)
                inst.AnimState:SetMultColour(.9, .9, .9, .9)
                inst.components.colouradder:PushColour("superjump", .2, .2, 0, 0)
            end),
            TimeEvent(3 * FRAMES, function(inst)
                inst.AnimState:SetMultColour(1, 1, 1, 1)
                inst.components.colouradder:PushColour("superjump", .4, .4, 0, 0)
                inst.DynamicShadow:Enable(true)
            end),
            TimeEvent(4 * FRAMES, function(inst)
                inst.components.colouradder:PushColour("superjump", 1, 1, 0, 0)
                inst.components.bloomer:PushBloom("superjump", "shaders/anim.ksh", -2)
                ToggleOnPhysics(inst)
                ShakeAllCameras(CAMERASHAKE.VERTICAL, .7, .015, .8, inst, 20)
                inst.sg.statemem.flash = 1.3
                inst.sg:RemoveStateTag("noattack")
                inst.components.health:SetInvincible(false)
                if inst.sg.statemem.weapon:IsValid() then
                    inst.sg.statemem.weapon.components.aoeweapon_leap:DoLeap(inst, inst.sg.statemem.startingpos, inst.sg.statemem.targetpos)
                    inst.sg.statemem.weapon = nil
                end
            end),
            TimeEvent(8 * FRAMES, function(inst)
                inst.components.bloomer:PopBloom("superjump")
            end),
            TimeEvent(19 * FRAMES, PlayFootstep),
        },

        ontimeout = function(inst)
            inst.sg:GoToState("idle", true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end
            inst.DynamicShadow:Enable(true)
            inst.components.health:SetInvincible(false)
            inst.components.bloomer:PopBloom("superjump")
            inst.components.colouradder:PopColour("superjump")
            if inst.sg.statemem.weapon ~= nil and inst.sg.statemem.weapon:IsValid() then
                inst.sg.statemem.weapon:PushEvent("superjumpcancelled", inst)
            end
        end,
    })
	
	table.insert(states, State {
        name = "castspell",
        tags = { "doing", "busy", "canrotate" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end
            inst.AnimState:PlayAnimation(anims.staff_pre or anims.generic)
			if anims.staff then
				inst.AnimState:PushAnimation(anims.staff, false)
			end
			if anims.staff_pst then
				inst.AnimState:PushAnimation(anims.staff_pst, false)
			end
            inst.components.locomotor:Stop()

            --Spawn an effect on the player's location
            local staff = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            local colour = staff ~= nil and staff.fxcolour or { 1, 1, 1 }

            inst.sg.statemem.stafffx = SpawnPrefab(inst.components.rider:IsRiding() and "staffcastfx_mount" or "staffcastfx")
            inst.sg.statemem.stafffx.entity:SetParent(inst.entity)
            inst.sg.statemem.stafffx.Transform:SetRotation(inst.Transform:GetRotation())
            inst.sg.statemem.stafffx:SetUp(colour)

            inst.sg.statemem.stafflight = SpawnPrefab("staff_castinglight")
            inst.sg.statemem.stafflight.Transform:SetPosition(inst.Transform:GetWorldPosition())
            inst.sg.statemem.stafflight:SetUp(colour, 1.9, .33)

            if staff ~= nil and staff.components.aoetargeting ~= nil and staff.components.aoetargeting.targetprefab ~= nil then
                local buffaction = inst:GetBufferedAction()
                if buffaction ~= nil and buffaction.pos ~= nil then
                    inst.sg.statemem.targetfx = SpawnPrefab(staff.components.aoetargeting.targetprefab)
                    if inst.sg.statemem.targetfx ~= nil then
                        inst.sg.statemem.targetfx.Transform:SetPosition(buffaction:GetActionPoint():Get())
                        inst.sg.statemem.targetfx:ListenForEvent("onremove", OnRemoveCleanupTargetFX, inst)
                    end
                end
            end

            inst.sg.statemem.castsound = staff ~= nil and staff.castsound or "dontstarve/wilson/use_gemstaff"
        end,

        timeline = timelines and timelines.staff or 
        {
            TimeEvent(anims.castspelltime or 53 * FRAMES, function(inst)
                if inst.sg.statemem.targetfx ~= nil then
                    if inst.sg.statemem.targetfx:IsValid() then
                        OnRemoveCleanupTargetFX(inst)
                    end
                    inst.sg.statemem.targetfx = nil
                end
                inst.sg.statemem.stafffx = nil --Can't be cancelled anymore
                inst.sg.statemem.stafflight = nil --Can't be cancelled anymore
                --V2C: NOTE! if we're teleporting ourself, we may be forced to exit state here!
                inst:PerformBufferedAction()
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            if inst.sg.statemem.stafffx ~= nil and inst.sg.statemem.stafffx:IsValid() then
                inst.sg.statemem.stafffx:Remove()
            end
            if inst.sg.statemem.stafflight ~= nil and inst.sg.statemem.stafflight:IsValid() then
                inst.sg.statemem.stafflight:Remove()
            end
            if inst.sg.statemem.targetfx ~= nil and inst.sg.statemem.targetfx:IsValid() then
                OnRemoveCleanupTargetFX(inst)
            end
        end,
    })
end 

--To easily add new commonhandlers that ALL mobs require
PP_CommonHandlers.AddCommonHandlers = function()
	return EventHandler("oneaten", oneaten), EventHandler("ms_opengift", opengift), EventHandler("itemranout", itemranout)
end

---------------------------------------------------------------------------
--//////////////////////// Action Handlers ///////////////////////////////
---------------------------------------------------------------------------

PP_CommonStates.GetCommonActions = function(longaction, shortaction, workaction, otheraction, ishuman)
function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local master_actionhandlers = {}
for k, v in pairs(ACTIONS) do
	table.insert(master_actionhandlers, ActionHandler(v, shortaction))
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, longaction),
	ActionHandler(ACTIONS.ATTACK, function(inst)
        --TODO any mobs that override attack need to check for their cooldown. 
        if inst.components.combat and not inst.components.combat:InCooldown() then
            return "attack"
        else
            return "idle"
        end
    end),
	--ActionHandler(ACTIONS.PICKUP, workaction),
	ActionHandler(ACTIONS.DROP, longaction),
	ActionHandler(ACTIONS.USEITEM, longaction),
	ActionHandler(ACTIONS.EAT, otheraction),
	ActionHandler(ACTIONS.HEAL, otheraction),
	ActionHandler(ACTIONS.FAN, longaction),
	ActionHandler(ACTIONS.SLEEPIN, "home"),
	ActionHandler(ACTIONS.PICK, workaction),
    ActionHandler(ACTIONS.PICKUP, shortaction),
	ActionHandler(ACTIONS.ACTIVATE, longaction),
	ActionHandler(ACTIONS.FEED, longaction),
	ActionHandler(ACTIONS.DEPLOY, shortaction),
	ActionHandler(ACTIONS.GIVE, longaction),
	ActionHandler(ACTIONS.GIVETOPLAYER, longaction),
	ActionHandler(ACTIONS.GIVEALLTOPLAYER, longaction),
	
	ActionHandler(ACTIONS.TRAVEL, longaction),
	ActionHandler(ACTIONS.LOOKAT, function(inst)
		print("PERFORMING LOOKAT ACTION")
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if target and target.components.writeable and target.components.writeable:IsWritten() then
			return "dotalk"
		else
			return "idle"
		end
	end),
	ActionHandler(ACTIONS.MIGRATE, longaction),
	ActionHandler(ACTIONS.JUMPIN, "jumpin_pre"),
	ActionHandler(ACTIONS.DRAW, shortaction),
	ActionHandler(ACTIONS.DECORATEVASE, longaction),
	--ActionHandler(ACTIONS.CHANGEIN, "openwardobe"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "revivecorpse"),
	ActionHandler(ACTIONS.CASTAOE,
        function(inst, action)
			if inst.noactions then
				return "idle"
			else
				return (action.invobject ~= nil and inst.sg.sg.states.combat_lunge_start ~= nil)
					and (   (action.invobject:HasTag("aoeweapon_lunge") and "combat_lunge_start") or
							(action.invobject:HasTag("aoeweapon_leap") and (action.invobject:HasTag("superjump") and "combat_superjump_start" or "combat_leap_start")) or
							(action.invobject:HasTag("blowdart") and "blowdart_special") or
							(action.invobject:HasTag("throw_line") and "throw_line") or
							(action.invobject:HasTag("book") and "book") or
							(action.invobject:HasTag("parryweapon") and "parry_pre")
						)
					or longaction
			end		
        end),
}


local mobCraftActions =
{
	ActionHandler(ACTIONS.GOHOME, "gohome"),
	ActionHandler(ACTIONS.WRITE, longaction),
	ActionHandler(ACTIONS.ATTACK, "attack"),
	ActionHandler(ACTIONS.FEED, longaction),
	--ActionHandler(ACTIONS.PICKUP, shortaction),
	ActionHandler(ACTIONS.JUMPIN, "jumpin_pre"),
	ActionHandler(ACTIONS.PICK, workaction),
	ActionHandler(ACTIONS.DROP, longaction),
	ActionHandler(ACTIONS.ACTIVATE, longaction),
	ActionHandler(ACTIONS.SLEEPIN, "home"),
	ActionHandler(ACTIONS.EAT, otheraction),
	ActionHandler(ACTIONS.HEAL, otheraction),
	ActionHandler(ACTIONS.FAN, longaction),
	ActionHandler(ACTIONS.DIG, shortaction),
	ActionHandler(ACTIONS.CHOP, workaction),
	ActionHandler(ACTIONS.MINE, workaction),
	ActionHandler(ACTIONS.GIVE, longaction),
	ActionHandler(ACTIONS.GIVEALLTOPLAYER, longaction),
	ActionHandler(ACTIONS.COOK, longaction),
	ActionHandler(ACTIONS.FILL, longaction),
	ActionHandler(ACTIONS.DRY, longaction),
	ActionHandler(ACTIONS.ADDFUEL, longaction),
	ActionHandler(ACTIONS.ADDWETFUEL, longaction),
	ActionHandler(ACTIONS.LIGHT, longaction),
	ActionHandler(ACTIONS.BAIT, longaction),
	ActionHandler(ACTIONS.BUILD, shortaction),
	ActionHandler(ACTIONS.PLANT, shortaction),
	ActionHandler(ACTIONS.REPAIR, longaction),
	ActionHandler(ACTIONS.HARVEST, longaction),
	ActionHandler(ACTIONS.STORE, shortaction),
	ActionHandler(ACTIONS.RUMMAGE, shortaction),
	ActionHandler(ACTIONS.DEPLOY, shortaction),
	ActionHandler(ACTIONS.HAMMER, workaction),
	ActionHandler(ACTIONS.FERTILIZE, shortaction),
	ActionHandler(ACTIONS.MURDER, workaction),
	ActionHandler(ACTIONS.UNLOCK, longaction),
	ActionHandler(ACTIONS.TURNOFF, shortaction),
	ActionHandler(ACTIONS.TURNON, shortaction),
	ActionHandler(ACTIONS.SEW, shortaction),
	ActionHandler(ACTIONS.COMBINESTACK, shortaction),
	ActionHandler(ACTIONS.UPGRADE, longaction),
	ActionHandler(ACTIONS.WRITE, longaction),
	ActionHandler(ACTIONS.FEEDPLAYER, longaction),
	ActionHandler(ACTIONS.TERRAFORM, shortaction),
	ActionHandler(ACTIONS.NET, workaction),
	ActionHandler(ACTIONS.CHECKTRAP, longaction),
	ActionHandler(ACTIONS.SHAVE, longaction),
	ActionHandler(ACTIONS.FISH, "fishing_pre"),
	ActionHandler(ACTIONS.REEL, longaction),
	ActionHandler(ACTIONS.CATCH, longaction),
	ActionHandler(ACTIONS.TEACH, longaction),
	ActionHandler(ACTIONS.MANUALEXTINGUISH, longaction),
	ActionHandler(ACTIONS.RESETMINE, longaction),
	ActionHandler(ACTIONS.BLINK, "quicktele"),
	ActionHandler(ACTIONS.CHANGEIN, longaction),
	ActionHandler(ACTIONS.SMOTHER, longaction),
	ActionHandler(ACTIONS.CASTSPELL, longaction),
	ActionHandler(ACTIONS.WRAPBUNDLE, "action_loop"),
	ActionHandler(ACTIONS.UNWRAP, longaction),
	ActionHandler(ACTIONS.UNLOCK, longaction),
	ActionHandler(ACTIONS.DRAW, longaction),
	ActionHandler(ACTIONS.DECORATEVASE, longaction),
	ActionHandler(ACTIONS.USEKLAUSSACKKEY, longaction),
	ActionHandler(ACTIONS.TAKEITEM, longaction),
	ActionHandler(ACTIONS.TOSS, workaction),
    ActionHandler(ACTIONS.UNPIN, shortaction),
	ActionHandler(ACTIONS.STARTCHANNELING, "startchanneling"),
	ActionHandler(ACTIONS.HALLOWEENMOONMUTATE, shortaction),
	ActionHandler(ACTIONS.ATTUNE, longaction),
	ActionHandler(ACTIONS.PLAY, longaction),
	ActionHandler(ACTIONS.BUNDLE, "bundle"),
	ActionHandler(ACTIONS.APPLYPRESERVATIVE, shortaction),
	ActionHandler(ACTIONS.CONSTRUCT,
        function(inst, action)
            return (action.target == nil or action.target.components.constructionsite == nil) and "startconstruct" or "construct"
        end
	),
	ActionHandler(ACTIONS.CASTSUMMON, shortaction),
    ActionHandler(ACTIONS.CASTUNSUMMON, shortaction),
    ActionHandler(ACTIONS.COMMUNEWITHSUMMONED, shortaction),
	ActionHandler(ACTIONS.COMPARE_WEIGHABLE, shortaction),
    ActionHandler(ACTIONS.WEIGH_ITEM, shortaction),
    ActionHandler(ACTIONS.GIVE_TACKLESKETCH, shortaction),
    ActionHandler(ACTIONS.REMOVE_FROM_TROPHYSCALE, shortaction),
    ActionHandler(ACTIONS.CYCLE, shortaction),
    ActionHandler(ACTIONS.OCEAN_TOSS, shortaction),
	ActionHandler(ACTIONS.TELEPORT, shortaction),
	ActionHandler(ACTIONS.CREATE, shortaction),
	
	--Forge--
	ActionHandler(ACTIONS.REVIVE_CORPSE, "revivecorpse"),
	ActionHandler(ACTIONS.CASTAOE,
        function(inst, action)
			if inst.noactions then
				return "idle"
			else
				return (action.invobject ~= nil and inst.sg.sg.states.combat_lunge_start ~= nil)
					and (   (action.invobject:HasTag("aoeweapon_lunge") and "combat_lunge_start") or
							(action.invobject:HasTag("aoeweapon_leap") and (action.invobject:HasTag("superjump") and "combat_superjump_start" or "combat_leap_start")) or
							(action.invobject:HasTag("blowdart") and longaction) or
							(action.invobject:HasTag("throw_line") and longaction) or
							(action.invobject:HasTag("book") and longaction) or
							(action.invobject:HasTag("parryweapon") and "parry_pre")
						)
					or longaction
			end		
        end),
	
	--Gorge
    ActionHandler(ACTIONS.TILL, longaction),
    ActionHandler(ACTIONS.PLANTSOIL, longaction),
    ActionHandler(ACTIONS.INSTALL, longaction),
    ActionHandler(ACTIONS.TAPTREE, longaction),
    ActionHandler(ACTIONS.SLAUGHTER, longaction),
    ActionHandler(ACTIONS.REPLATE, longaction),
    ActionHandler(ACTIONS.SALT, longaction),
	
	--RoT--	
	ActionHandler(ACTIONS.BREAK, longaction),
	ActionHandler(ACTIONS.RAISE_SAIL, longaction),
	ActionHandler(ACTIONS.LOWER_SAIL_BOOST, function(inst) 
		return (inst.noactions or inst._isflying) and "idle" or "furl"
	end),
    ActionHandler(ACTIONS.RAISE_ANCHOR, longaction),
    ActionHandler(ACTIONS.LOWER_ANCHOR, longaction),  
    ActionHandler(ACTIONS.REPAIR_LEAK, longaction),
    ActionHandler(ACTIONS.STEER_BOAT, function(inst) 
		return (inst.noactions or inst._isflying) and "idle" or "steer_boat_idle_pre"
	end),
    ActionHandler(ACTIONS.STOP_STEERING_BOAT, "stop_steering"),
    ActionHandler(ACTIONS.ROW_FAIL, "row_fail"),
    ActionHandler(ACTIONS.ROW, function(inst) 
		return (inst.noactions or inst._isflying) and "idle" or "row"
	end),
    ActionHandler(ACTIONS.EXTEND_PLANK, shortaction),
    ActionHandler(ACTIONS.RETRACT_PLANK, shortaction),
    ActionHandler(ACTIONS.ABANDON_SHIP, "abandon_ship"),
    ActionHandler(ACTIONS.MOUNT_PLANK, "mount_plank"),
    ActionHandler(ACTIONS.DISMOUNT_PLANK, shortaction),    
    ActionHandler(ACTIONS.SET_HEADING, "steer_boat_turning"),
    ActionHandler(ACTIONS.CAST_NET, longaction),
	ActionHandler(ACTIONS.BATHBOMB, shortaction),
	ActionHandler(ACTIONS.APPLYPRESERVATIVE, shortaction),
	ActionHandler(ACTIONS.COMPARE_WEIGHABLE, shortaction),
	ActionHandler(ACTIONS.WEIGH_ITEM, "use_pocket_scale"),
	ActionHandler(ACTIONS.GIVE_TACKLESKETCH, shortaction),
	ActionHandler(ACTIONS.REMOVE_FROM_TROPHYSCALE, shortaction),
	ActionHandler(ACTIONS.START_CARRAT_RACE, shortaction),
	
	ActionHandler(ACTIONS.FISH_OCEAN, "fishing_ocean_pre"),
    ActionHandler(ACTIONS.OCEAN_FISHING_POND, "fishing_ocean_pre"),
    ActionHandler(ACTIONS.OCEAN_FISHING_CAST, "oceanfishing_cast"),
    ActionHandler(ACTIONS.OCEAN_FISHING_REEL, 
		function(inst, action)
			local fishable = action.invobject ~= nil and action.invobject.components.oceanfishingrod.target or nil
			if fishable ~= nil and fishable.components.oceanfishable ~= nil and fishable:HasTag("partiallyhooked") then
				return "oceanfishing_sethook"
			elseif inst:HasTag("fishing_idle") and not (inst.sg:HasStateTag("reeling") and not inst.sg.statemem.allow_repeat) then
				return "oceanfishing_reel"
			end
			return nil
		end),
	ActionHandler(ACTIONS.CYCLE, shortaction),
	ActionHandler(ACTIONS.OCEAN_TOSS, shortaction),
	ActionHandler(ACTIONS.BEGIN_QUEST, shortaction),
    ActionHandler(ACTIONS.ABANDON_QUEST, shortaction),
	ActionHandler(ACTIONS.POUR_WATER, shortaction),
    ActionHandler(ACTIONS.POUR_WATER_GROUNDTILE, shortaction),
	ActionHandler(ACTIONS.INTERACT_WITH, shortaction),
	ActionHandler(ACTIONS.PLANTREGISTRY_RESEARCH_FAIL, shortaction),
    ActionHandler(ACTIONS.PLANTREGISTRY_RESEARCH, shortaction),
    ActionHandler(ACTIONS.ASSESSPLANTHAPPINESS, shortaction),
	ActionHandler(ACTIONS.DEPLOY_TILEARRIVE, shortaction),
	ActionHandler(ACTIONS.ADDCOMPOSTABLE, shortaction),
    ActionHandler(ACTIONS.WAX, shortaction),
	ActionHandler(ACTIONS.READ, shortaction),
	
	ActionHandler(ACTIONS.USEITEMON, shortaction),
    ActionHandler(ACTIONS.STOPUSINGITEM, shortaction),
    ActionHandler(ACTIONS.YOTB_STARTCONTEST, shortaction),
    ActionHandler(ACTIONS.YOTB_UNLOCKSKIN, shortaction),
    ActionHandler(ACTIONS.YOTB_SEW, shortaction),
	ActionHandler(ACTIONS.CHANGEIN, "usewardrobe"),
    ActionHandler(ACTIONS.HITCHUP, "usewardrobe"),
    ActionHandler(ACTIONS.UNHITCH, "usewardrobe"),
    ActionHandler(ACTIONS.MARK, shortaction),

    ActionHandler(ACTIONS.PERFORM, function(inst, action)
        inst:PerformBufferedAction()
        return "acting_idle"
    end),
	
	ActionHandler(ACTIONS.CARNIVALGAME_FEED, shortaction),
	ActionHandler(ACTIONS.APPLYPRESERVATIVE, shortaction),
	ActionHandler(ACTIONS.ADVANCE_TREE_GROWTH, shortaction),
    ActionHandler(ACTIONS.INVESTIGATE, shortaction),
	--Custom
	--ActionHandler(ACTIONS.MOBSLEEPIN, shortaction),
}

--for i, v in ipairs(mobCraftActions) do
	--table.insert(v, master_actionhandlers)
--end
mobCraftActions = TableConcat(master_actionhandlers, mobCraftActions)

local extraActions = 
{

}

if MOBCRAFT == "Enable" or ishuman then
	extraActions = mobCraftActions
end

local modded_actions = {
    --hamlet
    HACK        = workaction,
    DIG_HOLE    = workaction,
    EMBARK      = "embark",
    DISEMBARK   = "disembark",
}

for k, v in pairs(modded_actions) do
    if ACTIONS[k] then
        table.insert(mobCraftActions, ActionHandler(ACTIONS[k], v))
    end
end

if ACTIONS.DISEMBARK then
    ACTIONS.DISEMBARK.distance = 3
end

actionhandlers = TableConcat(actionhandlers, extraActions)


return actionhandlers
end