return {
    --This is basically a common abilities sort of thing.
    --don't modify these after setting them, just make custom ones.
    PP_SLEEP = {
        name = "pp_sleep", --internal name. This would get sent to the replica to access string/icon data in tuning.
        fname = "Sleep", --Probably not needed if I'm doing the above.
        state = nil, --The state to go to. Do nil if activatefn determines the work.
        activatefn = function(inst, cursor_pos)
            if inst.sg:HasStateTag("sleeping") and not inst._forcedsleep then
                inst.sg:GoToState("wake")	
            else
                inst.sg:GoToState("sleep")
            end
        end, --Runs on activate.
        validfn = function(inst, cursor_pos)
            return not inst._forcedsleep and (inst.sg:HasStateTag("sleeping") or not inst.sg:HasStateTag("busy"))
        end,
        cooldown = 0,
        enabled = true,
    },
    SKILL1 = {
        name = "skill1", 
        fname = "Taunt", 
        tags = {},
        activatefn = function(inst, cursor_pos)
            if inst.OnSkill1 then
                inst:OnSkill1(cursor_pos)
            else
                inst.sg:GoToState("special_atk1", cursor_pos)
            end
        end,
        validfn = function(inst, cursor_pos)
            return not inst.sg:HasStateTag("busy")
        end,
        attack = false,
        cooldown = 0,
        enabled = true,
    },
    SKILL2 = {
        name = "skill2", 
        fname = "Special Attack", 
        tags = {},
        activatefn = function(inst, cursor_pos)
            print(cursor_pos)
            if inst.OnSkill2 then
                inst:OnSkill2(cursor_pos)
            else
                inst.sg:GoToState("special_atk2", cursor_pos)
            end
        end,
        validfn = function(inst, cursor_pos)
            return not inst.sg:HasStateTag("busy")
        end,
        attack = false, 
        cooldown = 0, 
        enabled = true,
    },
    SKILL3 = {
        name = "skill3", 
        fname = "Special Attack", 
        tags = {},
        activatefn = function(inst, cursor_pos)
            if inst.OnSkill3 then
                inst:OnSkill3(cursor_pos)
            else
                inst.sg:GoToState("special_atk3", cursor_pos)
            end
        end,
        validfn = function(inst, cursor_pos)
            return not inst.sg:HasStateTag("busy")
        end,
        attack = false, 
        cooldown = 0, 
        enabled = true,
    },
}