local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local Widget = require "widgets/widget"
local Text = require "widgets/text"
local Grid = require "widgets/grid"
local Spinner = require "widgets/spinner"
local UIAnim = require "widgets/uianim"

local TEMPLATES = require "widgets/redux/templates"

require("util")

-------------------------------------------------------------------------------------------------------
local PigNote = Class(Widget, function(self, parent, data)
    Widget._ctor(self, "PigNote")
    self.root = self:AddChild(Widget("root"))
	local tab_root = self.root:AddChild(Widget("tab_root"))
    --ThePlayer:ShowPopUp(POPUPS.pignote, true)
    self.paper_root = self:AddChild(Widget("root"))
    self.paper_root:SetScale(0.6, 0.6)
    self.paperdata = data or {}
    ----------------
    --Base Card
    ----------------
	local page = self.paper_root:AddChild(Image("images/pignote_old.xml", "pignote_old.tex"))

    self.text_root = self:AddChild(Widget("root"))
    local page_text = self.text_root:AddChild(Text("hammerhead", 17, "", UICOLOURS.BROWN_DARK))
    --page_text:SetPosition(60, 200)
    page_text:SetHAlign(ANCHOR_LEFT)
    --page_text:SetRegionSize(500, 50)
    page_text:SetMultilineTruncatedString(data:value(), 25, 389)
    local desc_w, desc_h = page_text:GetRegionSize()
    page_text:SetPosition(-180 + 0.5 * desc_w, 215 - 0.5 * desc_h)
end)

function PigNote:GetHelpText()
    local controller_id = TheInput:GetControllerID()
    local t = {}

    table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_CRAFTING).."/"..TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_INVENTORY).. " " .. STRINGS.UI.HELP.CHANGE_TAB)

    return table.concat(t, "  ")
end


return PigNote
