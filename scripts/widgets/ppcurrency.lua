local Widget = require "widgets/widget"
local ImageButton = require "widgets/imagebutton"
local Text = require "widgets/text"
local UIAnim = require "widgets/uianim"
local UIAnimButton = require "widgets/uianimbutton"

local CurrencyWidget = Class(Widget, function(self, owner)
    Widget._ctor(self, "ppcurrency", owner)

    self.owner = owner

    self.cur_root = self:AddChild(Widget("root"))
    --self.cur_root:SetScaleMode(SCALEMODE_PROPORTIONAL)
    self.cur_root:SetPosition(-1200, 10)

	self.cur_icon = self.cur_root:AddChild(Image("images/oinc1p.xml", "oinc1p.tex"))  
    self.cur_icon:SetScale(1.5, 1.5)

    self.num_pos = {}
    self.num_pos.x = 140
    self.num_pos.y = 0
    local currency = self.owner.currentcurrency:value()
    self.cur_num = self.cur_root:AddChild(Text(NUMBERFONT, 33, currency and tostring(currency) or "ERROR"))
    self.cur_num:SetHAlign(ANCHOR_LEFT)
    self.cur_num:SetRegionSize(100, 30)
    self.cur_num:SetPosition(self.num_pos.x, self.num_pos.y)
    self.cur_num:SetScale(1.8, 1.8)
	self.cur_num:MoveToFront()

    --self:StartUpdating()
end)

function CurrencyWidget:SetAmount(amount)
    local val = tostring(amount)
    self.cur_num:SetString(amount)
end

return CurrencyWidget