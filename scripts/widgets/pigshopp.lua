local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local Widget = require "widgets/widget"
local Text = require "widgets/text"
local Grid = require "widgets/grid"
local Spinner = require "widgets/spinner"
local UIAnim = require "widgets/uianim"

local TEMPLATES = require "widgets/redux/templates"

require("util")

-------------------------------------------------------------------------------------------------------
local PigShopp = Class(Widget, function(self, parent, shop, customer)
    Widget._ctor(self, "PigShopp")
    self.root = self:AddChild(Widget("root"))
	local tab_root = self.root:AddChild(Widget("tab_root"))
    --ThePlayer:ShowPopUp(POPUPS.pigshopp, true)
    self.menu_root = self:AddChild(Widget("root"))
    
    self.menu_root:SetScale(0.65, 0.65)
    local shopdata = shop.replica.pigshopping
    self.shelves = {}
    local max_shelves = 6
    local currency = customer.currentcurrency:value()
    self.coincount = currency

    local bg = self.menu_root:AddChild(Image("images/pigshopwindowbg.xml", "pigshopwindowbg.tex"))

    local shopkeeper = self.menu_root:AddChild(Widget("root"))
    shopkeeper.anim = shopkeeper:AddChild(UIAnim())
    shopkeeper.animstate = shopkeeper.anim:GetAnimState()
    shopkeeper.animstate:SetBank("townspig")
    shopkeeper.animstate:SetBuild(PPSHOP[shopdata.type:value()].shopkeeper.build)
    shopkeeper:SetScale(0.8, 0.8)
    shopkeeper.animstate:PlayAnimation("idle_table_pre")
    shopkeeper.animstate:PushAnimation("idle_table_loop", true)
    shopkeeper:SetPosition(-180, -380)
    
	local backdrop = self.menu_root:AddChild(Image("images/pigshopwindow.xml", "pigshopwindow.tex"))
    self.menu_root.text = self.menu_root:AddChild(Text(NUMBERFONT, 50, "", UICOLOURS.WHITE))
    self.menu_root.text:SetHAlign(ANCHOR_LEFT)
    self:Talk("Welcome!")

    local base_shelf_offset = {x = 50, y = 200}
    local offset = {x = 200, y = -150}
    local row = 0
    for i = 1, max_shelves do
        local x = base_shelf_offset.x
        self.shelves[i] = self.menu_root:AddChild(Widget("root"))
        self:CreateShelf(self.shelves[i], shop, i, customer)
        if (i % 2 == 0) then
            x = base_shelf_offset.x + (offset.x)
            self.shelves[i]:SetPosition(x, base_shelf_offset.y + (offset.y*row))
            row = row + 1
        else
            self.shelves[i]:SetPosition(x, base_shelf_offset.y + (offset.y*row))
        end        
    end

    self.customer_root = self.menu_root:AddChild(Widget("root"))
    local cur_icon = self.customer_root:AddChild(Image("images/oinc1p.xml", "oinc1p.tex"))  
    local cur_text = self.customer_root:AddChild(Text(NUMBERFONT, 40, "Your Oincs:"))
    cur_text:SetPosition(0, 40)

    self.customer_root.cur_num = self.customer_root:AddChild(Text(NUMBERFONT, 40, currency and tostring(currency) or "ERROR"))
    self.customer_root.cur_num:SetHAlign(ANCHOR_LEFT)
    self.customer_root.cur_num:SetRegionSize(150, 50)
    self.customer_root.cur_num:SetPosition(105, 0)
	self.customer_root.cur_num:MoveToFront()

    self.customer_root:SetPosition(150, 350)
end)

function PigShopp:CreateShelf(root, shop, slot, customer)
    local currency = customer.currentcurrency:value()
    local shopdata = shop.replica.pigshopping
    local prefab = shopdata.inventory["slot"..slot]:value()
    local prefabcost = shopdata.inventory["cost"..slot]:value()
	root.shelf = root:AddChild(Image("images/pigshop_ui.xml", "itemshelf.tex"))
    root.shelf:SetScale(0.6, 0.6)

    root.oinc = root:AddChild(Image("images/oinc1p.xml", "oinc1p.tex"))
    root.oinc:SetScale(0.5, 0.5)
    root.oinc:SetPosition(-30, -10)

    local x = root:AddChild(Text(NUMBERFONT, 30, "x"))
    x:MoveToFront()
    x:SetPosition(-10, -10)

    root.cost = root:AddChild(Text(NUMBERFONT, 30, prefabcost, currency < prefabcost and UICOLOURS.RED or UICOLOURS.WHITE))
    root.cost:MoveToFront()
    root.cost:SetPosition(20, -10)
    root.cost:SetHAlign(ANCHOR_RIGHT)
    root.cost:SetRegionSize(50, 50)

    root.item = root:AddChild(ImageButton(PPSHOP[shopdata.type:value()].items[prefab].atlas or "images/inventoryimages.xml", PPSHOP[shopdata.type:value()].items[prefab].tex or prefab..".tex", nil, nil, nil, nil))
    root.item:SetHoverText(STRINGS.NAMES[string.upper(prefab)])
    root.item:SetPosition(0, 30)
    root.item:MoveToFront()
    root.item:SetClickable(true)
    root.item:SetOnClick(function()
        if self.coincount >= prefabcost then
            self.coincount = self.coincount - prefabcost
            PlayablePets.DoPurchasedItemRPC(customer, prefab, -prefabcost)
            self:Talk("Thank you very much! Come again!")
            self:RefreshPage(shop)
        else
            self:Talk("Sorry, but you can't afford that!")
        end
    end)

    root:SetScale(1.3, 1.3)
    --item.onclick()

    return root
end

function PigShopp:Talk(str)
    self.menu_root.text:SetMultilineTruncatedString(str, 50, 200)
    local desc_w, desc_h = self.menu_root.text:GetRegionSize()
    self.menu_root.text:SetPosition(-380 + 0.5 * desc_w, 300 - 0.5 * desc_h)
end

function PigShopp:_PositionTabs(tabs, w, y)
	local offset = #self.tabs / 2
	for i = 1, #self.tabs do
		local x = (i - offset - 0.5) * w
		tabs[i]:SetPosition(x, y)
	end
end

function PigShopp:_PositionTabs(tabs, w, y)
	local offset = #self.tabs / 2
	for i = 1, #self.tabs do
		local x = (i - offset - 0.5) * w
		tabs[i]:SetPosition(x, y)
	end
end

function PigShopp:RefreshPage(shop)
    local shopdata = shop.replica.pigshopping
    self.customer_root.cur_num:SetString(tostring(self.coincount))
    for i, v in ipairs(self.shelves) do
        local prefabcost = shopdata.inventory["cost"..i]:value()
        if prefabcost > self.coincount then
            self.shelves[i].cost:SetColour(1, 0, 0, 1)
        end
    end
end

function PigShopp:OnUpdate()
    if TheWorld.state.isnight then
        self:Kill()
    end
end

function PigShopp:OnControlTabs(control, down)
    --[[
	if control == CONTROL_OPEN_CRAFTING then
		local tab = self.tabs[((self.last_selected._tabindex - 1) % #self.tabs) + 1]
		if not down then
			tab.onclick()
			return true
		end
	elseif control == CONTROL_OPEN_INVENTORY then
		local tab = self.tabs[((self.last_selected._tabindex + 1) % #self.tabs) + 1]
		if not down then
			tab.onclick()
			return true
		end
	end]]
end

function PigShopp:OnControl(control, down)
    if PigShopp._base.OnControl(self, control, down) then return true end

	--return self:OnControlTabs(control, down)
end

function PigShopp:GetHelpText()
    local controller_id = TheInput:GetControllerID()
    local t = {}

    table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_CRAFTING).."/"..TheInput:GetLocalizedControl(controller_id, CONTROL_OPEN_INVENTORY).. " " .. STRINGS.UI.HELP.CHANGE_TAB)

    return table.concat(t, "  ")
end


return PigShopp
