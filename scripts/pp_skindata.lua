return {
    --Have to have these tables in the main mod to ensure that players don't get duplicates.
    --This determines currently droppable skins.
    RGB = {
        --Original
        "houndplayer",
        "clockwork1player",
        "clockwork2player",
        "clockwork3player",
        "krampusp",
        "dragonplayer",
        "elephantplayer",
        "rabbitp",
        "frogp",
        "glommerp",
        "tentaclep",

        --Rot Pets
        "dustmothp",
        "lightcrabp",
        "striderp",
        "hound_hedgep",

        --Hamlet Pets
        "heraldp",
        "ancient_hulkp",
        "mandrakemanp",
        "thunderbirdp",
        "vampirebatp",
        "pangoldenp",
        "chickenp",
        "antqueenp",
        "rabidbeetlep",
        "glowflyp",
        "frog2p",
        "pogp",
        "gnatp",
        "ancient_robot_headp",
        "ancient_robot_legp",
        "ancient_robot_spiderp",
        "ancient_robot_clawp",

        --New Home Pets
        "butterbirdp",

        --Event Pets
        "golemp",
        "eyeofterror_minip",
        "eyeofterrorp",
        "dragonfly_yulep",
        "boaronp",
        "snapperp",
        "tortankp",
        "peghookp",
        "boarriorp",
        "beetletaurp",
        "goatmomp",
        "goatkidp",
        "trailsp",
        "crowkidp",
        "crowhostp",
        "goose_yulep",
    },

}